<?php
require_once "BaseApis.php";

$date = date("Y-m-d");
$nisit = $conn->queryRaw("Select *  from scholarship ");

foreach ($nisit as $list) {
    if ($list["start_date"] <= $date and $list["end_date"] >= $date) {
        $value = array(
            "seq" => 2,
        );
        $conn->update("scholarship", $value, array("scholarship_id" => $list['scholarship_id']));
    }
}
