<?php

/**
 * Created by PhpStorm.
 * User: Phetkhung
 * Date: 9/5/2561
 * Time: 22:12
 */
class ExceptionCode
{
    public static $LOGIN_SUCCESS = 100;
    public static $LOGIN_FAIL = 101;
    public static $SUCCESS = 200;
    public static $REQUIRE_FIELD = 900;
}