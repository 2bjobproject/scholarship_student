<?php

class TransformModel
{
    private $conn;
    private static $WEB_URL = "/uploads/";

    public function __construct(MySQLDBConn $conn)
    {
        $this->conn = $conn;
    }

    public function transform_employee_model($obj)
    {
        $model = array();
        try {
            $employeeType = $this->conn->select("employee_type", array('employee_type_id' => $obj['employee_type_id']), true);
            $model['employeeId'] = $obj['employee_id'];
            $model['firstName'] = $obj['first_name'];
            $model['lastName'] = $obj['last_name'];
            $model['phone'] = $obj['phone'];
            $model['address'] = $obj['address'];
            $model['employeeType'] = $this->transform_employee_type_model($employeeType);
        } catch (Exception $e) {

        }
        return $model;
    }

    public function transform_customer_model($obj)
    {
        $model = array();
        try {
            $model['customerId'] = $obj['customer_id'];
            $model['firstName'] = $obj['first_name'];
            $model['lastName'] = $obj['last_name'];
            $model['phone'] = $obj['phone'];
            $model['email'] = $obj['email'];
            $model['displayName'] = $obj['display_name'];
            $model['address'] = $obj['address'];
            $model['score'] = $obj['score'];
        } catch (Exception $e) {

        }
        return $model;
    }

    public function transform_car_model_model($obj)
    {
        $model = array();
        try {
            $brand = $this->conn->select("car_brand", array("car_brand_id" => $obj['car_brand_id']), true);
            $model['modelId'] = $obj['car_model_id'];
            $model['modelName'] = $obj['car_model_name'];
            $model['brand'] = $this->transform_car_brand_model($brand);
        } catch (Exception $e) {

        }
        return $model;
    }

    public function transform_car_brand_model($obj)
    {
        $model = array();
        try {
            $model['brandId'] = $obj['car_brand_id'];
            $model['brandName'] = $obj['car_brand_name'];
        } catch (Exception $e) {

        }
        return $model;
    }

    public function transform_time_model($obj)
    {
        $model = array();
        try {
            $model['timeId'] = $obj['time_id'];
            $model['timeName'] = $obj['time_name'];
        } catch (Exception $e) {

        }
        return $model;
    }

    public function transform_package_model($obj)
    {
        $model = array();
        try {
            $service = $this->conn->select("service", array("service_id" => $obj['service_id']), true);
            $car_model = $this->conn->select("car_model", array("car_model_id" => $obj['car_model_id']), true);
            $model['actualPrice'] = $obj['actual_price'];
            $model['model'] = $this->transform_car_model_model($car_model);
            $model['service'] = $this->transform_service_model($service);
        } catch (Exception $e) {

        }
        return $model;
    }

    public function transform_request_repair_model($obj, $isChild = false)
    {
        $model = array();
        try {
            $customer = $this->conn->select("customer", array("customer_id" => $obj['customer_id']), true);
            $car_model = $this->conn->select("car_model", array("car_model_id" => $obj['car_model_id']), true);
            $status = $this->conn->select("status", array("status_id" => $obj['status_id']), true);
            $time = $this->conn->select("m_time", array("time_id" => $obj['time_id']), true);
            $model['reserveNo'] = $obj['reserve_no'];
            $model['reserveDate'] = $obj['reserve_date'];
            $model['walkinDatetime'] = $obj['walkin_datetime'];
            $model['returnDatetime'] = $obj['return_datetime'];
            $model['carLicense'] = $obj['car_license'];
            $model['mileNumber'] = $obj['mile_number'];
            $model['detailRepair'] = $obj['detail_repair'];
            $model['remark'] = $obj['remark'];
            $model['totalPrice'] = $obj['total_price'];
            $model['model'] = $this->transform_car_model_model($car_model);
            $model['customer'] = $this->transform_customer_model($customer);
            $model['status'] = $this->transform_status_model($status);
            $model['time'] = $this->transform_time_model($time);
            $model['image1'] = $this->transform_file_model($obj['image_path1']);
            $model['image2'] = $this->transform_file_model($obj['image_path2']);
            if ($isChild) {
                $model['products'] = $this->transform_reserve_product_list($obj['reserve_no']);
                $model['packages'] = $this->transform_reserve_package_list($obj['reserve_no']);
            }
        } catch (Exception $e) {

        }
        return $model;
    }

    public function transform_reserve_package_list($reserve_no)
    {
        $data = array();
        $list = $this->conn->queryRaw("SELECT *
                from request_package
                where reserve_no='$reserve_no'");
        foreach ($list as $item) {
            $data[] = $this->transform_package_model($item);
        }
        return $data;
    }

    public function transform_reserve_product_list($reserve_no)
    {
        $data = array();
        $list = $this->conn->queryRaw("SELECT
                request_product.product_id,
                request_product.amount,
                request_product.actual_price,
                product.product_name,
                product.detail,
                product.path_image,
                product.product_type_id
                from request_product
                left join product on product.product_id = request_product.product_id
                where reserve_no='$reserve_no'");
        foreach ($list as $item) {
            $data[] = $this->transform_product_model($item);
        }
        return $data;
    }

    public function transform_service_model($obj)
    {
        $model = array();
        try {
            $model['serviceId'] = $obj['service_id'];
            $model['serviceName'] = $obj['service_name'];
        } catch (Exception $e) {

        }
        return $model;
    }

    public function transform_status_model($obj)
    {
        $model = array();
        try {
            $model['statusId'] = $obj['status_id'];
            $model['statusName'] = $obj['status_name'];
        } catch (Exception $e) {

        }
        return $model;
    }

    public function transform_product_model($obj)
    {
        $model = array();
        try {
            $productType = $this->conn->select("product_type", array("product_type_id" => $obj['product_type_id']), true);
            $model['productId'] = $obj['product_id'];
            $model['productName'] = $obj['product_name'];
            $model['detail'] = $obj['detail'];
            $model['actualPrice'] = $obj['actual_price'];
            $model['amount'] = $obj['amount'];
            $model['productType'] = $this->transform_product_type_model($productType);
            $model['image'] = $this->transform_file_model($obj['path_image']);
        } catch (Exception $e) {

        }
        return $model;
    }

    public function transform_product_type_model($obj)
    {
        $model = array();
        try {
            $model['productTypeId'] = $obj['product_type_id'];
            $model['productTypeName'] = $obj['product_type_name'];
        } catch (Exception $e) {

        }
        return $model;
    }

    public function transform_file_model($fileName)
    {
        $model = array();
        try {
            $model['fileName'] = $fileName;
            $model['fileUrl'] = TransformModel::$WEB_URL . $fileName;
        } catch (Exception $e) {

        }
        return $model;
    }

    public function transform_employee_type_model($obj)
    {
        $model = array();
        try {
            $model['employeeTypeId'] = intval($obj['employee_type_id']);
            $model['employeeTypeName'] = $obj['employee_type_name'];
        } catch (Exception $e) {

        }
        return $model;
    }

    public function transform_news_model($obj)
    {
        $model = array();
        try {
            $model['newsId'] = $obj['news_id'];
            $model['newsName'] = $obj['news_name'];
            $model['newsDate'] = $obj['news_date'];
            $model['newsUrl'] = '/admin/news-view.php?__news_id='. $obj['news_id'];
        } catch (Exception $e) {

        }
        return $model;
    }

    public function transform_mile_model($obj)
    {
        $model = array();
        try {
            $model['mileId'] = $obj['mile_id'];
            $model['mileName'] = $obj['mile_name'];
        } catch (Exception $e) {

        }
        return $model;
    }

    public function transform_faq_model($obj)
    {
        $model = array();
        try {
            $model['question'] = $obj['subject'];
            $model['answer'] = $obj['detail'];
        } catch (Exception $e) {

        }
        return $model;
    }

    public function transform_calulate_service_model($obj)
    {
        $model = array();
        try {
            $model['serviceId'] = $obj['service_id'];
            $model['serviceName'] = $obj['service_name'];
            $model['actualPrice'] = floatval($obj['actual_price']);
        } catch (Exception $e) {

        }
        return $model;
    }
}