<div id="waitForm" class="modal">
    <div class="modal-dialog modal-dialog-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 id="headerBlock" class="modal-title">คำเตือน !!!!</h4>
            </div>
            <div class="modal-body">
                <span id="bodyBlock"></span>
            </div>
        </div>
    </div>
</div>
<!-- MAIN JS FILES REQUIRED ON ALL PAGES -->
<script src="front/html/vendor/jquery/jquery.min.js"></script>
<script src="../://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
<script type="text/javascript" src="plugins/SmartWizard-master/src/js/jquery.smartWizard.js"></script>
<script src="plugins/iCheck/icheck.min.js"></script>
<script src="plugins/jquery.steps/jquery.steps.min.js"></script>
<script src="front/html/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="front/html/vendor/bootstrap-hover-dropdown/bootstrap-hover-dropdown.js"></script>
<script src="front/html/vendor/wow/wow.min.js"></script>
<script src="front/html/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
<script src="front/html/vendor/SmoothScroll/SmoothScroll.min.js"></script>

<script src="front/html/vendor/waypoints/waypoints.min.js"></script>
<script src="front/html/vendor/counterup/jquery.counterup.min.js"></script>


<!-- HERO SLIDER-->
<script src="front/html/vendor/hero-slider/js/modernizr.js"></script>
<script src="front/html/vendor/hero-slider/js/main.js"></script>

<!-- OWL CAROUSEL -->
<script src="front/html/vendor/owl-carousel/owl.carousel.js"></script>

<!-- FILTERIZR -->
<script src="front/html/vendor/jquery/jquery.filterizr.js"></script>
<script src="front/html/vendor/jquery/jquery.filterizr.js"></script>
<script src="js/jquery.datetimepicker.js"></script>
<script src="plugins/jquery-bar-rating-master/jquery.barrating.js"></script>
<script src="assets/jquery/screen.js"></script>

<!-- EDURA JS REQUIRED ON ALL PAGES-->
<script src="front/html/edura.js"></script>


<script>
    function alertError(message) {
        var modal = $("#waitForm");
        modal.find("#bodyBlock").text(message);
        modal.modal();
    }
</script>