<?php
$user_id = $uprofile['id'];
?>
<section id="header">
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">
                    <img src="uploads/220px-KMUTNB-LOGO.jpg" alt="" style="height: 50px; width: 50px;">
                </a>
                <a class="navbar-brand" href="index.php" style="max-width: 450px">
                    <h4 style="margin-top: 5px; margin-left: 5px" > <?php echo TITLE_ENG;?></h4>
                </a>
            </div>

            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="index.php">หน้าหลัก</a></li>
                    <li><a href="scholarship.php">ทุน</a></li>
                    <li><a href="contact_us.php">ติดต่อเรา</a></li>
                    <?php if($uprofile == null && $uprofile['level'] != STUDENT_LEVEL){?>
                    <li><a href="admin/login.php">เข้าสู่ระบบ</a></li>
                    <?php }else{ ?>
                        <li><a href="list_scholarship.php">ประวัติการขอทุน</a></li>
                        <li><a href="nisit-update.php?__nisit_id=<?php echo $user_id; ?>">แก้ไขข้อมูลส่วนตัว</a></li>
                        <li><a href="admin/logout.php">ออกจากระบบ</a></li>
                    <?php } ?>
                </ul>
            </div>

        </div>
    </nav>
</section>