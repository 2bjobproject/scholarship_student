<?php
session_start(); // เปิดใช้งาน session
require_once "../common.inc.php"; //
require_once "../connection.inc.php"; //

$user_id = $uprofile['id'];
$cmd = getIsset("__cmd");
$nisit_id = getIsset('__nisit_id');
if ($cmd == "save") {
    $value = array(
        "nisit_code" => getIsset('__nisit_code'),
        "first_name" => getIsset('__first_name'),
        "last_name" => getIsset('__last_name'),
        "id_card" => getIsset('__id_card'),
        "religion" => getIsset('__religion'),
        "birth_date" => getIsset('__birth_date'),
        "image_path" => uploadFile($_FILES['__file_upload'], getIsset('__image_path'), PATH_UPLOAD),
        "title_id" => getIsset('__title_id'),
        "weight" => getIsset('__weight'),
        "tall" => getIsset('__tall'),
        "national" => getIsset('__national'),
        "rac" => getIsset('__rac'),
        "address" => getIsset('__address'),
        "moo" => getIsset('__moo'),
        "soi" => getIsset('__soi'),
        "street" => getIsset('__street'),
        "zipcode" => getIsset('__zipcode'),
        "province_code" => getIsset('__province_code'),
        "amphur_code" => getIsset('__amphur_code'),
        "district_code" => getIsset('__district_code'),
        "phone" => getIsset('__phone'),
        "email" => getIsset('__email'),
        "present_address" => getIsset('__present_address'),
        "present_moo" => getIsset('__present_moo'),
        "present_soi" => getIsset('__present_soi'),
        "present_street" => getIsset('__present_street'),
        "present_province_code" => getIsset('__present_province_code'),
        "present_amphur_code" => getIsset('__present_amphur_code'),
        "present_district_code" => getIsset('__present_district_code'),
        "present_zipcode" => getIsset('__present_zipcode'),
        "present_phone" => getIsset('__present_phone'),
        "present_fax" => getIsset('__present_fax'),
        "account_type_id" => getIsset('__account_type_id'),
        "bank_id" => getIsset('__bank_id'),
        "account_no" => getIsset('__account_no'),
        "register_date" => date("Y-m-d H:i:s"),
        "nisit_status_id" => 1,

//        "password" => getIsset('__password'),
    );
    if ($conn->create("nisit", $value )) {
        $nisit_id = $conn->getLastInsertId();
        $data = array( "nisit_id" => $nisit_id);
        $conn->create("nisit_family",$data);
        $conn->create("nisit_event",$data);
        $conn->create("nisit_education",$data);
    }
    redirectTo("index.php");
}
?>
<!doctype html>
<html lang="en">
<head>
    <title><?php echo TITLE_ENG; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php include "css.php";?>
</head>
<body>

<div class="wrapper d-flex align-items-stretch">
    <?php include "nav.php"?>
    <!-- Page Content  -->
    <div id="content" class="p-4 p-md-5 pt-5"  >
        <form class="form-horizontal" id="form_data" name="form_data" method="post" enctype="multipart/form-data">
            <input id="__cmd" name="__cmd" type="hidden" value="">
            <div class="box box-custom">
                <div class="box-header with-border">
                    <h3 class="box-title">สมัครสมาชิก </h3>
                </div>
                <div class="box-body">
                    <input type="hidden" name="__nisit_id" id="__nisit_id" class="form-control"
                           value="0"
                           required="true" readonly>
                    <div class="form-group">
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    รูปภาพ :
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <img id="img-preview" src="" class="image img-thumbnail"
                                     onerror="src='../uploads/uploadfile.png'">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">

                            </label>

                            <div class="col-sm-5">
                                <input type="file" name="__file_upload" style="display: none">
                                <input type="hidden" name="__image_path" id="__image_path">
                                <a type="button" class="btn btn-success btn-xs upload-logo">อัพโหลด</a>
                                <a type="button" class="btn btn-danger btn-xs del-logo">ลบไฟล์</a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div align="right">
                            <label class="col-sm-3 control-label">
                                รหัสนักศึกษา :
                            </label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" name="__nisit_code" id="__nisit_code"
                                   class="form-control"
                                   value=""  >
                        </div>
                        <div align="right">
                            <label class="col-sm-2 control-label">
                                คำนำหน้า :
                            </label>
                        </div>
                        <div class="col-sm-3">
                            <select name="__title_id" id="__title_id" class="form-control">
                                <?php
                                $level = $conn->select('title');
                                foreach ($level as $type) {
                                    ?>
                                    <option
                                            value="<?php echo $type['title_id']; ?>">
                                        <?php echo $type['title_name']; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div align="right">
                            <label class="col-sm-3 control-label">
                                ชื่อ :
                            </label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" name="__first_name" id="__first_name"
                                   class="form-control"
                                   value=""
                                   onblur="trimValue(this);" required="true">
                        </div>
                        <div align="right">
                            <label class="col-sm-2 control-label">
                                นามสกุล :
                            </label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" name="__last_name" id="__last_name"
                                   class="form-control"
                                   value=""
                                   onblur="trimValue(this);" required="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <div align="right">
                            <label class="col-sm-3 control-label">
                                เลขบัตรประจำตัวประชาชน :
                            </label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" name="__id_card" id="__id_card"
                                   class="form-control" value=""
                                   maxlength="13" required
                                   onkeypress="chkInteger(event)">
                        </div>

                        <div align="right">
                            <label class="col-sm-2 control-label">
                                ศาสนา :
                            </label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" name="__religion" id="__religion"
                                   class="form-control"
                                   value=""
                                   onblur="trimValue(this);" required="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <div align="right">
                            <label class="col-sm-3 control-label">
                                วันเกิด:
                            </label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" name="__birth_date"
                                   id="__birth_date"
                                   class="form-control"
                                   value="" readonly
                                   >
                        </div>
                    </div>
                    <div class="form-group">
                        <div align="right">
                            <label class="col-sm-2 control-label">
                                น้ำหนัก :
                            </label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" name="__weight" id="__weight"
                                   class="form-control" value=""
                                   onblur="trimValue(this);" required
                                   onkeypress="chkInteger(event)">
                        </div>
                        <div align="right">
                            <label class="col-sm-1 control-label">
                                ส่วนสูง :
                            </label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" name="__tall" id="__tall"
                                   class="form-control" value=""
                                   onblur="trimValue(this);" maxlength="10" required
                                   onkeypress="chkInteger(event)">
                        </div>

                        <div align="right">
                            <label class="col-sm-1 control-label">
                                สัญชาติ :
                            </label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" name="__national" id="__national"
                                   class="form-control"
                                   value=""
                                   onblur="trimValue(this);" required="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <div align="right">
                            <label class="col-sm-2 control-label">
                                เชื่อชาติ :
                            </label>
                        </div>
                        <div class="col-sm-5">
                            <input type="text" name="__rac" id="__rac"
                                   class="form-control"
                                   value=""
                                   onblur="trimValue(this);" required="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <div align="right">
                            <label class="col-sm-2 control-label">
                                บ้านเลขที่ :
                            </label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" name="__address" id="__address"
                                   class="form-control" value=""
                                   onblur="trimValue(this);" required
                                   onkeypress="chkInteger(event)">
                        </div>

                        <div align="right">
                            <label class="col-sm-1 control-label">
                                หมู่ :
                            </label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" name="__moo" id="__moo"
                                   class="form-control" value=""
                                   onblur="trimValue(this);" required
                                   onkeypress="chkInteger(event)">
                        </div>
                        <div align="right">
                            <label class="col-sm-1 control-label">
                                ซอย :
                            </label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" name="__soi" id="__soi"
                                   class="form-control"
                                   value=""
                                   onblur="trimValue(this);" required="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <div align="right">
                            <label class="col-sm-2 control-label">
                                ถนน :
                            </label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" name="__street" id="__street"
                                   class="form-control"
                                   value=""
                                   onblur="trimValue(this);" required="true">
                        </div>
                        <div align="right">
                            <label class="col-sm-2 control-label">
                                รหัสไปรษณี :
                            </label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" name="__zipcode" id="__zipcode"
                                   class="form-control" value=""
                                   onblur="trimValue(this);" maxlength="5" required
                                   onkeypress="chkInteger(event)">
                        </div>
                    </div>
                    <div class="form-group">
                        <div align="right">
                            <label class="col-sm-2 control-label">
                                จังหวัด :
                            </label>
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <input type="hidden" name="__province_code" id="__province_code"
                                       class="form-control"
                                       value=""
                                       readonly>
                                <input type="text" name="__province_name" id="__province_name"
                                       class="form-control"
                                       value="" readonly required>
                                <a href="javascript:goPage('../admin/linkhelp.php?__filter=province&__action=getProvinceById');"
                                   class="btn btn-default input-group-addon"><i
                                            class="fa fa-search"></i> </a>
                            </div>
                        </div>
                        <div align="right">
                            <label class="col-sm-2 control-label">
                                อำเภอ(เขต)  :
                            </label>
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <input type="hidden" name="__amphur_code" id="__amphur_code"
                                       class="form-control"
                                       value=""
                                       readonly>
                                <input type="text" name="__amphur_name" id="__amphur_name"
                                       class="form-control"
                                       value="" readonly required>
                                <a href="javascript:goPageReference('../admin/linkhelp.php?__filter=amphur&__action=getAmphurById','__province_code');"
                                   class="btn btn-default input-group-addon"><i
                                            class="fa fa-search"></i> </a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div align="right">
                            <label class="col-sm-2 control-label">
                                ตำบล(แขวง) :
                            </label>
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <input type="hidden" name="__district_code" id="__district_code"
                                       class="form-control"
                                       value=""
                                       readonly>
                                <input type="text" name="__district_name" id="__district_name"
                                       class="form-control"
                                       value="" readonly required>
                                <a href="javascript:goPageReference('../admin/linkhelp.php?__filter=district&__action=getDistrictById','__amphur_code');"
                                   class="btn btn-default input-group-addon"><i
                                            class="fa fa-search"></i> </a>
                            </div>
                        </div>
                        <div align="right">
                            <label class="col-sm-2 control-label">
                                เบอร์โทร :
                            </label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" name="__phone" id="__phone"
                                   class="form-control" value=""
                                   onblur="trimValue(this);" maxlength="10" required
                                   onkeypress="chkInteger(event)">
                        </div>
                    </div>
                    <div class="form-group">
                        <div align="right">
                            <label class="col-sm-2 control-label">
                                อีเมล์ :
                            </label>
                        </div>
                        <div class="col-sm-5">
                            <input type="email" name="__email" id="__email"
                                   class="form-control"
                                   value=""
                                   onblur="trimValue(this);" required="true">
                        </div>
                    </div>


                    <div class="form-group">
                        <div align="right">
                            <label class="col-sm-2 control-label">
                                ที่อยู่ปัจจุบัน: บ้านเลขที่ :
                            </label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" name="__present_address" id="__present_address"
                                   class="form-control" value=""
                                   onblur="trimValue(this);" required
                                   onkeypress="chkInteger(event)">
                        </div>

                        <div align="right">
                            <label class="col-sm-1 control-label">
                                หมู่ :
                            </label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" name="__present_moo" id="__present_moo"
                                   class="form-control" value=""
                                   onblur="trimValue(this);" = required
                            onkeypress="chkInteger(event)">
                        </div>

                        <div align="right">
                            <label class="col-sm-1 control-label">
                                ซอย :
                            </label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" name="__present_soi" id="__present_soi"
                                   class="form-control"
                                   value=""
                                   onblur="trimValue(this);" required="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <div align="right">
                            <label class="col-sm-2 control-label">
                                ถนน :
                            </label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" name="__present_street" id="__present_street"
                                   class="form-control"
                                   value=""
                                   onblur="trimValue(this);" required="true">
                        </div>
                        <div align="right">
                            <label class="col-sm-1 control-label">
                                จังหวัด :
                            </label>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <input type="hidden" name="__present_province_code" id="__present_province_code"
                                       class="form-control"
                                       value=""
                                       readonly>
                                <input type="text" name="__present_province_name" id="__present_province_name"
                                       class="form-control"
                                       value="" readonly required>
                                <a href="javascript:goPage('../admin/linkhelp.php?__filter=present_province&__action=getProvincePById');"
                                   class="btn btn-default input-group-addon"><i
                                            class="fa fa-search"></i> </a>
                            </div>
                        </div>
                        <div align="right">
                            <label class="col-sm-1 control-label">
                                อำเภอ(เขต)  :
                            </label>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <input type="hidden" name="__present_amphur_code" id="__present_amphur_code"
                                       class="form-control"
                                       value=""
                                       readonly>
                                <input type="text" name="__present_amphur_name" id="__present_amphur_name"
                                       class="form-control"
                                       value="" readonly required>
                                <a href="javascript:goPageReference('../admin/linkhelp.php?__filter=present_amphur&__action=getAmphurPById','__present_province_code');"
                                   class="btn btn-default input-group-addon"><i
                                            class="fa fa-search"></i> </a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div align="right">
                            <label class="col-sm-2 control-label">
                                ตำบล(แขวง) :
                            </label>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <input type="hidden" name="__present_district_code" id="__present_district_code"
                                       class="form-control"
                                       value=""
                                       readonly>
                                <input type="text" name="__present_district_name" id="__present_district_name"
                                       class="form-control"
                                       value="" readonly required>
                                <a href="javascript:goPageReference('../admin/linkhelp.php?__filter=present_district&__action=getDistrictPById','__present_amphur_code');"
                                   class="btn btn-default input-group-addon"><i
                                            class="fa fa-search"></i> </a>
                            </div>
                        </div>
                        <div align="right">
                            <label class="col-sm-2 control-label">
                                รหัสไปรษณี :
                            </label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" name="__present_zipcode" id="__present_zipcode"
                                   class="form-control" value=""
                                   onblur="trimValue(this);" maxlength="5" required
                                   onkeypress="chkInteger(event)">
                        </div>

                        <div align="right">
                            <label class="col-sm-1 control-label">
                                เบอร์โทร :
                            </label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" name="__present_phone" id="__present_phone"
                                   class="form-control" value=""
                                   onblur="trimValue(this);" maxlength="10" required
                                   onkeypress="chkInteger(event)">
                        </div>
                    </div>
                    <div class="form-group">
                        <div align="right">
                            <label class="col-sm-2 control-label">
                                แฟรกซ์ :
                            </label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" name="__present_fax" id="__present_fax"
                                   class="form-control" value=""
                                   onblur="trimValue(this);" maxlength="15" required
                                   onkeypress="return chkNumber()">
                        </div>
                        <div align="right">
                            <label class="col-sm-2 control-label">
                                ประเภทบัญชี :
                            </label>
                        </div>
                        <div class="col-sm-2">
                            <select name="__account_type_id" id="__account_type_id" class="form-control">
                                <?php
                                $level = $conn->select('account_type');
                                foreach ($level as $type) {
                                    ?>
                                    <option
                                            value="<?php echo $type['account_type_id']; ?>">
                                        <?php echo $type['account_type_name']; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div align="right">
                            <label class="col-sm-1 control-label">
                                บัญชี :
                            </label>
                        </div>
                        <div class="col-sm-2">
                            <select name="__bank_id" id="__bank_id" class="form-control">
                                <?php
                                $level = $conn->select('bank');
                                foreach ($level as $type) {
                                    ?>
                                    <option
                                            value="<?php echo $type['bank_id']; ?>">
                                        <?php echo $type['bank_name']; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div align="right">
                            <label class="col-sm-2 control-label">
                                เลขที่บัญชี :
                            </label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" name="__account_no" id="__account_no"
                                   class="form-control" value=""
                                   onblur="trimValue(this);" maxlength="20" required
                                   onkeypress="chkInteger(event)">
                        </div>
                        <!--                            <div align="right">-->
                        <!--                                <label class="col-sm-2 control-label">-->
                        <!--                                    รหัสผ่าน :-->
                        <!--                                </label>-->
                        <!--                            </div>-->
                        <!--                            <div class="col-sm-2">-->
                        <!--                                <input type="password" name="__password" id="__password"-->
                        <!--                                       class="form-control" value=""-->
                        <!--                                       onblur="trimValue(this);" maxlength="20" required>-->
                        <!--                            </div>-->
                    </div>
                    <div class="form-group">
                        <div align="right">
                            <label class="col-sm-5 control-label">
                            </label>
                        </div>
                        <div class="col-sm-5">
                            <a class="btn btn-success" href="javascript:goSave();">บันทึก</a>
                            <a class="btn btn-warning" href="javascript:goClear()">ล้าง</a>
                            <a class="btn btn-default" href="index.php">ย้อนกลับ</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<?php include 'script.php'?>
<script>
    $('#__birth_date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
    });
    function chkNumber(ele) {
        var vchar = String.fromCharCode(event.keyCode);
        if ((vchar < '0' || vchar > '9') && (vchar != '-')) return false;
        ele.onKeyPress = vchar;
    }
    function helpReturn(value, action) {
        $.ajax({
            url: '../admin/Allservice.php',
            data: {id: value, action: action},
            method: 'GET',
            success: function (result) {
                var data = JSON.parse(result);
                if (action == "getNisitById") {
                    if (data.nisit_id != null) {
                        console.log(data);
                        setValueNisit(data);
                    }
                }
                if (action == "getProvinceById") {
                    if (data.province_code != null) {
                        setValueProvince(data);
                    }
                }
                if (action == "getAmphurById") {
                    if (data.amphur_code != null) {
                        setValueAmphur(data);
                    }
                }
                if (action == "getDistrictById") {
                    if (data.district_code != null) {
                        setValueDistrict(data);
                    }
                }
                if (action == "getProvincePById") {
                    if (data.province_code != null) {
                        setValueProvinceP(data);
                    }
                }
                if (action == "getAmphurPById") {
                    if (data.amphur_code != null) {
                        setValueAmphurP(data);
                    }
                }
                if (action == "getDistrictPById") {
                    if (data.district_code != null) {
                        setValueDistrictP(data);
                    }
                }
                if (action == "getSectionById") {
                    if (data.section_id != null) {
                        setValueSection(data);
                    }
                }
                if (action == "getDepartmentById") {
                    if (data.department_id != null) {
                        setValueDepartment(data);
                    }
                }
            }
        });
    }

    function setValueNisit(data) {
        with (document.form_data) {
            $("#__nisit_id").val(data.nisit_id);
            $("#__nisit_code").val(data.nisit_code);
            $("#__first_name").val(data.first_name);
            $("#__last_name").val(data.last_name);
            $("#__id_card").val(data.id_card);
            $("#__birth_date").val(data.birth_date);
            $("#__religion").val(data.religion);
            $("#__image_path").val(data.image_path);
            $("#img-preview").attr('src', '<?php echo PATH_UPLOAD . '/';?>' + data.image_path);
            $("#__title_id").val(data.title_id);
            $("#__weight").val(data.weight);
            $("#__tall").val(data.tall);
            $("#__national").val(data.national);
            $("#__rac").val(data.rac);
            $("#__address").val(data.address);
            $("#__moo").val(data.moo);
            $("#__soi").val(data.soi);
            $("#__street").val(data.street);
            $("#__zipcode").val(data.zipcode);
            $("#__province_code").val(data.province_code);
            $("#__amphur_code").val(data.amphur_code);
            $("#__district_code").val(data.district_code);
            $("#__province_name").val(data.province_name);
            $("#__amphur_name").val(data.amphur_name);
            $("#__district_name").val(data.district_name);
            $("#__phone").val(data.phone);
            $("#__email").val(data.email);
            $("#__present_address").val(data.present_address);
            $("#__present_moo").val(data.present_moo);
            $("#__present_soi").val(data.present_soi);
            $("#__present_street").val(data.present_street);
            $("#__present_province_code").val(data.present_province_code);
            $("#__present_amphur_id").val(data.present_amphur_code);
            $("#__present_district_code").val(data.present_district_code);
            $("#__present_province_name").val(data.province_name);
            $("#__present_amphur_name").val(data.amphur_name);
            $("#__present_district_name").val(data.district_name);
            $("#__present_zipcode").val(data.present_zipcode);
            $("#__present_phone").val(data.present_phone);
            $("#__present_fax").val(data.present_fax);
            $("#__account_type_id").val(data.account_type_id);
            $("#__bank_id").val(data.bank_id);
            $("#__account_no").val(data.account_no);
            $("#__password").val(data.password);
            $("#__register_date").val(data.register_date);

        }
    }

    function setValueProvince(data) {
        with (document.form_data) {
            $("#__province_code").val(data.province_code);
            $("#__province_name").val(data.province_name);
        }
    }

    function setValueAmphur(data) {
        with (document.form_data) {
            $("#__amphur_code").val(data.amphur_code);
            $("#__amphur_name").val(data.amphur_name);
        }
    }

    function setValueDistrict(data) {
        with (document.form_data) {
            $("#__district_code").val(data.district_code);
            $("#__district_name").val(data.district_name);
        }
    }

    function setValueProvinceP(data) {
        with (document.form_data) {
            $("#__present_province_code").val(data.province_code);
            $("#__present_province_name").val(data.province_name);
        }
    }

    function setValueAmphurP(data) {
        with (document.form_data) {
            $("#__present_amphur_code").val(data.amphur_code);
            $("#__present_amphur_name").val(data.amphur_name);
        }
    }

    function setValueDistrictP(data) {
        with (document.form_data) {
            $("#__present_district_code").val(data.district_code);
            $("#__present_district_name").val(data.district_name);
        }
    }

    function setValueSection(data) {
        with (document.form_data) {
            $("#__section_id").val(data.section_id);
            $("#__section_name").val(data.section_name);
        }
    }

    function setValueDepartment(data) {
        with (document.form_data) {
            $("#__department_id").val(data.department_id);
            $("#__department_name").val(data.department_name);
        }
    }

</script>
<script>helpReturn('<?php echo $nisit_id;?>', 'getNisitById')</script>
</body>
</html>
