<?php
session_start(); // เปิดใช้งาน session
require_once "../common.inc.php";
require_once "../connection.inc.php";

$cmd = getIsset("__cmd");

$date = date("Y-m-d");

$filterDefault = " where 1=1 ";

$keyword = getIsset("keyword");
if ($keyword != "") {
    $filterDefault .= " and " . 'scholarship_name' . " like '%" . $keyword . "%'";
}

$sql = "select scholarship.*,scholarship_type_name,semester_name from scholarship
left join scholarship_type on scholarship_type.scholarship_type_id=scholarship.scholarship_type_id  
left join semester on semester.semester_id=scholarship.semester_id";
$result_row = $conn->queryRaw($sql.$filterDefault);//คิวรี่ คำสั่ง
$total = sizeof($result_row);
$for_end = $limit;
$for_start = $start * $limit;

$select_all = $conn->queryRaw($sql . $filterDefault." order by seq desc, end_date desc limit " . $for_start . "," . $for_end);
$total_num = sizeof($select_all);

$max = "select * from faculty";
$total = $conn->queryRaw($max);
?>
<!doctype html>
<html lang="en">
<head>

    <title><?php echo TITLE_ENG; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include "css.php"; ?>
</head>
<body>
<?php include "nav1.php" ?>
<div class="wrapper d-flex align-items-stretch">
    <?php include "nav.php" ?>
    <!-- Page Content  -->
    <div id="content" class="p-4 p-md-5 pt-5">
        <section id="blog-list" class="container">
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" id="form_data" name="form_data" method="post">
                        <input id="__delete_field" name="__delete_field" type="hidden" value="">
                        <input id="__cmd" name="__cmd" type="hidden" value="">

                        <div class="col-md-12">
                            <label class="col-sm-3 control-label">
                            </label>
                        </div>
                        <div class="clr"></div>
                        <div class="col-sm-12">
                            <div class="box box-gold">
                                <div class="box-header with-border">
                                    <h3 class="box-title">ประกาศรับสมัครทุน</h3>
                                </div>
                                <div class="box-body">
                                    <div class="form-group">
                                        <div class="col-sm-7">

                                        </div>
                                        <div class="col-sm-2">

                                        </div>
                                        <div class="col-sm-3">
                                            <div class="input-group">
                                                <input class="form-control" type="text" id="keyword" name="keyword"
                                                       onblur="trimValue(this)" onkeyup="goSearch()"
                                                       value="<?php echo $keyword; ?>">
                                                <a href="javascript:goSearch();"
                                                   class="btn btn-default input-group-addon"><i
                                                            class="fa fa-search"></i> </a>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class=" table-responsive">
                                                <table class="table table-hover tbgray" id="tbView">
                                                    <tr>
                                                        <!--                                                        <th width="5%">ลำดับ</th>-->
                                                        <!--                                                        <th width="20%">ชื่อผู้ขอทุน</th>-->

                                                        <th class="" width="20%" nowrap="">ชื่อทุน</th>
                                                        <th class="" width="5%" nowrap="">จำนวน</th>
                                                        <!-- <th class="" width="20%" nowrap="">คณะ</th> -->
                                                        <th class="" width="20%" nowrap="">ประเภท</th>
                                                        <th class="" width="20%" nowrap="">ระยะเวลา</th>

                                                        <th class="" width="5%" nowrap="">สถานะ</th>
                                                    </tr>
                                                    <tbody>
                                                    <?php
                                                    $index = $for_start;
                                                    foreach ($select_all

                                                    as $row) {

                                                    $fac = $conn->queryRaw("select scholarship_detail.*,faculty.faculty_name  from scholarship_detail
left join faculty on faculty.faculty_id=scholarship_detail.faculty_id where scholarship_id='" . $row['scholarship_id'] . "'");
                                                    $index++;
                                                    ?>
                                                    <?php if ($uprofile != null) { ?>
                                                    <?php if ($row["start_date"] <= $date and $row["end_date"] >= $date) { ?>
                                                    <tr onclick="location.href='scholarship-detail.php?scholarship_id=<?php echo $row["scholarship_id"]; ?>'">
                                                        <?php } else { ?>
                                                    <tr>
                                                        <?php } ?>
                                                        <?php } else { ?>
                                                    <tr onclick="location.href='scholarship-detail.php?scholarship_id=<?php echo $row["scholarship_id"]; ?>'">
                                                        <?php } ?>


                                                        <td class="active "
                                                            nowrap><?php echo $row['scholarship_name']; ?></td>
                                                        <td class="active">
                                                            <div class="col-xs-5 text-left"></i> <?php echo $row["amount_use"]; ?></div>
                                                        </td>
                                                        
                                                        <td class="active "
                                                            nowrap><?php echo $row['scholarship_type_name']; ?></td>

                                                        <td class="active ">
                                                            <?php echo $row["start_date"]; ?> -
                                                            <?php echo $row["end_date"]; ?>
                                                        </td>

                                                        <td class="active ">
                                                            <?php if ($uprofile != null) { ?>
                                                                <?php if ($row["start_date"] <= $date and $row["end_date"] >= $date) { ?>
                                                                    <div class="blog-item-inner">
                                                                        <a href="scholarship-detail.php?scholarship_id=<?php echo $row["scholarship_id"]; ?>"
                                                                           class="btn btn-sm btn-success">สมัคร</a>
                                                                    </div>
                                                                <?php } else { ?>
                                                                    <div class="blog-item-inner">
                                                                        <a href="#"
                                                                           class="btn btn-sm btn-danger">หมดเวลา</a>
                                                                    </div>
                                                                <?php } ?>
                                                            <?php } else { ?>
                                                                <div class="blog-item-inner">
                                                                    <a href="scholarship-detail.php?scholarship_id=<?php echo $row["scholarship_id"]; ?>"
                                                                       class="btn btn-sm btn-default">รายละเอียด</a>
                                                                </div>
                                                            <?php } ?>
                                                        </td>
                                                        <?php } ?>
                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-sm-12">
                                                <?php include "../admin/pageindex.php"; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
<?php include 'footer.php' ?>
<?php include 'script.php' ?>
<script>
    var chartRenderIntervalId = null;
    var interval = 2400;
    function intervalChartRender() {
        chartRenderIntervalId = setInterval(() => {
            updateChart();
        }, interval);
    }


    function updateChart() {
        $.ajax({
            url: 'http://student-scholarship.com/apis/updateseqDateApis.php',

            method: 'GET',
            success: function (result) {
                // var data = JSON.parse(result);
                // alert("xxxx");
                // if (action == "getNisit_scholarshipById") {
                //     if (data.nisit_scholarship_id != null) {
                //         setValueNisit_Scholarship(data);
                //     }
                // }
            }
        });
    }
</script>
<script>intervalChartRender()</script>
</body>
</html>
