<?php
session_start(); // เปิดใช้งาน session
require_once "../common.inc.php";
require_once "../connection.inc.php";
$user_id = $uprofile['id'];
$nisit_scholarship_id = getIsset('__nisit_scholarship_id');

$sql = "select nisit_scholarship.*,status_name,first_name,last_name,scholarship_name,scholarship_type_name,faculty_name,semester_name from nisit_scholarship
left join nisit on nisit.nisit_id=nisit_scholarship.nisit_id
left join scholarship on scholarship.scholarship_id=nisit_scholarship.scholarship_id
left join scholarship_type on scholarship_type.scholarship_type_id=scholarship.scholarship_type_id
left join faculty on faculty.faculty_id=scholarship.faculty_id
left join semester on semester.semester_id=scholarship.semester_id
left join status on status.status_id=nisit_scholarship.status_id
WHERE nisit_scholarship_id = '$nisit_scholarship_id'
 ";
$topic = $conn->queryRaw($sql, true);

?>
<!doctype html>
<html lang="en">
<head>

    <title><?php echo TITLE_ENG; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include "css.php";?>
</head>
<body>
<?php include "nav1.php" ?>
<div class="wrapper d-flex align-items-stretch">
    <?php include "nav.php"?>
    <!-- Page Content  -->
    <div id="content" class="p-4 p-md-5 pt-5">
        <section id="blog-list" class="container">
            <form class="form-horizontal" id="form_data" name="form_data" method="post"
                  enctype="multipart/form-data">
                <input id="__cmd" name="__cmd" type="hidden" value="">
                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="blog-item">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="blog-item-inner label-content-ckeditor">
                                        <h2>รหัสการขอทุน :<?php echo $topic['nisit_scholarship_id']; ?> <a href="list_scholarship.php"
                                                                                                           class="btn btn-default pull-right">Back</a>
                                        </h2>
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-6 control-label">
                                                    รหัสทุน :
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-control-static">
                                                <?php echo $topic['scholarship_id']; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-6 control-label">
                                                    ชื่อผู้ขอทุน :
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-control-static">
                                                <?php echo $topic['first_name'].' '.$topic['last_name']; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-6 control-label">
                                                    ชื่อทุน :
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-control-static">
                                                <?php echo $topic['scholarship_name']; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div align="left">
                                                <label class="col-sm-6 control-label">
                                                    คณะ :
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-control-static">
                                                <?php echo $topic['faculty_name']; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div align="left">
                                                <label class="col-sm-6 control-label">
                                                    ปีการศึกษา :
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-control-static">
                                                <?php echo $topic['semester_name']; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-6 control-label">
                                                    ประเภททุน :
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-control-static">
                                                <?php echo $topic['scholarship_type_name']; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-6 control-label">
                                                    สถานะ :
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-control-static">
                                                <?php echo $topic['status_name']; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-6 control-label">
                                                    วันที่ลงทะเบียน :
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-control-static">
                                                <?php echo $topic['register_date']; ?>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </form>

        </section>
    </div>
</div>
<?php include 'script.php'?>
<script>
    function goSavee() {
        alert("ยืนยันการทำรายการหรือไม่");
        $('input[name=__cmd]').val("save");
        $('#form_data').submit();
    }
</script>
</body>
</html>
