<?php
session_start(); // เปิดใช้งาน session
require_once "../common.inc.php";
require_once "../connection.inc.php";

?>
<!doctype html>
<html lang="en">
<head>

    <title><?php echo TITLE_ENG; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include "css.php";?>
</head>
<body>
<?php include "nav1.php" ?>
<div class="wrapper d-flex align-items-stretch">
    <?php include "nav.php"?>
    <!-- Page Content  -->
    <div id="content" class="p-4 p-md-5 pt-5">
        <section class="content-header">
            <h1>
                หน้าแรก
                <small>ติดต่อ</small>
            </h1>
            <ol class="breadcrumb">
            </ol>
        </section>
        <section id="blog-list" class="container">
            <div class="form-group">
                <div class="col-sm-8">
                    <div id="map" style="height: 400px"></div>
                </div>
                <br> <br> <br>
                <div class="col-sm-4">
                    <b>สถานที่ตั้ง</b>
                    <div id="text">มหาวิทยาลัยเทคโนโลยีพระจอมเกล้าพระนครเหนือ
                        1518 ถนนประชาราษฎร์ 1 แขวงวงศ์สว่าง เขตบางซื่อ
                        กรุงเทพมหานคร 10800</div>
                </div>
                <div class="col-sm-4">
                    <b>ติดต่อ</b>
                    <div id="text">โทรศัพท์ : 0-2555-2000</div>
                </div>
                <div class="col-sm-4">
                    <div id="text">แฟกซ์ : 0-2587-4350</div>
                </div>
                <div class="col-sm-4">
                    <div id="text">อีเมลติดต่อ : contact@op.kmutnb.ac.th</div>
                </div>

            </div>
            <input type="hidden" name="__lat" id="__lat" value="13.819048">
            <input type="hidden" name="__lng" id="__lng" value="100.514366">
        </section>
    </div>
</div>
<?php include 'script.php'?>
<script>
    function initMap() {
        var lat = getFloatValue("__lat");
        var lng = getFloatValue("__lng");
        console.log("lat",lat);
        console.log("lng",lng);
        var latLng = new google.maps.LatLng(lat, lng);
        var map = new google.maps.Map(document.getElementById('map'), {
            center: latLng,
            zoom: 18
        });
        var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            title: "<?php echo TITLE_ENG; ?>"
        });
        google.maps.event.addListener(map, 'click', function (event) {
            setLatLng(event.latLng.lat(), event.latLng.lng());
            marker.setMap(null);
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(event.latLng.lat(), event.latLng.lng()),
                map: map,
                title: "<?php echo TITLE_ENG; ?>"
            });
        });
    }
    function getFloatValue(id) {
        var value = parseFloat($("#" + id).val());
        if(isNaN(value)) value = 0.0;
        return value;
    }
    function setLatLng(lat,lng) {
        $('#__lat').val(lat);
        $('#__lng').val(lng);
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBA84Lz86jVp2J3kK9j4z1DUA68aJFEtjY&callback=initMap">
</script>
</body>
</html>
