<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">

<link rel="shortcut icon" href="../front/html/images/favicon.png"/>
<link href="../front/html/style.css" rel="stylesheet" type="text/css">
<!-- LOAD MAIN CSS -->
<!-- Date Picker -->
<link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<!-- Daterange picker -->
<link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">
<!-- LOAD COLOR STYLE (OPTONAL) CSS -->
<!--<link href="../front/html/styles/orange.css" rel="stylesheet" type="text/css" id="colorstyle">-->

<link href="../assets/dist/pagination.css" rel="stylesheet" type="text/css">
<!-- LOAD CUSTOM CSS -->
<!--<link href="../assets/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css">-->
<!--<link href="../plugins/iCheck/all.css" rel="stylesheet">-->

<style>

    #demo, .paginationjs {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        justify-content: center;
        align-items: center;
    }
    #sidebar .custom-menu .btn.btn-primary:after, #sidebar .custom-menu .btn.btn-primary:before {
        position: absolute;
        top: 8px;
        left: 0;
        right: 0;
        bottom: 0;
        font-family: "FontAwesome";
        color: gray;
    }

    #sidebar .custom-menu {
        background: grey!important;
        display: inline-block;
        position: absolute;
        top: 0px;
        right: 0;
        margin-right: -35px;
        -webkit-transition: 0.3s;
        -o-transition: 0.3s;
        transition: 0.3s;
    }

    .content {
        min-height: 100%;
        /*background-color: #ecf0f5;*/
        background: url('../uploads/unnameds.jpg') no-repeat center center;
        background-size: cover;
        z-index: 800;
    }
    .container {
        width: 100%;
    }
    .pb-4, .py-4 {
        padding-bottom: 4rem !important;
    }

    .pt-4, .py-4 {
        padding-top: 4rem !important;
    }

    .topnav {
        overflow: hidden;
        background-color: #f77100;
    }

    .topnav a {
        float: left;
        color: #f2f2f2;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
        font-size: 17px;
    }

    .topnav a:hover {
        background-color: #ddd;
        color: black;
    }

    .topnav a.active {
        background-color: #4CAF50;
        color: white;
    }

    .bg-wrap .user-logo .img {
        width: 100px;
        height: 100px;
        border-radius: 50%;
        /*margin-left: auto;*/
        margin-bottom: 10px;
        margin-top: 10px;
    }
    .bg-wrap .user-logo2 .img {
        width: 100px;
        height: 100px;
        border-radius: 50%;
        margin-left: auto;
        margin-bottom: 10px;
        margin-top: 10px;
    }
    .topnav2 {
        background-color: #32373d;
        height: 40px;

    }
    #content {
        min-height: calc(100vh - 170px) !important;
        background: gainsboro;
    }
    table th {
        text-align: left!important;
        background: red;
        color: white;
    }
    table td {
        text-align: left!important;
    }
</style>
