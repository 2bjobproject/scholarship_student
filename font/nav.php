<style>
#sidebar {
    min-width: 250px;
    max-width: 250px;
    background: grey!important;
    }
    #sidebar.active {
    margin-left: -250px;
}
}
</style>
    <nav id="sidebar">
    <div class="custom-menu">
        <button type="button" id="sidebarCollapse" class="btn btn-primary">
        </button>
    </div>
        <?php if($uprofile != null ){?>
    <div class="img bg-wrap text-center py-4" style="background-color: background: orange; /* For browsers that do not support gradients */
    background: -webkit-linear-gradient(orange, white); /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(orange, white); /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(orange, white); /* For Firefox 3.6 to 15 */
    background: linear-gradient(orange, white);">
        <div class="user-logo2">
            <img class="img" src="../uploads/<?php echo $uprofile['picture']?>"  style="margin: auto">
            <h3><?php echo $uprofile['name']?></h3>
        </div>
    </div>
        <?php } ?>
    <ul class="list-unstyled components mb-5">
        <li ><a href="index.php"><span class="fa fa-home mr-3"></span> หน้าหลัก</a></li>
<!--        <li ><a href="registor.php"><span class="fa fa-edit mr-3"></span> ลงทะเบียน</a></li>-->
<!--        <li><a href="scholarship_new_gen.php"><span class="fa fa-gift mr-3"></span>  ประกาศรับสมัครทุน</a></li>-->
        <?php if($uprofile == null && $uprofile['level'] != STUDENT_LEVEL){?>
            <li><a href="../admin/login.php"><span class="fa fa-sign-in mr-3"></span> เข้าสู่ระบบ</a></li>
<!--            <li><a href="contact_us.php"><span class="fa fa-support mr-3"></span> ติดต่อเรา</a></li>-->
        <?php }else{ ?>
            <li><a href="list_scholarship.php"><span class="fa fa-trophy mr-3"></span> ประวัติการขอทุน</a></li>
            <li><a href="nisit-update.php?__nisit_id=<?php echo $uprofile['id']; ?>"><span class="fa fa-cog mr-3"></span> อัปเดตข้อมูลส่วนตัว</a></li>
<!--            <li><a href="contact_us.php"><span class="fa fa-support mr-3"></span> ติดต่อเรา</a></li>-->
            <li><a href="../admin/logout.php"><span class="fa fa-sign-out mr-3"></span> ออกจากระบบ</a></li>
        <?php } ?>
<!--        <li>-->
<!--            <a href="index.php"><span class="fa fa-download mr-3 notif"><small-->
<!--                            class="d-flex align-items-center justify-content-center">5</small></span> Download</a>-->
<!--        </li>-->
<!---->
<!--        <li>-->
<!--            <a href="scholarship.php"><span class="fa fa-trophy mr-3"></span>ทุน</a>-->
<!--        </li>-->
<!--        <li>-->
<!--            <a href="contact_us.php"><span class="fa fa-cog mr-3"></span> ติดต่อเรา</a>-->
<!--        </li>-->
<!--        <li>-->
<!--            <a href="#"><span class="fa fa-support mr-3"></span> Support</a>-->
<!--        </li>-->
<!--        <li>-->
<!--            <a href="#"><span class="fa fa-sign-out mr-3"></span> Sign Out</a>-->
<!--        </li>-->
    </ul>

</nav> 