<?php
session_start(); // เปิดใช้งาน session
require_once "../common.inc.php"; //
require_once "../connection.inc.php"; //
$cmd = getIsset("__cmd");

$filterDefault = " where 1=1 ";

$keyword = getIsset("keyword");
if ($keyword != "") {
    $filterDefault .= " and " . 'scholarship_name' . " like '%" . $keyword . "%'";
}
$sql = "select scholarship.*,scholarship_type_name,semester_name from scholarship
left join scholarship_type on scholarship_type.scholarship_type_id=scholarship.scholarship_type_id  
left join semester on semester.semester_id=scholarship.semester_id";
$result_row = $conn->queryRaw($sql.$filterDefault);//คิวรี่ คำสั่ง
$total = sizeof($result_row);
$for_end = $limit;
$for_start = $start * $limit;

$select_all = $conn->queryRaw($sql . $filterDefault." order by seq desc, end_date desc limit " . $for_start . "," . $for_end);
$total_num = sizeof($select_all);
?>
<!doctype html>
<html lang="en">
<head>
    <title><?php echo TITLE_ENG; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php include "css.php"; ?>
    <style>
        .navbar-fixed-top, .navbar-fixed-bottom {
            position: fixed;
            right: 0;
            left: unset;
            z-index: 1030;
        }

    </style>
</head>
<body>
<?php include "nav1.php" ?>

<div class="wrapper d-flex align-items-stretch">
    <?php include "nav.php" ?>
    <!-- Page Content  -->
    <div id="content" class="p-4 p-md-5 pt-5">
        <section id="blog-list" class="container">
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" id="form_data" name="form_data" method="post">
                        <input id="__delete_field" name="__delete_field" type="hidden" value="">
                        <input id="__cmd" name="__cmd" type="hidden" value="">

                        <div class="col-md-12">
                            <label class="col-sm-3 control-label">
                            </label>
                        </div>
                        <div class="clr"></div>
                        <div class="col-sm-12">
                            <div class="box box-gold">

                                <div class="box-body">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class=" table-responsive">
                                                <table class="table table-hover tbgray" id="tbView">
                                                    <tr>
                                                        <!--                                                        <th width="5%">ลำดับ</th>-->
                                                        <!--                                                        <th width="20%">ชื่อผู้ขอทุน</th>-->

                                                        <th class="text-center" width="20%" nowrap="">ประกาศรับสมุครทุน</th>
                                                        <th class="" width="20%" nowrap="">ระยะเวลา</th>
                                                    </tr>
                                                    <tbody>
                                                    <?php
                                                    $index = $for_start;
                                                    foreach ($select_all

                                                    as $row) {

                                                    $fac = $conn->queryRaw("select scholarship_detail.*,faculty.faculty_name  from scholarship_detail
left join faculty on faculty.faculty_id=scholarship_detail.faculty_id where scholarship_id='" . $row['scholarship_id'] . "'");
                                                    $index++;
                                                    ?>
                                                    <tr>
                                                        <td class="active "
                                                            nowrap><?php echo $row['scholarship_name']; ?></td>

                                                        <td class="active ">
                                                            <?php echo $row["start_date"]; ?> -
                                                            <?php echo $row["end_date"]; ?>
                                                        </td>


                                                        <?php } ?>
                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-sm-12">
                                                <?php include "../admin/pageindex.php"; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
<!--    <div id="content" class="p-4 p-md-5 pt-5"-->
<!--         style="background: url('../assets/img/logo2.jpg') no-repeat center center ; background-size:cover;">-->
<!--    </div>-->
</div>


<?php include 'footer.php' ?>
<?php include 'script.php' ?>
</body>
</html>
