<?php
session_start(); // เปิดใช้งาน session
require_once "../common.inc.php";
require_once "../connection.inc.php";
$user_id = $uprofile['id'];
$scholarship_id = getIsset('scholarship_id');


$result = $conn->queryRaw("select * from nisit where nisit_id='$user_id'", true);


$sql = "select scholarship.*,scholarship_type_name,semester_name from scholarship
left join scholarship_type on scholarship_type.scholarship_type_id=scholarship.scholarship_type_id 
left join semester on semester.semester_id=scholarship.semester_id
where scholarship_id= '$scholarship_id'
 ";
$topic = $conn->queryRaw($sql, true);

$cmd = getIsset("__cmd");
if ($cmd == "save") {

    if ($user_id == null) {
        redirectTo("admin/login.php");
    } else {
        $value = array(
            "nisit_id" => $user_id,
            "scholarship_id" => $scholarship_id,
            "status_id" => 1,
            "register_date" => date("Y-m-d H:i:s"),
        );
        if ($conn->create("nisit_scholarship", $value)) {
//            $id = $conn->getLastInsertId();

            redirectTo("list_scholarship.php");
//            redirectTo("../admin/print-nisit_scholarship-update.php?__nisit_scholarship_id=".$id);
        }
    }
}
if ($cmd == "open") {
    redirectTo("nisit-update.php?__nisit_id=" . $uprofile['id']);
}
$fac = $conn->queryRaw("select scholarship_detail.*,faculty.faculty_name  from scholarship_detail
left join faculty on faculty.faculty_id=scholarship_detail.faculty_id where scholarship_id='" . $scholarship_id . "'");


$max = "select * from faculty";
$total = $conn->queryRaw($max);
?>
<!doctype html>
<html lang="en">
<head>

    <title><?php echo TITLE_ENG; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include "css.php"; ?>
</head>
<body>
<?php include "nav1.php" ?>
<div class="wrapper d-flex align-items-stretch">
    <?php include "nav.php" ?>
    <!-- Page Content  -->
    <div id="content" class="p-4 p-md-5 pt-5">
        <section id="blog-list" class="container">
            <form class="form-horizontal" id="form_data" name="form_data" method="post"
                  enctype="multipart/form-data">
                <input id="__cmd" name="__cmd" type="hidden" value="">
                <input id="__status_id" name="__status_id" type="hidden"
                       value="<?php echo $result['nisit_status_id'] ?>">
                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="blog-item">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="blog-item-inner label-content-ckeditor">
                                        <div class="form-group">
                                            <div align="left">
                                                <label class="col-sm-4 control-label">
                                                    ชื่อทุน :
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-control-static">
                                                <?php echo $topic['scholarship_name']; ?>
                                            </div>
                                        </div>
                                        <!--                                        <div class="form-group">-->
                                        <!--                                            <div align="left">-->
                                        <!--                                                <label class="col-sm-6 control-label">-->
                                        <!--                                                    คณะ :-->
                                        <!--                                                </label>-->
                                        <!--                                            </div>-->
                                        <!--                                            <div class="col-sm-6 form-control-static">-->
                                        <!--                                                --><?php //echo $topic['faculty_name']; ?>
                                        <!--                                            </div>-->
                                        <!--                                        </div>-->
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-4 control-label">
                                                    จำนวนคนที่รับ :
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-control-static">
                                                <?php echo $topic['amount_use']; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-4 control-label">
                                                    คณะ :
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-control-static">
                                                <?php if ($fac != null) {
                                                    $total1 = sizeof($total);
                                                    $total2 = sizeof($fac);
                                                    if ($total2 == $total1) {
                                                        echo "ทุกคณะ";
                                                    } else {
                                                        foreach ($fac as $i => $list) {
                                                            if ($i + 1 == sizeof($fac)) {
                                                                echo $list['faculty_name'];
                                                            } else {
                                                                echo $list['faculty_name'] . ",";
                                                            }
                                                        }
                                                    }
                                                }; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div align="left">
                                                <label class="col-sm-4 control-label">
                                                    ปีการศึกษา :
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-control-static">
                                                <?php echo $topic['semester_name']; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-4 control-label">
                                                    ประเภททุน :
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-control-static">
                                                <?php echo $topic['scholarship_type_name']; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-4 control-label">
                                                    ระยะเวลา :
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-control-static">
                                                <?php echo $topic["start_date"]; ?> -
                                                <?php echo $topic["end_date"]; ?>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-sm-8 control-label text-left">
                                            คุณสมบัติ :
                                        </label>
                                        <div class="col-sm-10 form-control-static">
                                            <?php echo $topic['property_detail']; ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <?php if ($uprofile != null) { ?>
                                        <div class="form-group">
                                            <a class="btn btn-sm btn-default"
                                               href="javascript:goSavee();">สมัครทุน</a>
                                        </div>
                                    <?php } ?>
                                    <div class="form-group">

                                        <a class="btn btn-sm btn-dark text-white"
                                           href="scholarship_new_gen.php">ย้อนกลับ</a>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </form>

        </section>
    </div>
</div>
<?php include 'footer.php' ?>
<?php include 'script.php' ?>
<script>
    function goSavee() {
        var txt;
        if ($("#__status_id").val() == 1) {
            if (confirm("กรุณาอัปเดตข้อมูลส่วนตัวก่อนทำการสมัครทุน")) {
                $('input[name=__cmd]').val("open");
                $('#form_data').submit();
            } else {
                txt = "Cancel!";
            }
        } else {
            if (confirm("กรุณาพิมพ์ใบสมัครทุนในหน้าประวัติการขอทุนส่งที่ทำการภาควิชาของนักศึกษา")) {
                $('input[name=__cmd]').val("save");
                $('#form_data').submit();
            } else {
                txt = "Cancel!";
            }
        }
    }


    // function goSavee() {
    //     alert("ยืนยันการทำรายการหรือไม่");
    //     $('input[name=__cmd]').val("save");
    //     $('#form_data').submit();
    // }
</script>
</body>
</html>
