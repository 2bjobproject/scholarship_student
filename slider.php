<section class="cd-hero">
    <ul class="list-unstyled cd-hero-slider autoplay">

        <li class="selected" style="background-image:url('front/imgdog/slider-bar1.jpg');">
            <div class="container">
<!--                <h1>อาบน้ำตัดขน</h1>-->
<!--                <h2>รายละเอียด อาบน้ำตัดขน</h2>-->
            </div>
        </li>

        <li style="background-image:url('front/imgdog/slider-bar2.jpg');">
            <div class="container">
<!--                <h1>ฝากเลี้ยง</h1>-->
<!--                <h2>รายละเอียด ฝากเลี้ยง</h2>-->
            </div>
        </li>

        <li style="background-image:url('front/imgdog/slider-bar3.jpg');">
            <div class="container">
<!--                <h1>รถรับส่ง</h1>-->
<!--                <h2>รายละเอียด รถรับส่ง</h2>-->
            </div>
        </li>

    </ul>
    <div class="arrow-nav">
        <div class="prev"><a class="hero-prev" href="#"><i class="fa fa-angle-left"></i></a></div>
        <div class="next"><a class="hero-next" href="#"><i class="fa fa-angle-right"></i></a></div>
    </div>
</section>

<div id="intro" class="container-fluid bg-dark padding15">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left">
                &nbsp;
            </div>
        </div>
    </div>
</div>
