<?php
session_start(); // เปิดใช้งาน session
require_once "common.inc.php";
require_once "connection.inc.php";

$option_faculty = getIsset("faculty_id");
$option_scholarship_type = getIsset("scholarship_type_id");
$filterDefault = " where 1=1";
if ($option_faculty != "") {
    $filterDefault .= " and " . 'scholarship.faculty_id' . " like '%" . $option_faculty . "%'";
}
if ($option_scholarship_type != "") {
    $filterDefault .= " and " . 'scholarship.scholarship_type_id' . " = '" . $option_scholarship_type . "'";
}

$sql = "select scholarship.*,faculty_name,scholarship_type_name,semester_name from scholarship
left join scholarship_type on scholarship_type.scholarship_type_id=scholarship.scholarship_type_id
left join faculty on faculty.faculty_id=scholarship.faculty_id
left join semester on semester.semester_id=scholarship.semester_id";
$result_row = $conn->queryRaw($sql . $filterDefault);//คิวรี่ คำสั่ง
$total = sizeof($result_row);
$for_end = $limit;
$for_start = $start * $limit;

$select_all = $conn->queryRaw($sql . $filterDefault . " order by scholarship_id desc  limit " . $for_start . "," . $for_end);
$total_num = sizeof($select_all);

$faculty = $conn->queryRaw("select *,(select count(*) from scholarship where scholarship.faculty_id=faculty.faculty_id) as scholarship_count from faculty");
$scholarship_type = $conn->queryRaw("select *,(select count(*) from scholarship where scholarship.scholarship_type_id=scholarship_type.scholarship_type_id) as scholarship_count from scholarship_type");
$count_scholarship = $conn->queryRaw("select count(*) as count from scholarship", true)['count'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- METAS -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- TITLE -->
    <title><?php echo TITLE_ENG; ?></title>
    <?php require_once "css.php"; ?>


</head>

<body>
<!-- Preloader Start -->
<div id="preloader">
    <i class="fa fa-spinner fa-spin preloader-animation" aria-hidden="true"></i>
</div>
<!-- Preloader End -->

<!-- WRAPPER START -->
<div id="wrapper">
    <!-- HEADER START -->
    <?php require_once "menu.php"; ?>
    <!-- HEADER END -->


    <!-- HERO SLIDER -->
    <?php require_once "slider.php"; ?>
    <!-- HERO SLIDER END-->


    <!-- CONTENT START -->
    <section id="content">
        <section id="blog-list" class="container">
            <form class="form-horizontal" id="form_data" name="form_data" method="post">
                <input type="hidden" name="__cmd" id="__cmd">
                <input type="hidden" name="faculty_id" id="faculty_id" value="<?php echo $option_faculty; ?>">
                <input type="hidden" name="scholarship_type_id" id="scholarship_type_id" value="<?php echo $option_scholarship_type; ?>">
                <div class="form-group">
                    <div class="col-sm-9 col-xs-12">
                        <?php
                        $index = $for_start;
                        foreach ($select_all as $row) {
                            $index++;
                            ?>
                            <div class="col-md-4 wow fadeInUp">
                                <div class="blog-item">
                                    <div class="blog-item-inner">
                                        <div class="blog-item-inner">
                                            <label>ชื่อทุน :</label>
                                            <?php echo $row["scholarship_name"]; ?>
                                        </div>
                                        <div class="blog-item-inner">
                                            <div class="ellipsis-2-line">
                                                <label>คณะ :</label>
                                                <?php echo $row["faculty_name"]; ?>
                                            </div>
                                        </div>
                                        <div class="blog-item-inner">
                                            <div class="ellipsis-2-line">
                                                <label>ประเภท :</label>
                                                <?php echo $row["scholarship_type_name"]; ?>
                                            </div>
                                        </div>
                                        <?php if ($uprofile != null) { ?>
                                            <div class="blog-item-inner">
                                                <a href="scholarship-detail.php?scholarship_id=<?php echo $row["scholarship_id"]; ?>"
                                                   class="btn btn-sm btn-default">สมัคร</a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="row blog-meta">
                                        <div class="col-xs-5 text-left"></i> <?php echo "รับ" . $row["amount_use"] . "คน"; ?></div>
                                        <div class="col-xs-7 text-right"></i> <?php echo $row["semester_name"]; ?></div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                    <div class="widget">
                        <h4>คณะ</h4>
                        <ul class="list-unstyled link-list">
                            <li>
                                <i class="fa fa-angle-right fa-fw" aria-hidden="true"></i>
                                <a href="javascript:search_faculty('');">
                                    <span class="pull-right">(<?php echo $count_scholarship; ?>)</span>
                                    <span class="one-line <?php echo $option_faculty == "" ? "text-bold" : ""; ?>">ทั้งหมด</span>
                                </a>
                            </li>
                            <?php foreach ($faculty as $item) { ?>
                                <li>
                                    <i class="fa fa-angle-right fa-fw" aria-hidden="true"></i>
                                    <a href="javascript:search_faculty('<?php echo $item['faculty_id']; ?>');">
                                        <span class="pull-right">(<?php echo $item['scholarship_count']; ?>)</span>
                                        <span
                                                class="one-line <?php echo $option_faculty == $item['faculty_id'] ? "text-bold" : ""; ?>"><?php echo $item['faculty_name']; ?></span>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="widget">
                        <h4>ประเภท</h4>
                        <ul class="list-unstyled link-list">
                            <li>
                                <i class="fa fa-angle-right fa-fw" aria-hidden="true"></i>
                                <a href="javascript:search_scholarship_type('');">
                                    <span class="pull-right">(<?php echo $count_scholarship; ?>)</span>
                                    <span class="one-line <?php echo $option_scholarship_type == "" ? "text-bold" : ""; ?>">ทั้งหมด</span>
                                </a>
                            </li>
                            <?php foreach ($scholarship_type as $item) { ?>
                                <li>
                                    <i class="fa fa-angle-right fa-fw" aria-hidden="true"></i>
                                    <a href="javascript:search_scholarship_type('<?php echo $item['scholarship_type_id']; ?>');">
                                        <span class="pull-right">(<?php echo $item['scholarship_count']; ?>)</span>
                                        <span
                                                class="one-line <?php echo $option_scholarship_type == $item['scholarship_type_id'] ? "text-bold" : ""; ?>"><?php echo $item['scholarship_type_name']; ?></span>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                        <div class="widget">
                            <a href="javascript:reset_search();" class="btn btn-default">Reset Search</a>
                        </div>
                    </div>
                </div>
            </form>
        </section>
        <div id="partners" class="container-fluid padding35">
            <div class="container">
                <div class="row wow fadeIn">

                </div>
            </div>
        </div>
    </section>
    <!-- CONTENT END -->
    <!-- FOOTER START -->
    <?php require_once "footer.php"; ?>
    <!-- FOOTER END -->
</div>
<!-- WRAPPER END -->

<!-- back to top button -->
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="เลื่อนขึ้น"
   data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>


<!-- SCRIPT START -->
<?php require_once "script.php"; ?>
<!-- SCRIPT END -->
<script>
    function search_scholarship_type(scholarship_type_id) {
        $("#scholarship_type_id").val(scholarship_type_id);
        $("#form_data").submit();
    }

    function search_faculty(faculty_id) {
        $("#faculty_id").val(faculty_id);
        $("#form_data").submit();
    }
    function reset_search() {
        $("#faculty_id").val('');
        $("#scholarship_type_id").val('');
        $("#form_data").submit();
    }

</script>
</body>
</html>

