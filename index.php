<?php
session_start(); // เปิดใช้งาน session
require_once "common.inc.php"; //
require_once "connection.inc.php"; //
redirect_to('font/index.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- METAS -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=""/>
    <meta name="author" content="">
    <!-- TITLE -->
    <title><?php echo TITLE_ENG; ?></title>
    <?php require_once "css.php"; ?>
</head>

<body>
<!-- Preloader Start -->
<div id="preloader">
    <i class="fa fa-spinner fa-spin preloader-animation" aria-hidden="true"></i>
</div>
<!-- Preloader End -->

<!-- WRAPPER START -->
<div id="wrapper">
    <!-- HEADER START -->
    <?php require_once "menu.php"; ?>
    <!-- HEADER END -->
    <!-- HERO SLIDER -->
    <?php require_once "slider.php"; ?>
    <!-- HERO SLIDER END-->
    <!-- CONTENT START -->

    <!-- CONTENT END -->

    <!-- FOOTER START -->
    <?php require_once "footer.php"; ?>
    <!-- FOOTER END -->


</div>
<!-- WRAPPER END -->

<!-- back to top button -->
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="เลื่อนขึ้น"
   data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>


<!-- SCRIPT START -->
<?php require_once "script.php"; ?>
<!-- SCRIPT END -->
<script>

</script>
</body>
</html>

