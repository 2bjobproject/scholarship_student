<?php
$cmd = getIsset('__filter');
$freturn = getIsset('__freturn');
$filter = array();
$filter['title'] = 'DEFAULT';
$filter['order_by'] = "";

if ($cmd == 'employee') {
    $filter['column'] = array('employee_id' => 'รหัส', 'first_name' => 'ชื่อ', 'last_name' => 'นามสกุล', 'username' => 'ชื่อผู้ใช้', 'phone' => 'เบอร์โทรศัพท์');
    $filter['sql'] = "select * from employee where 1=1 ";
    $filter['key_id'] = 'employee_id';
    $filter['title'] = 'ข้อมูลผู้ใช้งานขับรถ';
    $filter['options'] = array('employee_id' => 'รหัส', 'first_name' => 'ชื่อ', 'last_name' => 'นามสกุล', 'username' => 'ชื่อผู้ใช้', 'phone' => 'เบอร์โทรศัพท์');
}

if ($cmd == 'customer') {
    $filter['column'] = array('customer_id' => 'รหัส', 'first_name' => 'ชื่อ', 'last_name' => 'นามสกุล', 'username' => 'ชื่อผู้ใช้', 'phone' => 'เบอร์โทรศัพท์');
    $filter['sql'] = "select * from customer where 1=1 ";
    $filter['key_id'] = 'customer_id';
    $filter['title'] = 'ข้อมูลสมาชิก';
    $filter['options'] = array('customer_id' => 'รหัส', 'first_name' => 'ชื่อ', 'last_name' => 'นามสกุล', 'username' => 'ชื่อผู้ใช้', 'phone' => 'เบอร์โทรศัพท์');
}

if ($cmd == 'semester') {
    $filter['column'] = array('semester_id' => 'รหัส', 'semester_name' => 'ชื่อ');
    $filter['sql'] = "select * from semester where 1=1 ";
    $filter['key_id'] = 'semester_id';
    $filter['title'] = 'ข้อมูลปีการศึกษา';
    $filter['options'] = array('semester_id' => 'รหัส', 'semester_name' => 'ชื่อ');
}

if ($cmd == 'scholarship_type') {
    $filter['column'] = array('scholarship_type_id' => 'รหัส', 'scholarship_type_name' => 'ชื่อ');
    $filter['sql'] = "select * from scholarship_type where 1=1 ";
    $filter['key_id'] = 'scholarship_type_id';
    $filter['title'] = 'ข้อมูลประเภททุน';
    $filter['options'] = array('scholarship_type_id' => 'รหัส', 'scholarship_type_name' => 'ชื่อ');
}

if ($cmd == 'faculty') {
    $filter['column'] = array('faculty_id' => 'รหัส', 'faculty_name' => 'ชื่อ');
    $filter['sql'] = "select * from faculty where 1=1 ";
    $filter['key_id'] = 'faculty_id';
    $filter['title'] = 'ข้อมูลคณะ';
    $filter['options'] = array('faculty_id' => 'รหัส', 'faculty_name' => 'ชื่อ');
}

if ($cmd == 'department') {
    $filter['column'] = array('department_id' => 'รหัส', 'department_name' => 'ชื่อ');
    $filter['sql'] = "select * from department where 1=1 ";
    $filter['key_id'] = 'department_id';
    $filter['title'] = 'ข้อมูลสาขา';
    $filter['options'] = array('department_id' => 'รหัส', 'department_name' => 'ชื่อ');
}

if ($cmd == 'event') {
    $filter['column'] = array('event_id' => 'รหัส', 'event_name' => 'ชื่อ');
    $filter['sql'] = "select * from event where 1=1 ";
    $filter['key_id'] = 'event_id';
    $filter['title'] = 'ข้อมูลกิจกรรม';
    $filter['options'] = array('event_id' => 'รหัส', 'event_name' => 'ชื่อ');
}

if ($cmd == 'nisit') {
    $filter['column'] = array('nisit_id' => 'รหัส', 'nisit_code' => 'ชื่อ', 'first_name' => 'ชื่อ', 'last_name' => 'นามสกุล');
    $filter['sql'] = "select * from nisit where 1=1 ";
    $filter['key_id'] = 'nisit_id';
    $filter['title'] = 'ข้อมูลนักศึกษา';
    $filter['options'] = array('nisit_id' => 'รหัส', 'nisit_code' => 'ชื่อ', 'first_name' => 'ชื่อ', 'last_name' => 'นามสกุล');
}

if ($cmd == 'scholarship') {
    $filter['column'] = array('scholarship_id' => 'รหัส', 'scholarship_name' => 'ชื่อ',);
    $filter['sql'] = "select * from scholarship where 1=1 ";
    $filter['key_id'] = 'scholarship_id';
    $filter['title'] = 'ข้อมูลนักศึกษา';
    $filter['options'] = array('scholarship_id' => 'รหัส', 'scholarship_name' => 'ชื่อ');
}

if ($cmd == 'major') {
    $filter['column'] = array('major_id' => 'รหัส', 'major_name' => 'ชื่อ',);
    $filter['sql'] = "select * from major where 1=1 ";
    $filter['key_id'] = 'major_id';
    $filter['title'] = 'ข้อมูลmajor';
    $filter['options'] = array('major_id' => 'รหัส', 'major_name' => 'ชื่อ');
}

if ($cmd == 'section') {
    $filter['column'] = array('section_id' => 'รหัส', 'section_name' => 'ชื่อ',);
    $filter['sql'] = "select * from section where 1=1 ";
    $filter['key_id'] = 'section_id';
    $filter['title'] = 'ข้อมูลภาควิชา';
    $filter['options'] = array('section_id' => 'รหัส', 'section_name' => 'ชื่อ');
}

if ($cmd == 'province') {
    $filter['column'] = array('province_code' => 'รหัส', 'province_name' => 'ชื่อ');
    $filter['sql'] = "select * from province where 1=1 ";
    $filter['key_id'] = 'province_code';
    $filter['title'] = 'ข้อมูลจังหวัด';
    $filter['options'] = array('province_code' => 'รหัส', 'province_name' => 'ชื่อ');
}
if ($cmd == 'amphur') {
    $filter['column'] = array('amphur_code' => 'รหัส', 'amphur_name' => 'ชื่อ');
    $filter['sql'] = "select * from amphur where 1=1 ";
    $filter['key_id'] = 'amphur_code';
    $filter['title'] = 'ข้อมูลอำเภอ';
    $filter['options'] = array('amphur_code' => 'รหัส', 'amphur_name' => 'ชื่อ');
}
if ($cmd == 'district') {
    $filter['column'] = array('district_code' => 'รหัส', 'district_name' => 'ชื่อ');
    $filter['sql'] = "select * from district where 1=1 ";
    $filter['key_id'] = 'district_code';
    $filter['title'] = 'ข้อมูลตำบล';
    $filter['options'] = array('district_code' => 'รหัส', 'district_name' => 'ชื่อ');
}

if ($cmd == 'present_province') {
    $filter['column'] = array('province_code' => 'รหัส', 'province_name' => 'ชื่อ');
    $filter['sql'] = "select * from province where 1=1 ";
    $filter['key_id'] = 'province_code';
    $filter['title'] = 'ข้อมูลจังหวัด';
    $filter['options'] = array('province_code' => 'รหัส', 'province_name' => 'ชื่อ');
}
if ($cmd == 'present_amphur') {
    $filter['column'] = array('amphur_code' => 'รหัส', 'amphur_name' => 'ชื่อ');
    $filter['sql'] = "select * from amphur where 1=1 ";
    $filter['key_id'] = 'amphur_code';
    $filter['title'] = 'ข้อมูลอำเภอ';
    $filter['options'] = array('amphur_code' => 'รหัส', 'amphur_name' => 'ชื่อ');
}
if ($cmd == 'present_district') {
    $filter['column'] = array('district_code' => 'รหัส', 'district_name' => 'ชื่อ');
    $filter['sql'] = "select * from district where 1=1 ";
    $filter['key_id'] = 'district_code';
    $filter['title'] = 'ข้อมูลตำบล';
    $filter['options'] = array('district_code' => 'รหัส', 'district_name' => 'ชื่อ');
}
function get_filter($col, $value, $sql)
{
    return $sql . ' AND ' . $col . ' LIKE \'%' . $value . '%\' ';
}
