<?php
session_start();
require_once "../common.inc.php";
require_once '../connection.inc.php';
$cmd = getIsset('__cmd');
$test = array();
$nisit_id = getIsset('__nisit_id');
if ($cmd == "login") {
    if (getIsset('__password') != getIsset('__c_password')) {
        alertMassage("กรุณากรอกรหัสผ่านให้ตรงกัน");
    } else {
        $value = array(
            "title_id" => getIsset('__title_id'),
            "nisit_code" => getIsset('__nisit_code'),
            "id_card" => getIsset('__id_card'),
            "first_name" => getIsset('__first_name'),
            "last_name" => getIsset('__last_name'),
            "email" => getIsset('__email'),
            "phone" => getIsset('__phone'),
//            "password" => getIsset('__password'),
            "nisit_status_id" => 1

        );
        if ($conn->create("nisit", $value)) {
            $nisit_id = $conn->getLastInsertId();
        }
        $valuee = array(
            "faculty_id" => $faculty_id,
            "nisit_id" => $nisit_id
        );
        $conn->create("nisit_education", $value);
    }


    redirectTo("login.php");
}
?>
<html>
<head>
    <title><?php echo TITLE_ENG; ?></title>

    <!-- Meta -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Insight Station Network">
    <meta name="author" content="Insight Station Network">

    <?php require_once "css.php" ?>
</head>
<body style="margin:0px">
<div id="header" class="header navbar navbar-fixed-top navbar-success has-scroll">
    <!-- begin container-fluid -->
    <div class="container-fluid" style="border-bottom: 1px solid;height: 130px;">
        <!-- begin mobile sidebar expand / collapse button -->
        <div class="navbar-header" style="width: 100%">
            <a href="http://localhost/academic_rank/public/index" class="navbar-brand">
                <img src="../uploads/220px-KMUTNB-LOGO.jpg" style="width: 100px; height: 100px; border-radius: 50%;">
            </a><div style="line-height: 20px;padding-top: 40px;    padding-left: 120px;">
                <a style="color: white;font-size: 23px; "><b><?php echo TITLE_ENG; ?></b></a><br>
                <a style="color: white;font-size: 20px; "><b> <?php echo TITLE_THAI; ?></b></a>
            </div>

        </div>

    </div>
    <!-- end container-fluid -->
</div>
<div
        style="background: blue; /* For browsers that do not support gradients */
    background: -webkit-linear-gradient(blue, black); /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(blue, black); /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(blue, black); /* For Firefox 3.6 to 15 */
    background: linear-gradient(blue, black);;background-size:cover;width:100%;height:100%;position:absolute">
    <div style="max-width:800px;min-width: 250px;height:auto;margin:0 auto;padding:20px;position:relative;top:20%;background-color:rgba(0,0,0,0.5);border-radius:10px">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h5 style="color:white"><?php echo TITLE_ENG; ?></h5>
            </div>
        </div>

        <hr>

        <div id="UpdatePanel1">
            <form class="form-horizontal" method="post" id="form_login" name="form_login">
                <input type="hidden" name="__cmd" id="__cmd" value="login">
                <h3 style="color:white;margin-bottom:30px">สมัครสมาชิกใหม่</h3>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label" style="color: white;">เลข ปปช.</label>

                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="__id_card" id="__id_card"
                                       placeholder="ระบุตัวเลข 13 หลัก" onkeypress="chkInteger(event)" maxlength="13" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label" style="color: white;">รหัส นศ.</label>

                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="__nisit_code" id="__nisit_code" placeholder=""
                                       onkeypress="chkInteger(event)" maxlength="255" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label" style="color: white;">คำนำหน้า</label>
                            <div class="col-sm-5">
                                <select name="__title_id" id="__title_id" class="form-control">
                                    <?php
                                    $status = $conn->queryRaw('select * from title ');
                                    foreach ($status as $item) {
                                        ?>
                                        <option <?php echo getIsset("__title_id") == $item['title_id'] ? "selected" : ""; ?>
                                                value="<?php echo $item['title_id']; ?>"><?php echo $item['title_name']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="color: white;">ชื่อ</label>

                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="__first_name" id="__first_name" placeholder=""
                                       maxlength="255" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="color: white;">นามสกุล</label>

                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="__last_name" id="__last_name" placeholder=""
                                       maxlength="255" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="color: white;">คณะ</label>

                            <div class="col-sm-5">
                                <select name="__faculty_id" id="__faculty_id" class="form-control">
                                    <?php
                                    $status = $conn->queryRaw('select * from faculty ');
                                    foreach ($status as $item) {
                                        ?>
                                        <option <?php echo getIsset("__faculty_id") == $item['faculty_id'] ? "selected" : ""; ?>
                                                value="<?php echo $item['faculty_id']; ?>"><?php echo $item['faculty_name']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label" style="color: white;">อีเมล</label>

                            <div class="col-sm-9">
                                <input type="email" class="form-control" name="__email" id="__email" placeholder=""
                                       maxlength="255" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="color: white;">เบอร์โทรศัพท์</label>

                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="__phone" id="__phone" placeholder=""
                                       maxlength="10" onkeypress="chkInteger(event)" required>
                            </div>
                        </div>
                    </div>
                </div>

<!--                <div class="row">-->
<!--                    <div class="col-sm-6">-->
<!--                        <div class="form-group">-->
<!--                            <label class="col-sm-3 control-label" style="color: white;">รหัสผ่าน</label>-->
<!---->
<!--                            <div class="col-sm-9">-->
<!--                                <input type="text" class="form-control" name="__password" id="__password"-->
<!--                                       placeholder="เลข ปชช. 13 หลัก" maxlength="255" required>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-sm-6">-->
<!--                        <div class="form-group">-->
<!--                            <label class="col-sm-3 control-label" style="color: white;">ยืนยันรหัสผ่าน</label>-->
<!---->
<!--                            <div class="col-sm-9">-->
<!--                                <input type="text" class="form-control" name="__c_password" id="__c_password"-->
<!--                                       placeholder="ยืนยัน เลข ปชช. 13 หลัก" maxlength="255" required>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->

                <br>
                <div class="row">
                    <div class="col-md-6">
                        <a class="btn btn-default pull-left" href="../font/index.php">กลับหน้าหลัก</a>
                    </div>

                    <div class="col-md-6">
                        <button class="btn btn-info pull-right" type="submit">สมัครสมาชิก</button>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal flat" id="alertModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: white">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">x</span></button>
                <span>
                <span class="glyphicon glyphicon-info-sign" style="font-size:30px;color:#f2b712"></span>
                <span style="font-size: 20px;" id="alertTitle"></span></span>
            </div>
        </div>
    </div>
</div>
<?php require_once 'javascript.php'; ?>
<script>
    function showModal() {
        $('#alertTitle').text('<?php echo isset($message) ? $message : ""; ?>');
        $('#alertModal').modal();
    }
</script>
<?php if ($error_type) {
    echo '<script>showModal()</script>';
} ?>
</body>
</html>
