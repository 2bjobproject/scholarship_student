<?php session_start();
require_once "../common.inc.php";
require_once "../connection.inc.php";
$user_id = $uprofile['id'];
if (isset($_POST['__nisit_id'])) {
    $nisit_id = $_POST['__nisit_id'];
} else {
    $nisit_id = "";
}
$approve_date =  date('Y-m-d') ;
if (isset($_GET['__nisit_scholarship_id'])) {
    if ($_GET['status_id'] == 1) {
        $status = 2;
    } else {
        $status = 3;
    }
    $value = array(
        "status_id" => $status,
        "employee_id" => $user_id,
        "approve_date" =>$approve_date
    );
    $insert_result = $conn->update("nisit_scholarship", $value,array('nisit_scholarship_id'=>$_GET['__nisit_scholarship_id']));
    echo "<script language='javascript'>";
    echo " alert('อัพเดทเรียบร้อยแล้ว'); ";
    echo " top.window.location = 'check_scholarship.php'; ";
    echo "</script>";
}
