<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(ADMIN_LEVEL, STAFF_LEVEL)))
    redirect_to('index.php');

require_once "../connection.inc.php";

$cmd = getIsset("__cmd");
if ($cmd == "delete") {
    $conn->delete("scholarship", array("scholarship_id" => getIsset('__delete_field')));
    redirectTo('scholarship.php');
}
$filterDefault = " where 1=1 ";

$keyword = getIsset("keyword");
$option_val = getIsset("option");
if ($keyword != "") {
    $filterDefault .= " and " . $option_val . " like '%" . $keyword . "%'";
}
$sql = "select scholarship.*,scholarship_type_name,semester_name from scholarship
left join scholarship_type on scholarship_type.scholarship_type_id=scholarship.scholarship_type_id 
left join semester on semester.semester_id=scholarship.semester_id 
";
$result_row = $conn->queryRaw($sql . $filterDefault);//คิวรี่ คำสั่ง
$total = sizeof($result_row);
$for_end = $limit;
$for_start = $start * $limit;

$select_all = $conn->queryRaw($sql . $filterDefault . " order by semester_id desc , scholarship_id desc  limit " . $for_start . "," . $for_end);
$total_num = sizeof($select_all);

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php require_once "css.php" ?>
</head>
<body class="skin-custom sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                จัดการข้อมูลทุน
            </h1>

        </section>
        <!-- Main content -->
        <section class="content">
            <form class="form-horizontal" id="form_data" name="form_data" method="post">
                <input id="__delete_field" name="__delete_field" type="hidden" value="">
                <input id="__cmd" name="__cmd" type="hidden" value="">
                <div class="box box-custom">

                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-7">
                                <?php if ($uprofile['level'] == ADMIN_LEVEL) { ?>
                                    <a class="btn btn-primary" href="scholarship-update.php"><i
                                                class="fa fa-plus"></i> เพิ่มข้อมูล</a>
                                <?php } ?>
                            </div>
                            <div class="col-sm-2">
                                <select id="option" name="option" class="form-control">
                                    <option value="scholarship_name" <?php echo getIsset("option") == 'scholarship_name' ? "selected" : ""; ?>>ทุน</option>
                                    <option value="scholarship_type_name" <?php echo getIsset("option") == 'scholarship_type_name' ? "selected" : ""; ?>>ประเภททุน</option>
                                    <option value="faculty_name" <?php echo getIsset("option") == 'faculty_name' ? "selected" : ""; ?>>คณะ</option>
                                    <option value="semester_name" <?php echo getIsset("option") == 'semester_name' ? "selected" : ""; ?>>ปีการศึกษา</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <input class="form-control" type="text" id="keyword" name="keyword"
                                           onblur="trimValue(this)" onkeyup="goSearch()"
                                           value="<?php echo $keyword; ?>">
                                    <a href="javascript:goSearch();"
                                       class="btn btn-default input-group-addon"><i
                                                class="fa fa-search"></i> </a>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class=" table-responsive">
                                    <table class="table table-bordered table-striped" id="tbView">
                                        <tr>
                                            <th class="text-center" width="5%">ลำดับ</th>
                                            <th class="text-center" width="15%">ชื่อทุน</th>
                                            <th class="text-center" width="10%">จำนวนคน</th>
                                            <th class="text-center" width="10%">ปีการศึกษา</th>
                                            <th class="text-center" width="10%">ประเภททุน</th>
                                            <th class="text-center" width="10%">คณะ</th>
                                            <th  class="text-center" width="10%" class="text-center"></th>
                                        </tr>
                                        <tbody>
                                        <?php
                                        $index = $for_start;
                                        foreach ($select_all as $row) {
                                            $fac = $conn->queryRaw("select scholarship_detail.*,faculty.faculty_name from scholarship_detail
left join faculty on faculty.faculty_id=scholarship_detail.faculty_id where scholarship_id='" . $row['scholarship_id'] . "'");


                                            $index++;
                                            ?>
                                            <tr>
                                                <td class="text-center"
                                                    nowrap><?php echo $index; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['scholarship_name']; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['amount_use']; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['semester_name']; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['scholarship_type_name']; ?></td>
                                                <td class=""
                                                    nowrap>
                                                    <?php if ($fac != null) {
                                                        foreach ($fac as $i => $list) {
                                                            if($i+1 == sizeof($fac)){
                                                                echo $list['faculty_name'] ;
                                                            }else{
                                                                echo $list['faculty_name'] . ' ,<br>';
                                                            }
                                                        }
                                                    }; ?></td>
                                                <td class="text-center" nowrap>
                                                    <div class="btn-group">
                                                        <?php if ($uprofile['level'] == STAFF_LEVEL) { ?>
                                                            <a class="btn btn-primary btn-xs" title="Print"
                                                               href="scholarship-view.php?__scholarship_id=<?php echo $row['scholarship_id']; ?>"><i
                                                                        class="fa fa-print"></i> </a>
                                                        <?php } ?>
                                                        <?php if ($uprofile['level'] == ADMIN_LEVEL) { ?>
                                                            <a class="btn btn-warning btn-xs" title="แก้ไข"
                                                               href="scholarship-update.php?__scholarship_id=<?php echo $row['scholarship_id']; ?>"><i
                                                                        class="fa fa-edit"></i> </a>
                                                            <a class="btn btn-danger btn-xs" title="ลบ"
                                                               onclick="deleteRowData('<?php echo $row['scholarship_id']; ?>')"><i
                                                                        class="fa fa-trash"></i> </a>
                                                        <?php } ?>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-12">
                                    <?php include "pageindex.php"; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <?php include "footer.php" ?>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>
    $('#menu-scholarship_type-main').addClass('active');
    $('#menu-scholarship').addClass('active');
</script>
</body>
</html>


