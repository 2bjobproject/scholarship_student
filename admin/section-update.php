<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(ADMIN_LEVEL)))
    redirect_to('index.php');

require_once "../connection.inc.php";

$cmd = getIsset("__cmd");
$test = array();
$section_id = getIsset('__section_id');
if ($cmd == "save") {
    $value = array(
        "section_name" => getIsset('__section_name'),
        "section_short" => getIsset('__section_short'),
        "department_id" => getIsset('__department_id'),
    );
    if ($section_id == "0") {
        if ($conn->create("section", $value)) {
            redirectTo("section.php");
        }

    } else {
        if ($conn->update("section", $value, array("section_id" => $section_id))) {
            redirectTo("section.php");
        }
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php include "css.php" ?>

</head>
<body class="skin-custom sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                จัดการข้อมูลสาขา
            </h1>

        </section>
        <section class="content">
            <form class="form-horizontal" id="form_data" name="form_data" method="post" enctype="multipart/form-data">
                <input id="__cmd" name="__cmd" type="hidden" value="">
                <div class="box box-custom">
                    <div class="box-body">
                        <input type="hidden" name="__section_id" id="__section_id" class="form-control"
                               value="0"
                               required="true" readonly>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    ชื่อสาขา :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="__section_name" id="__section_name"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    ชื่อย่อ :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="__section_short" id="__section_short"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    ภาควิชา :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <input type="hidden" name="__department_id" id="__department_id"
                                           class="form-control"
                                           value=""
                                           readonly>
                                    <input type="text" name="__department_name" id="__department_name"
                                           class="form-control"
                                           value="" readonly required>
                                    <a href="javascript:goPage('linkhelp.php?__filter=department&__action=getDepartmentById');"
                                       class="btn btn-default input-group-addon"><i
                                            class="fa fa-search"></i> </a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <a class="btn btn-success" href="javascript:goSave();">บันทึก</a>
                                <a class="btn btn-warning" href="javascript:goClear()">ล้าง</a>
                                <a class="btn btn-default" href="section.php">ย้อนกลับ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <?php include "footer.php" ?>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>
    $('#menu-fac-main').addClass('active');
    $('#menu-section').addClass('active');

    function helpReturn(value, action) {
        $.ajax({
            url: 'Allservice.php',
            data: {id: value, action: action},
            method: 'GET',
            success: function (result) {
                var data = JSON.parse(result);
                if (action == "getSectionById") {
                    if (data.section_id != null) {
                        console.log(data);
                        setValueSection(data);
                    }
                }
                if (action == "getDepartmentById") {
                    if (data.department_id != null) {
                        console.log(data);
                        setValueDepartment(data);
                    }
                }
            }
        });
    }

    function setValueSection(data) {
        with (document.form_data) {
            $("#__section_id").val(data.section_id);
            $("#__section_name").val(data.section_name);
            $("#__section_short").val(data.section_short);
            $("#__department_id").val(data.department_id);
            $("#__department_name").val(data.department_name);
        }
    }
    function setValueDepartment(data) {
        with (document.form_data) {
            $("#__department_id").val(data.department_id);
            $("#__department_name").val(data.department_name);
            $("#__department_short").val(data.department_short);

        }
    }
</script>
<script>helpReturn('<?php echo $section_id;?>', 'getSectionById')</script>
</body>
</html>


