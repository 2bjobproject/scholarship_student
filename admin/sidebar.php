<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header"></li>

            <li class="">
                <a href="index.php">
                    <i class="fa fa-home"></i> <span>หน้าหลัก</span>
                </a>
            </li>
            <?php if ($uprofile['level'] == ADMIN_LEVEL) { ?>
                <li class="treeview" id="menu-user-main">
                    <a href="#">
                        <i class="fa fa-folder-open-o"></i> <span>ผู้ใช้งานระบบ</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li id="menu-employee"><a href="employee.php"><i class="fa fa-circle-o"></i>
                                จัดการข้อมูลผู้ใช้งาน</a></li>
                    </ul>
                </li>
            <?php } ?>
            <li class="treeview" id="menu-fac-main">
                <a href="#">
                    <i class="fa fa-folder-open-o"></i> <span>คณะ</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li id="menu-faculty"><a href="faculty.php"><i class="fa fa-circle-o"></i> จัดการข้อมูลคณะ</a></li>

                    <li id="menu-department"><a href="department.php"><i class="fa fa-circle-o"></i> จัดการข้อมูลภาควิชา</a>
                    </li>

                    <li id="menu-section"><a href="section.php"><i class="fa fa-circle-o"></i> จัดการข้อมูลสาขา</a></li>
                </ul>
            </li>
            <li class="treeview" id="menu-major-main">
                <a href="#">
                    <i class="fa fa-folder-open-o"></i> <span>ระดับการศึกษา</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li id="menu-major"><a href="major.php"><i class="fa fa-circle-o"></i> จัดการข้อมูลระดับการศึกษา</a>
                    </li>
                </ul>
            </li>
            <li class="treeview" id="menu-semester-main">
                <a href="#">
                    <i class="fa fa-folder-open-o"></i> <span>ปีการศึกษา</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li id="menu-semester"><a href="semester.php"><i class="fa fa-circle-o"></i> จัดการข้อมูลปีการศึกษา</a>
                    </li>
                </ul>
            </li>

                <li class="treeview" id="menu-scholarship_type-main">
                    <a href="#">
                        <i class="fa fa-folder-open-o"></i> <span>ทุน</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li id="menu-scholarship_type"><a href="scholarship_type.php"><i class="fa fa-circle-o"></i>
                                จัดการข้อมูลประเภททุน</a></li>
                        <li id="menu-scholarship"><a href="scholarship.php"><i class="fa fa-circle-o"></i>จัดการข้อมูลทุน</a>
                        </li>
                    </ul>
                </li>

            <li class="treeview" id="menu-scholarship-main">
                <a href="#">
                    <i class="fa fa-folder-open-o"></i> <span>รายการขอทุน</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <?php if ($uprofile['level'] == ADMIN_LEVEL) { ?>
                        <li id="menu-nisit_scholarship">
                            <a href="check_scholarship.php"><i class="fa fa-circle-o"></i>รออนุมัติทุน</a></li>
                    <?php } ?>
                    <!--                    <li id="menu-event-">-->
                    <!---->
                    <!--                        <a href="nisit_event-detail.php"><i class="fa fa-circle-o"></i>บันทึกกิจกรรมนักศึกษาทุน</a></li>-->
                    <li id="menu-list-">
                        <a href="list_nisit_event-detail.php"><i class="fa fa-circle-o"></i>ประวัติการขอรับทุน</a></li>
                    <!--                    <li id="menu-list-sc-">-->
                    <!--                        <a href="scholarship-nisit.php"><i class="fa fa-circle-o"></i>ผู้ที่ได้รับทุน</a></li>-->
                </ul>
            </li>


            <!--            <li class="treeview" id="menu-event-main">-->
            <!--                <a href="#">-->
            <!--                    <i class="fa fa-folder-open-o"></i> <span>กิจกรรม</span>-->
            <!--                    <span class="pull-right-container">-->
            <!--              <i class="fa fa-angle-left pull-right"></i>-->
            <!--            </span>-->
            <!--                </a>-->
            <!--                <ul class="treeview-menu">-->
            <!--                    <li id="menu-event"><a href="event.php"><i class="fa fa-circle-o"></i>จัดการข้อมูลกิจกรรม</a></li>-->
            <!--                </ul>-->
            <!--            </li>-->

                <li class="treeview" id="menu-nisit-main">
                    <a href="#">
                        <i class="fa fa-folder-open-o"></i> <span>นักศึกษา</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
<!--                        --><?php //if ($uprofile['level'] == ADMIN_LEVEL) { ?>
<!--                            <li id="menu-new-nisit"><a href="new-nisit.php"><i class="fa fa-circle-o"></i>เพิ่มนักศึกษาใหม่</a>-->
<!--                            </li>-->
<!--                        --><?php //} ?>
                        <li id="menu-nisit"><a href="nisit.php"><i class="fa fa-circle-o"></i>จัดการข้อมูลนักศึกษา</a>
                        </li>
                    </ul>
                </li>



            <li class="treeview" id="menu-report-main">
                <a href="#">
                    <i class="fa fa-folder-open-o"></i> <span>รายงาน</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <!--                    <li id="menu-report-"><a href="report_scholarship.php"><i class="fa fa-circle-o"></i>รายงานทุนแต่ละปีการศึกษา</a></li>-->
                    <li id="menu-report"><a href="report_income.php"><i class="fa fa-circle-o"></i>รายงานนักศึกษาทุน</a>
                      </ul>
            </li>
            <li id="menu-report2">
                <a href="statistics.php">
                      <span>สถิติ</span>
                </a>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
