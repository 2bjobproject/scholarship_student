<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(ADMIN_LEVEL)))
    redirect_to('index.php');
require_once "../connection.inc.php";

$cmd = getIsset("__cmd");
$test = array();
$nisit_id = getIsset('__nisit_id');
if ($cmd == "save") {
    $value = array(
        "nisit_code" => getIsset('__nisit_code'),
        "first_name" => getIsset('__first_name'),
        "last_name" => getIsset('__last_name'),
        "id_card" => getIsset('__id_card'),
        "title_id" => getIsset('__title_id'),
        "birth_date" => getIsset('__birth_date'),
        "password" => getIsset("__password"),
    );
    if ($conn->create("nisit", $value)) {
        $nisit_id = $conn->getLastInsertId();
    }
    redirectTo("nisit.php");
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php include "css.php" ?>

</head>
<body class="skin-custom sidebar-mini" onload="hiddenn('0')">

<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                จัดการข้อมูลนักศึกษา
            </h1>

        </section>
        <section class="content">
            <form class="form-horizontal" id="form_data" name="form_data" method="post" enctype="multipart/form-data">
                <input id="__cmd" name="__cmd" type="hidden" value="">
                <div class="box box-custom">
                    <div class="box-body">
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    รหัสนักศึกษา :
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="__nisit_code" id="__nisit_code"
                                       class="form-control"
                                       value=""
                                       maxlength="15" required
                                       onkeypress="chkInteger(event)">
                            </div>
                            <div align="right">
                                <label class="col-sm-2 control-label">
                                    คำนำหน้า :
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <select name="__title_id" id="__title_id" class="form-control">
                                    <?php
                                    $level = $conn->select('title');
                                    foreach ($level as $type) {
                                        ?>
                                        <option
                                            value="<?php echo $type['title_id']; ?>">
                                            <?php echo $type['title_name']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    ชื่อ :
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="__first_name" id="__first_name"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true">
                            </div>
                            <div align="right">
                                <label class="col-sm-2 control-label">
                                    นามสกุล :
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="__last_name" id="__last_name"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    เลขบัตรประจำตัวประชาชน :
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="__id_card" id="__id_card"
                                       class="form-control" value=""
                                       maxlength="13" required
                                       onkeypress="chkInteger(event)">
                            </div>

                            <div align="right">
                                <label class="col-sm-2 control-label">
                                    วันเกิด:
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="__birth_date"
                                       id="__birth_date"
                                       class="form-control"
                                       value="" readonly
                                       required>
                            </div>
                        </div>
                        <div class="form-group">

                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    รหัสผ่าน:
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <input type="password" name="__password"
                                       id="__password"
                                       class="form-control"
                                       value=""
                                       required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-5 control-label">
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <a class="btn btn-success" href="javascript:goSave();">บันทึก</a>
                                <a class="btn btn-warning" href="javascript:goClear()">ล้าง</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <?php include "footer.php" ?>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>
    $('#menu-nisit-main').addClass('active');
    $('#menu-new-nisit').addClass('active');
    $('#__birth_date').datetimepicker({
        datepicker: true,
        timepicker: false,
        format: 'Y-m-d',
        closeOnDateSelect: true
    });

</script>
</body>
</html>


