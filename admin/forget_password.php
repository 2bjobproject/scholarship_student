<?php
session_start();
require_once "../common.inc.php";
require_once '../connection.inc.php';
$cmd = getIsset('__cmd');
$error_type = false;
if ($cmd == "login") {
    $mail1 = getIsset('__email');
//    $id_card = (getIsset('__id_card'));
    $nisit_code = (getIsset('__nisit_code'));

    $chk_login = $conn->select('nisit', array('nisit_code' => $nisit_code), true);
    if ($chk_login == null) {
        alertMassage("ไม่มีผู้ใช่นี้ในระบบ");
    } else {

        try {
            require '../plugins/PHPMailer/PHPMailerAutoload.php';
            $mail = new PHPMailer;
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            $mail->isSMTP();
            $mail->CharSet = "utf-8";
            $mail->Host = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->SMTPKeepAlive = true;
            $mail->Port = 587;
            $mail->Username = 'katejeerawontestsendmail@gmail.com';
            $mail->Password = 'katejeerawon1234';
            $mail->setFrom('katejeerawontestsendmail@gmail.com', "Mail Testing");
            $mail->Subject = 'Your Username Password';
            $mail->addAddress($chk_login['email'], $chk_login['first_name'] . ' ' . $chk_login['last_name']);
            $mail->msgHTML("Username =" . $chk_login['nisit_code'] . "<br>Password =" . $chk_login['id_card'] . "");

            if ($mail->send()) {
                $mail->clearAddresses();
                $mail->clearAttachments();
                alertMassage('ส่งสำเร็จ');
            } else {
                alertMassage('ส่งไม่สำเร็จ');
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}

?>
<html>
<head>
    <title><?php echo TITLE_ENG; ?></title>

    <!-- Meta -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Insight Station Network">
    <meta name="author" content="Insight Station Network">

    <?php require_once "css.php" ?>
    <style>
        img.op {
            opacity: 1;
        }
    </style>
</head>
<body style="margin:0px">
<div id="header" class="header navbar navbar-fixed-top navbar-success has-scroll">
    <!-- begin container-fluid -->
    <div class="container-fluid" style="    border-bottom: 1px solid;height: 130px;">
        <!-- begin mobile sidebar expand / collapse button -->
        <div class="navbar-header" style="width: 100%">
            <a href="../font/index.php" class="navbar-brand">
                <img src="../uploads/220px-KMUTNB-LOGO.jpg" style="width: 100px; height: 100px; border-radius: 50%;">
            </a>
            <div style="line-height: 20px;padding-top: 40px;    padding-left: 120px;">
                <a style="color: #E67E22;font-size: 18px; "><b><?php echo TITLE_ENG; ?></b></a><br>
                <a style="color: #E67E22;font-size: 18px; "><b> <?php echo TITLE_THAI; ?></b></a>
            </div>

        </div>

    </div>
    <!-- end container-fluid -->
</div>
<img class="op" src="../assets/img/logo2.jpg" style="background-size:cover;width:100%;height:100%;position:absolute">
<div style="max-width:400px;min-width: 250px;height:auto;margin:0 auto;padding:20px;position:relative;top:20%;background-color:rgba(0,0,0,0.5);border-radius:10px">
    <div class="row">
        <div class="col-xs-12 text-center">
            <h4 style="color:white">ลืมรหัสผ่าน</h4>
        </div>
    </div>

    <hr>

    <div id="UpdatePanel1">
        <form class="form-horizontal" method="post" id="form_login" name="form_login">
            <input type="hidden" name="__cmd" id="__cmd" value="login">

<!--            <div class="form-group">-->
<!--                <label for="inputEmail3" class="col-sm-3 control-label" style="color: white;">เลข ปปช.</label>-->
<!---->
<!--                <div class="col-sm-9">-->
<!--                    <input type="text" class="form-control" name="__id_card" id="__id_card"-->
<!--                           placeholder="ระบุตัวเลข 13 หลัก" onkeypress="chkInteger(event)" maxlength="13">-->
<!--                </div>-->
<!--            </div>-->
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label" style="color: white;">รหัส นศ.</label>

                <div class="col-sm-9">
                    <input type="text" class="form-control" name="__nisit_code" id="__nisit_code" placeholder=""
                           onkeypress="chkInteger(event)" maxlength="255">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" style="color: white;">ชื่อ</label>

                <div class="col-sm-9">
                    <input type="text" class="form-control" name="__first_name" id="__first_name" placeholder=""
                           maxlength="255">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" style="color: white;">นามสกุล</label>

                <div class="col-sm-9">
                    <input type="text" class="form-control" name="__last_name" id="__last_name" placeholder=""
                           maxlength="255">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label" style="color: white;">อีเมล</label>

                <div class="col-sm-9">
                    <input type="email" class="form-control" name="__email" id="__email" placeholder="" maxlength="255"
                           required>
                </div>
            </div>

            <!--                <div class="input-group" style="margin-bottom:20px;">-->
            <!--                    <span class="input-group-addon"><i class="fa fa-mail-forward"></i></span>-->
            <!--                    <input name="__email" type="text" maxlength="255" id="__email" autofocus-->
            <!--                           class=" form-control input-sm " placeholder="Email" required>-->
            <!--                </div>-->
            <!---->
            <!--                <div class="input-group" style="margin-bottom:20px;">-->
            <!--                    <span class="input-group-addon"><i class="fa fa-mail-forward"></i></span>-->
            <!--                    <input name="__email2" type="text" maxlength="255" id="__email2"-->
            <!--                           class=" form-control input-sm " placeholder="Confirm Email" required>-->
            <!--                </div>-->

            <div class="row">
                <div class="col-md-6">
                    <a class="btn btn-default pull-left" href="login.php">ย้อนกลับ</a>
                </div>

                <div class="col-md-6">
                    <button class="btn btn-info pull-right" type="submit">ยืนยัน</button>

                </div>
            </div>
        </form>
    </div>
</div>
</div>
<div class="modal flat" id="alertModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: white">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">x</span></button>
                <span>
                <span class="glyphicon glyphicon-info-sign" style="font-size:30px;color:#f2b712"></span>
                <span style="font-size: 20px;" id="alertTitle"></span></span>
            </div>
        </div>
    </div>
</div>
<?php require_once 'javascript.php'; ?>
<script>
    function showModal() {
        $('#alertTitle').text('<?php echo isset($message) ? $message : ""; ?>');
        $('#alertModal').modal();
    }
</script>
<?php if ($error_type) {
    echo '<script>showModal()</script>';
} ?>
</body>
</html>
