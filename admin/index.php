<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(ADMIN_LEVEL,STAFF_LEVEL)))
    redirect_to('login.php');

require_once "../connection.inc.php";

$filterDefault = " where 1=1 ";

$keyword = getIsset("keyword");
if ($keyword != "") {
    $filterDefault .= " and " . 'scholarship_name' . " like '%" . $keyword . "%'";
}
$sql = "select scholarship.*,scholarship_type_name,semester_name from scholarship
left join scholarship_type on scholarship_type.scholarship_type_id=scholarship.scholarship_type_id  
left join semester on semester.semester_id=scholarship.semester_id";
$result_row = $conn->queryRaw($sql.$filterDefault);//คิวรี่ คำสั่ง
$total = sizeof($result_row);
$for_end = $limit;
$for_start = $start * $limit;

$select_all = $conn->queryRaw($sql . $filterDefault." order by seq desc, end_date desc limit " . $for_start . "," . $for_end);
$total_num = sizeof($select_all);
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php require_once 'css.php' ?>
</head>
<body class="skin-custom sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="background: url('../assets/img/logo2.jpg') no-repeat center center ; background-size:cover;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 class="text-white">
                หน้าหลัก
            </h1>
        </section>

        <!-- Main content -->
        <section id="blog-list" class="container">
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" id="form_data" name="form_data" method="post">
                        <input id="__delete_field" name="__delete_field" type="hidden" value="">
                        <input id="__cmd" name="__cmd" type="hidden" value="">

                        <div class="col-md-12">
                            <label class="col-sm-3 control-label">
                            </label>
                        </div>
                        <div class="clr"></div>
                        <div class="col-sm-12">
                            <div class="box box-custom">

                                <div class="box-body" style="padding: 0">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class=" table-responsive">
                                                <table class="table table-hover tbgray" id="tbView">
                                                    <tr>
                                                        <!--                                                        <th width="5%">ลำดับ</th>-->
                                                        <!--                                                        <th width="20%">ชื่อผู้ขอทุน</th>-->

                                                        <th class="text-center" width="20%" nowrap="">ประกาศรับสมุครทุน</th>
                                                        <th class="" width="20%" nowrap="">ระยะเวลา</th>
                                                    </tr>
                                                    <tbody>
                                                    <?php
                                                    $index = $for_start;
                                                    foreach ($select_all

                                                    as $row) {

                                                    $fac = $conn->queryRaw("select scholarship_detail.*,faculty.faculty_name  from scholarship_detail
left join faculty on faculty.faculty_id=scholarship_detail.faculty_id where scholarship_id='" . $row['scholarship_id'] . "'");
                                                    $index++;
                                                    ?>
                                                    <tr>
                                                        <td class="active "
                                                            nowrap><?php echo $row['scholarship_name']; ?></td>

                                                        <td class="active ">
                                                            <?php echo $row["start_date"]; ?> -
                                                            <?php echo $row["end_date"]; ?>
                                                        </td>


                                                        <?php } ?>
                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-sm-12">
                                                <?php include "../admin/pageindex.php"; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include "footer.php" ?>
</div>
<!-- ./wrapper -->
<?php require_once 'javascript.php' ?>
<!-- Page script -->
<script>

</script>
</body>
</html>


