<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(ADMIN_LEVEL)))
    redirect_to('index.php');

require_once "../connection.inc.php";

$cmd = getIsset("__cmd");
if ($cmd == "delete") {
    $conn->delete("nisit_event", array("nisit_event_id" => getIsset('__delete_field')));
    redirectTo('nisit_event.php');
}
$filterDefault = " where 1=1 ";

$keyword = getIsset("keyword");
$option_val = getIsset("option");
if ($keyword != "") {
    $filterDefault .= " and " . $option_val . " like '%" . $keyword . "%'";
}
$sql = "select nisit_event.*,first_name,last_name,event_name from nisit_event
left join nisit on nisit.nisit_id=nisit_event.nisit_id 
left join event on event.event_id=nisit_event.event_id 

";
$result_row = $conn->queryRaw($sql . $filterDefault);//คิวรี่ คำสั่ง
$total = sizeof($result_row);
$for_end = $limit;
$for_start = $start * $limit;

$select_all = $conn->queryRaw($sql . $filterDefault . " order by nisit_event_id desc  limit " . $for_start . "," . $for_end);
$total_num = sizeof($select_all);

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php require_once "css.php" ?>
</head>
<body class="skin-custom sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                จัดการข้อมูลกิจกรรมนักศึกษา
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <form class="form-horizontal" id="form_data" name="form_data" method="post">
                <input id="__delete_field" name="__delete_field" type="hidden" value="">
                <input id="__cmd" name="__cmd" type="hidden" value="">
                <div class="box box-custom">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-7">
                                <a class="btn btn-primary" href="nisit_event-update.php"><i
                                        class="fa fa-plus"></i> เพิ่มข้อมูล</a>
                            </div>
                            <div class="col-sm-2">
                                <select id="option" name="option" class="form-control">
                                    <option value="event_name">กิจกรรม</option>
                                    <option value="nisit_name">นักศึกษา</option>
                                    <option value="register_date">วันที่ลงทะเบียน</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <input class="form-control" type="text" id="keyword" name="keyword"
                                           onblur="trimValue(this)" onkeyup="goSearch()"
                                           value="<?php echo $keyword; ?>">
                                    <a href="javascript:goSearch();"
                                       class="btn btn-default input-group-addon"><i
                                            class="fa fa-search"></i> </a>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class=" table-responsive">
                                    <table class="table table-bordered table-striped" id="tbView">
                                        <tr>
                                            <th width="5%">ลำดับ</th>
                                            <th width="5%">ชื่อกิจกรรม</th>
                                            <th width="10%">นักศึกษา</th>
                                            <th width="20%">วันที่ลงทะเบียน</th>
                                            <th width="10%" class="text-center"></th>
                                        </tr>
                                        <tbody>
                                        <?php
                                        $index = $for_start;
                                        foreach ($select_all as $row) {
                                            $index++;
                                            ?>
                                            <tr>
                                                <td class="text-right"
                                                    nowrap><?php echo $index; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['event_name']; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['first_name'] .' '.$row['last_name']; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['register_date']; ?></td>
                                                <td class="text-center" nowrap >
                                                    <div class="btn-group">
                                                        <a class="btn btn-warning btn-xs" title="แก้ไข"
                                                           href="nisit_event-update.php?__nisit_event_id=<?php echo $row['nisit_event_id']; ?>"><i
                                                                class="fa fa-edit"></i> </a>
                                                        <a class="btn btn-danger btn-xs" title="ลบ"
                                                           onclick="deleteRowData('<?php echo $row['nisit_event_id']; ?>')"><i
                                                                class="fa fa-trash"></i> </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-12">
                                    <?php include "pageindex.php"; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <?php include "footer.php" ?>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>
    $('#menu-nisit-main').addClass('active');
    $('#menu-nisit_event').addClass('active');
</script>
</body>
</html>


