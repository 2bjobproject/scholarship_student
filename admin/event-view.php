<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(STAFF_LEVEL)))
    redirect_to('index.php');

require_once "../connection.inc.php";

$cmd = getIsset("__cmd");
$test = array();
$event_id = getIsset('__event_id');
if ($cmd == "save") {
    $value = array(
        "event_name" => getIsset('__event_name'),
    );
    if ($event_id == "0") {
        if ($conn->create("event", $value)) {
            redirectTo("event.php");
        }

    } else {
        if ($conn->update("event", $value, array("event_id" => $event_id))) {
            redirectTo("event.php");
        }
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php include "css.php" ?>

</head>
<body class="skin-custom sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                ข้อมูลกิจกรรม
                <small>จัดการข้อมูลกิจกรรม</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>
                <li><a href="event.php">กิจกรรม</a></li>
                <li class="active">จัดการข้อมูล</li>
            </ol>
        </section>
        <section class="content">
            <form class="form-horizontal" id="form_data" name="form_data" method="post" enctype="multipart/form-data">
                <input id="__cmd" name="__cmd" type="hidden" value="">
                <div class="box box-custom">
                    <div class="box-header with-border">
                        <h3 class="box-title">จัดการข้อมูลกิจกรรม </h3>
                    </div>
                    <div class="box-body">
                        <input type="hidden" name="__event_id" id="__event_id" class="form-control"
                               value="0"
                               required="true" readonly>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    ชื่อกิจกรรม :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="__event_name" id="__event_name"
                                       class="form-control"
                                       value="" readonly
                                       onblur="trimValue(this);" required="true">
                            </div>
                        </div>

                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <a class="btn btn-default" href="event.php">ย้อนกลับ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <?php include "footer.php" ?>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>
    $('#menu-event-main').addClass('active');
    $('#menu-event').addClass('active');
    function helpReturn(value, action) {
        $.ajax({
            url: 'Allservice.php',
            data: {id: value, action: action},
            method: 'GET',
            success: function (result) {
                var data = JSON.parse(result);
                if (action == "getEventById") {
                    if (data.event_id != null) {
                        console.log(data);
                        setValueevent(data);
                    }
                }
            }
        });
    }
    function setValueevent(data) {
        with (document.form_data) {
            $("#__event_id").val(data.event_id);
            $("#__event_name").val(data.event_name);

        }
    }
</script>
<script>helpReturn('<?php echo $event_id;?>', 'getEventById')</script>
</body>
</html>


