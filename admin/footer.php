<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2018 <?php echo TITLE_ENG?>.</strong> All rights
    reserved.
</footer>