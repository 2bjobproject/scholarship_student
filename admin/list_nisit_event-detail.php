<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(ADMIN_LEVEL,STAFF_LEVEL)))
    redirect_to('index.php');
require_once "../connection.inc.php";

$cmd = getIsset("__cmd");
if ($cmd == "delete") {
    $conn->delete("nisit_scholarship", array("nisit_scholarship_id" => getIsset('__delete_field')));
    redirectTo('check_scholarship.php');
}

$filterDefault = " where 1=1 and nisit_scholarship.status_id in ('2','3') ";

$keyword = getIsset("keyword");
$option_val = getIsset("option");
if ($keyword != "") {
    $filterDefault .= " and " . $option_val . " like '%" . $keyword . "%'";
}
$sql = "select nisit_scholarship.*,status_name,first_name,last_name,scholarship_name,semester_name from nisit_scholarship
left join nisit on nisit.nisit_id=nisit_scholarship.nisit_id
left join scholarship on scholarship.scholarship_id=nisit_scholarship.scholarship_id
left join status on status.status_id=nisit_scholarship.status_id
left join semester on semester.semester_id=scholarship.semester_id
";
$result_row = $conn->queryRaw($sql . $filterDefault);//คิวรี่ คำสั่ง
$total = sizeof($result_row);
$for_end = $limit;
$for_start = $start * $limit;

$select_all = $conn->queryRaw($sql . $filterDefault . " order by scholarship.semester_id desc ,register_date desc limit " . $for_start . "," . $for_end);
$total_num = sizeof($select_all);

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php require_once "css.php" ?>
</head>
<body class="skin-custom sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                จัดการประวัติการขอรับทุน
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <form class="form-horizontal" id="form_data" name="form_data" method="post">
                <input id="__delete_field" name="__delete_field" type="hidden" value="">
                <input id="__cmd" name="__cmd" type="hidden" value="">
                <div class="box box-custom">

                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-7">

                            </div>
                            <div class="col-sm-2">
                                <select id="option" name="option" class="form-control">
                                    <option value="scholarship.scholarship_name">ทุน</option>
                                    <option value="nisit_scholarship.register_date">วันที่ลงทะเทียน</option>
                                    <option value="status_name">สถานะ</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <input class="form-control" type="text" id="keyword" name="keyword"
                                           onblur="trimValue(this)"
                                           value="<?php echo $keyword; ?>">
                                    <a href="javascript:goSearch();"
                                       class="btn btn-default input-group-addon"><i
                                                class="fa fa-search"></i> </a>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class=" table-responsive">
                                    <table class="table table-bordered table-striped" id="tbView">
                                        <tr>
                                            <th class="text-center" width="5%">ลำดับ</th>
                                            <th class="text-center" width="10%">ชื่อ - นามสกุล</th>
                                            <th  class="text-center" width="10%">ชื่อทุน</th>
                                            <th class="text-center"  width="10%">วันที่สมัครทุนการศึกษา</th>
                                            <th class="text-center" width="10%">ปีการศึกษา</th>
                                            <th class="text-center" width="10%">สถานะ</th>

                                        </tr>
                                        <tbody>
                                        <?php
                                        $index = $for_start;
                                        foreach ($select_all as $row) {
                                            $index++;
                                            ?>
                                            <tr>
                                                <td class="text-center"
                                                    nowrap><?php echo $index; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['first_name']. ' ' .$row['last_name']; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['scholarship_name']; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['register_date']; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['semester_name']; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['status_name']; ?> </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-12">
                                    <?php include "pageindex.php"; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <?php include "footer.php" ?>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>
    $('#menu-scholarship-main').addClass('active');
    $('#menu-list-').addClass('active');


    function goSave() {
        if (!required()) {
            alert("ยืนยันการทำรายการหรือไม่");
            $('input[name=__cmd]').val("save");
            $('#form_data').submit();
        }
    }
    var chartRenderIntervalId = null;
    var interval = 2400;
    function intervalChartRender() {
        chartRenderIntervalId = setInterval(() => {
            updateChart();
        }, interval);
    }


    function updateChart() {
        $.ajax({
            url: 'http://student-scholarship.com/apis/updateStatusDateApis.php',

            method: 'GET',
            success: function (result) {
                var data = JSON.parse(result);
                alert("xxxx");
                // if (action == "getNisit_scholarshipById") {
                //     if (data.nisit_scholarship_id != null) {
                //         setValueNisit_Scholarship(data);
                //     }
                // }
            }
        });
    }
</script>


<script>intervalChartRender()</script>
</body>
</html>


