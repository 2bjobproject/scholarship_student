<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(ADMIN_LEVEL, STAFF_LEVEL)))
    redirect_to('index.php');

require_once "../connection.inc.php";

$cmd = getIsset("__cmd");
$limit = 100;
$filterDefault = " where 1=1 and status_id = 2";
$show = 0;
$scholarship_type_id = getIsset("__scholarship_type_id");
$scholarship_id = getIsset("__scholarship_id");
$semester_id = getIsset("__semester_id");
if ($scholarship_type_id != "") {
    $filterDefault .= " and " . 'scholarship.scholarship_type_id' . " ='" . $scholarship_type_id . "'";
}
if ($scholarship_id != "") {
    $filterDefault .= " and " . 'nisit_scholarship.scholarship_id' . "='" . $scholarship_id . "'";
}
if ($semester_id != "") {
    $filterDefault .= " and " . 'scholarship.semester_id' . "='" . $semester_id . "'";
}
if ($cmd == "save") {
    $show = 1;
}
$sql = "SELECT nisit_scholarship.*,first_name,last_name,scholarship_name,semester_name,scholarship_type_name FROM nisit_scholarship
LEFT JOIN scholarship on scholarship.scholarship_id = nisit_scholarship.scholarship_id
LEFT JOIN nisit on nisit.nisit_id = nisit_scholarship.nisit_id
LEFT JOIN nisit_education on nisit_education.nisit_id = nisit.nisit_id 
LEFT JOIN semester on semester.semester_id = scholarship.semester_id
LEFT JOIN scholarship_type on scholarship_type.scholarship_type_id = scholarship.scholarship_type_id";
$result_row = $conn->queryRaw($sql . $filterDefault);//คิวรี่ คำสั่ง
$total = sizeof($result_row);
$for_end = 10;
$for_start = $start * $limit;

$select_all = $conn->queryRaw($sql . $filterDefault . " order by nisit_scholarship_id desc  limit " . $for_start . "," . $for_end);
$total_num = sizeof($select_all);

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php require_once "css.php" ?>
    <style>
        .display {
            display: unset;
        }

        .text-red {
            color: red;
        }
    </style>

</head>
<body class="skin-custom sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div id="posContain" class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" id="form_data" name="form_data" enctype="multipart/form-data"
                          method="post">
                        <div class="">
                            <input id="__delete_field" name="__delete_field" type="hidden" value="">
                            <input id="__cmd" name="__cmd" type="hidden" value="">
                            <div class="col-md-12">
                                <label class="col-sm-3 control-label">
                                </label>
                            </div>
                            <div class="clr"></div>
                            <div class="col-sm-12">
                                <div class="box box-success">

                                    <div class="box-body">
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-3 control-label">
                                                    <i class="text-red">*</i> ปีการศึกษา :
                                                </label>
                                            </div>
                                            <div class="col-sm-3">
                                                <select name="__semester_id" id="__semester_id" class="form-control">
                                                    <option value="">เลือก</option>
                                                    <?php
                                                    $level = "SELECT * FROM semester";
                                                    $select_allaaa = $conn->queryRaw($level . " order by semester_id desc ");
                                                    foreach ($select_allaaa as $type) {
                                                        ?>
                                                        <option <?php echo getIsset("__semester_id") == $type['semester_id'] ? "selected" : ""; ?>
                                                                value="<?php echo $type['semester_id']; ?>"><?php echo $type['semester_name'] . ' ' . $type['year']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-3 control-label">
                                                    ประเภททุน :
                                                </label>
                                            </div>
                                            <div class="col-sm-3">
                                                <select name="__scholarship_type_id" id="__scholarship_type_id"
                                                        class="form-control">
                                                    <option value="">เลือก</option>
                                                    <?php
                                                    $level = "SELECT * FROM scholarship_type";
                                                    $select_allaaa = $conn->queryRaw($level . " order by scholarship_type_id desc ");
                                                    foreach ($select_allaaa as $type) {
                                                        ?>
                                                        <option <?php echo getIsset("__scholarship_type_id") == $type['scholarship_type_id'] ? "selected" : ""; ?>
                                                                value="<?php echo $type['scholarship_type_id']; ?>"><?php echo $type['scholarship_type_name']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-3 control-label">
                                                    ทุน :
                                                </label>
                                            </div>
                                            <div class="col-sm-3">
                                                <select name="__scholarship_id" id="__scholarship_id"
                                                        class="form-control">
                                                    <option value="">เลือก</option>

                                                    <?php
                                                    $level = "SELECT * FROM scholarship";
                                                    $select_allaaa = $conn->queryRaw($level . " order by scholarship_id desc ");
                                                    foreach ($select_allaaa as $type) {
                                                        ?>
                                                        <option <?php echo getIsset("__scholarship_id") == $type['scholarship_id'] ? "selected" : ""; ?>
                                                                value="<?php echo $type['scholarship_id']; ?>"><?php echo $type['scholarship_name']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-3 control-label">
                                                </label>
                                            </div>
                                            <div class="col-sm-5">
                                                <?php if (getIsset("__scholarship_type_id") and getIsset("__scholarship_id") and getIsset("__scholarship_type_id")) ?>
                                                <a class="btn btn-success" href="javascript:goSave();"
                                                   onkeyup="goSave()">ค้นหา</a>
                                                <a class="btn btn-warning" href="report_income.php">ล้าง</a>
                                            </div>
                                        </div>
                                        <div class="form-group" <?php if ($show == 0) { ?> style="display: none" <?php } ?>>
                                            <div class="col-sm-12">
                                                <div class=" table-responsive">
                                                    <table class="table table-bordered table-striped" id="tbView">
                                                        <tr>
                                                            <th width="5%">ลำดับ</th>
                                                            <th width="10%">ชื่อ - นามสกุล</th>
                                                            <th width="10%">ชื่อทุน</th>
                                                            <th width="10%">ประเภททุน</th>
                                                            <th width="10%">ปีการศึกษา</th>
                                                            <th width="10%">วันที่สมัครทุนการศึกษา</th>
                                                            <!--                                                            <th width="10%" class="text-center">จัดการ</th>-->
                                                        </tr>
                                                        <tbody>
                                                        <?php
                                                        $index = $for_start;
                                                        foreach ($select_all as $row) {
                                                            $index++;
                                                            ?>
                                                            <tr>
                                                                <td class="text-right"
                                                                    nowrap><?php echo $index; ?></td>
                                                                <td class=""
                                                                    nowrap><?php echo $row['first_name'] . ' ' . $row['last_name']; ?></td>
                                                                <td class=""
                                                                    nowrap><?php echo $row['scholarship_name']; ?></td>
                                                                <td class=""
                                                                    nowrap><?php echo $row['scholarship_type_name']; ?></td>
                                                                <td class=""
                                                                    nowrap><?php echo $row['semester_name']; ?></td>
                                                                <td class=""
                                                                    nowrap><?php echo $row['register_date']; ?></td>
                                                                <!--                                                                <td class="text-center" nowrap >-->
                                                                <!--                                                                    <div class="btn-group">-->
                                                                <!--                                                                        <a class="btn btn-success btn-xs" target="_blank"-->
                                                                <!--                                                                           href="print-nisit_scholarship-update.php?__nisit_scholarship_id=-->
                                                                <?php //echo $row['nisit_id']; ?><!--"><i-->
                                                                <!--                                                                                    class="fa fa-print" ></i> </a>-->
                                                                <!--                                                                    </div>-->
                                                                <!--                                                                </td>-->
                                                            </tr>
                                                        <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-sm-12">
<!--                                                    --><?php //include "pageindex.php"; ?>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <a class="btn btn-info" style="margin-top: 20px" href="#"
                                                   onclick="goPrintConfirm()">พิมพ์รายงาน</a>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>

    function goSave() {
        var semester_id = $("#__semester_id").val();
        if (semester_id != "") {
            var s1 = $("#__scholarship_id").val();
            var s2 = $("#__semester_id").val();
            var s3 = $("#__scholarship_type_id").val();
            if (s1 && s2 && s3 != null) {
                $('input[name=__cmd]').val("save");
                $('#form_data').submit();
            } else {
                alert("ยืนยันการทำรายการหรือไม่");
                $('input[name=__cmd]').val("save");
                $('#form_data').submit();
            }


        } else {
            alert("กรุณาระบุข้อมูลปีการศึกษา ");
        }
    }

    $('#__startDate,#__endDate').datetimepicker({
        datepicker: true,
        timepicker: false,
        format: 'Y-m-d',
        closeOnDateSelect: true
    });

    function goClearStart() {
        $("#__startDate").val('');
    }

    function goClearEnd() {
        $("#__endDate").val('');
    }

    $('#menu-report').addClass('active');
    $('#menu-report-main').addClass('active');

    function goPrintConfirm() {
        var scholarship_type_id = $("#__scholarship_type_id").val();
        var scholarship_id = $("#__scholarship_id").val();
        var semester_id = $("#__semester_id").val();
        if (semester_id != "") {
            if (confirm("ต้องการพิมพ์หรือไม่")) {
                var url = "print_income.php?scholarship_type_id=" + scholarship_type_id + "&scholarship_id=" + scholarship_id + "&semester_id=" + semester_id;
                window.open(url, "_blank");
            }
        } else {
            alert("กรุณาระบุข้อมูลปีการศึกษา ");
        }
    }
</script>
</body>
</html>


