<?php
session_start();
require_once "../connection.inc.php";
require_once '../mpdf/mpdf.php';

$nisit = getIsset("__nisit_scholarship_id");

$nisit_id = $conn->queryRaw("select * from nisit_scholarship  where nisit_scholarship_id='$nisit'", true);

$sql = "SELECT nisit_scholarship.*,bank_name,account_type_name,
nisit.*,
nisit_education.*,
nisit_family.*,
faculty_name,section_short,department_name,major_name,
province_name,amphur_name,district_name

FROM nisit_scholarship

LEFT JOIN nisit on nisit.nisit_id = nisit_scholarship.nisit_id
LEFT JOIN account_type on account_type.account_type_id = nisit.account_type_id
LEFT JOIN bank on bank  .bank_id = nisit.bank_id

LEFT JOIN nisit_education on nisit_education.nisit_id = nisit.nisit_id
LEFT JOIN faculty on faculty.faculty_id = nisit_education.faculty_id
LEFT JOIN section on section.section_id = nisit_education.section_id
LEFT JOIN major on major.major_id = nisit_education.major_id
LEFT JOIN department on department.department_id = nisit_education.department_id

LEFT JOIN nisit_family on nisit_family.nisit_id = nisit.nisit_id

LEFT JOIN province on province.province_code = nisit.province_code 
LEFT JOIN amphur on amphur.amphur_code = nisit.amphur_code 
LEFT JOIN district on district.district_code = nisit.district_code 


where nisit_scholarship.nisit_scholarship_id=$nisit
";
$result_row = $conn->queryRaw($sql, true);
$total = sizeof($result_row);
$for_end = $limitt;
$for_start = $start * $limitt;

$select_all = $conn->queryRaw($sql . " order by nisit_scholarship_id desc  limit " . $for_start . "," . $for_end);
$total_num = sizeof($select_all);


$scholarship = "select nisit_scholarship.*,scholarship_name,semester_name from nisit_scholarship
LEFT JOIN scholarship on scholarship.scholarship_id = nisit_scholarship.scholarship_id 
LEFT JOIN nisit on nisit.nisit_id = nisit_scholarship.nisit_id
LEFT JOIN semester on semester.semester_id = scholarship.semester_id
where nisit_scholarship.nisit_scholarship_id=$nisit";

$result = $conn->queryRaw($scholarship, true);
$total = sizeof($result);
$end = $limitt;
$starts = $start * $limitt;

$select_all1 = $conn->queryRaw($scholarship . " order by nisit_scholarship.nisit_scholarship_id desc  limit " . $starts . "," . $end);
$total_num = sizeof($select_all1);


$event = "select nisit_event.*,event_name from nisit_event
LEFT JOIN event on event.event_id = nisit_event.event_id 
LEFT JOIN nisit on nisit.nisit_id = nisit_event.nisit_id
where nisit_event.nisit_id='".$nisit_id['nisit_id']."'";

$result2 = $conn->queryRaw($event, true);
$total = sizeof($result2);
$end2 = $limitt;
$starts2 = $start * $limitt;

$select_all2 = $conn->queryRaw($event . " order by nisit_event.nisit_id desc  limit " . $starts2 . "," . $end2);
$total_num = sizeof($select_all2);

$sql2 = "SELECT nisit_scholarship.*,province_name,amphur_name,district_name

FROM nisit_scholarship
LEFT JOIN nisit on nisit.nisit_id = nisit_scholarship.nisit_id
LEFT JOIN province on province.province_code = nisit.present_province_code 
LEFT JOIN amphur on amphur.amphur_code = nisit.present_amphur_code 
LEFT JOIN district on district.district_code = nisit.present_district_code 
where nisit_scholarship.nisit_scholarship_id=$nisit";
$result_row2 = $conn->queryRaw($sql2, true);
$total = sizeof($result_row2);
$for_end = $limitt;
$for_start = $start * $limitt;


$pahth = PATH_UPLOAD.$result_row['image_path'];
$type = pathinfo($pahth, PATHINFO_EXTENSION);
$image = file_get_contents($pahth);
$base64 = 'data:image/' . $type . ';base64,' . base64_encode($image);
ob_start();
?>


<html>
<head>
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
    <style>
    </style>
</head>
<body>
<!--<span style="font-size: 0.6em">พิมพ์เมื่อ --><?php //echo Datethai() ?><!--</span>-->

<div style="font-size: 22px;
    font-weight: bold;
    text-align: center;">แบบฟอร์มประวัตินักศึกษาทุน
</div>
<div id="menu" style="height: auto;width:100%;float:left;">
    <div style=" text-align: center;font-size: 16px;"><b>&emsp;มหาวิทยาลัยเทคโนโลยีพระจอมเกล้าพระนครเหนือ</b></div>
    <div style=" text-align: center;font-size: 16px;"><b>&emsp;ทุน <?php echo $result['scholarship_name']; ?> ปีการศึกษา <?php echo $result['semester_name']; ?></b></div>
</div>
<div id="menu" style="height: auto;width:80%;float:left;">
    <div style="width: 100%;display: inline; line-height: 20pt">
        <div style="width: 100%;float:left;font-size: 16px;margin-bottom: 10px;"><b>ข้อมูลทั่วไป</b></div>
    </div>
    <div style="font-size: 14px">
        <div style="width: 100%;display: inline; line-height: 20pt">
            <div style="width: 7%;float:left;"><b>ชื่อ :</b></div>
            <div style="width: 15%;float:left;"><?php echo $result_row['first_name']; ?></div>
            <div style="width: 13%;float:left;"><b>นามสกุล :</b></div>
            <div style="width: 15%;float:left;"><?php echo $result_row['last_name']; ?></div>
            <div style="width: 7%;float:left;"><b>อายุ :</b></div>
            <div style="width: 5%;float:left;"> <?php echo date('Y-m-d') - $result_row['birth_date'] ; ?> </div>

        </div>
        <div style="width: 100%;display: inline; line-height: 20pt">
            <div style="width: 30%;float:left;"><b>เลขประจำตัวประชาชน :</b></div>
            <div style="width: 20%;float:left;"><?php echo $result_row['id_card']; ?></div>
            <div style="width: 30%;float:left;"><b>รหัสประจำตัวนักศึกษา :</b></div>
            <div style="width: 20%;float:left;"><?php echo $result_row['nisit_code']; ?></div>
        </div>
    </div>
</div>


<div id="content" style="height: auto;width:20%;float:left;">
    <div style="text-align: center"> <img width="100" height="100" src="<?php echo $base64  ?>">
    </div>
</div>

<div id="menu" style="height: auto;width:100%;float:left;">

    <div style="font-size: 14px">


        <div style="width: 100%;display: inline; line-height: 20pt">
            <div style="width: 10%;float:left;"><b>น้ำหนัก :</b></div>
            <div style="width: 5%;float:left;"><?php echo $result_row['weight']; ?></div>
            <div style="width: 9%;float:left;"><b>ส่วนสูง :</b></div>
            <div style="width: 6%;float:left;"><?php echo $result_row['tall']; ?></div>
            <div style="width: 9%;float:left;"><b>ศาสนา :</b></div>
            <div style="width: 10%;float:left;"><?php echo $result_row['religion']; ?></div>
            <div style="width: 10%;float:left;"><b>สัญชาติ :</b></div>
            <div style="width: 10%;float:left;"><?php echo $result_row['national']; ?></div>
            <div style="width: 10%;float:left;"><b>เชื้อชาติ :</b></div>
            <div style="width: 10%;float:left;"><?php echo $result_row['rac']; ?></div>
        </div>
        <div style="width: 100%;display: inline; line-height: 20pt">
            <div style="width: 23%;float:left;"><b>ที่อยู่(ภูมิลำเนา)เลขที่ :</b></div>
            <div style="width: 10%;float:left;"><?php echo $result_row['address']; ?></div>
            <div style="width: 6%;float:left;"><b>หมู่ :</b></div>
            <div style="width: 10%;float:left;"><?php echo $result_row['moo']; ?></div>
            <div style="width: 6%;float:left;"><b>ซอย :</b></div>
            <div style="width: 10%;float:left;"><?php echo $result_row['soi']; ?></div>
        </div>
        <div style="width: 100%;display: inline; line-height: 20pt">
            <div style="width: 15%;float:left;"><b>ตำบล/แขวง :</b></div>
            <div style="width: 18%;float:left;"><?php echo $result_row['district_name']; ?></div>
            <div style="width: 14%;float:left;"><b>อำเภอ/เขต :</b></div>
            <div style="width: 18%;float:left;"><?php echo $result_row['amphur_name']; ?></div>
            <div style="width: 9%;float:left;"><b>จังหวัด :</b></div>
            <div style="width: 20%;float:left;"><?php echo $result_row['province_name']; ?></div>
        </div>
        <div style="width: 100%;display: inline; line-height: 20pt">
            <div style="width: 15%;float:left;"><b>รหัสไปรษณีย์ :</b></div>
            <div style="width: 10%;float:left;"><?php echo $result_row['zipcode']; ?></div>
            <div style="width: 11%;float:left;"><b>โทรศัพท์ :</b></div>
            <div style="width: 20%;float:left;"><?php echo $result_row['phone']; ?></div>
            <div style="width: 8%;float:left;"><b>อีเมล์ :</b></div>
            <div style="width: 25%;float:left;"><?php echo $result_row['email']; ?></div>
        </div>
        <div style="width: 100%;display: inline; line-height: 20pt">
            <div style="width: 19%;float:left;"><b>ที่อยู่ปัจจุบันเลขที่ :</b></div>
            <div style="width: 10%;float:left;"><?php echo $result_row['present_address']; ?></div>
            <div style="width: 5%;float:left;"><b>หมู่ :</b></div>
            <div style="width: 5%;float:left;"><?php echo $result_row['present_moo']; ?></div>
            <div style="width: 6%;float:left;"><b>ซอย :</b></div>
            <div style="width: 11%;float:left;"><?php echo $result_row['present_soi']; ?></div>
            <div style="width: 7%;float:left;"><b>ถนน :</b></div>
            <div style="width: 20%;float:left;"><?php echo $result_row['present_street']; ?></div>
        </div>
        <div style="width: 100%;display: inline; line-height: 20pt">
            <div style="width: 15%;float:left;"><b>ตำบล/แขวง :</b></div>
            <div style="width: 16%;float:left;"><?php echo $result_row['district_name']; ?></div>
            <div style="width: 13%;float:left;"><b>อำเภอ/เขต :</b></div>
            <div style="width: 16%;float:left;"><?php echo $result_row['amphur_name']; ?></div>
            <div style="width: 9%;float:left;"><b>จังหวัด :</b></div>
            <div style="width: 25%;float:left;"><?php echo $result_row['province_name']; ?></div>
        </div>
        <div style="width: 100%;display: inline; line-height: 20pt">
            <div style="width: 15%;float:left;"><b>รหัสไปรษณีย์ :</b></div>
            <div style="width: 10%;float:left;"><?php echo $result_row['present_zipcode']; ?></div>
            <div style="width: 10%;float:left;"><b>โทรศัพท์ :</b></div>
            <div style="width: 20%;float:left;"><?php echo $result_row['present_phone']; ?></div>

        </div>
        <div style="width: 100%;display: inline; line-height: 20pt;margin-top: 15px">
            <div style="width: 30%;float:left;"><b style="font-size: 16px;">ข้อมูลด้านการเงิน</b> <b>ธนาคาร :</b></div>
            <div style="width: 15%;float:left;"><?php echo $result_row['bank_name']; ?></div>
            <!--            <div style="width: 10%;float:left;"><b>สาขา :</b></div>-->
            <!--            <div style="width: 10%;float:left;"><b>-</b></div>-->
        </div>
        <div style="width: 100%;display: inline; line-height: 20pt">
            <div style="width: 15%;float:left;"><b>ประเภทบัญชี :</b></div>
            <div style="width: 15%;float:left;"><?php echo $result_row['account_type_name']; ?></div>
            <div style="width: 10%;float:left;"><b>เลขบัญชี :</b></div>
            <div style="width: 40%;float:left;"><?php echo $result_row['account_no']; ?></div>
        </div>
    </div>
    <div style="width: 100%;display: inline; line-height: 20pt;margin-top: 15px">
        <div style="width: 100%;float:left;font-size: 16px;"><b>ข้อมูลด้านการศึกษา</b></div>
    </div>
    <div style="font-size: 14px">
        <div style="width: 100%;display: inline; line-height: 20pt">
            <div style="width: 13%;float:left;"><b>กำลังศึกษา :</b></div>
            <div style="width: 16%;float:left;"><?php echo $result_row['major_name'] == null ? '-' : $result_row['major_name'] ?></div>
            <div style="width: 9%;float:left;"><b>ชั้นปีที่ :</b></div>
            <div style="width: 55%;float:left;"><?php echo $result_row['year']; ?></div>
        </div>
        <div style="width: 100%;display: inline; line-height: 20pt">
            <div style="width: 8%;float:left;"><b>คณะ :</b></div>
            <div style="width: 90%;float:left;"><?php echo $result_row['faculty_name']?></div>
        </div>
        <div style="width: 100%;display: inline; line-height: 20pt">
            <div style="width: 10%;float:left;"><b>ภาควิชา :</b></div>
            <div style="width: 60%;float:left;"><?php echo $result_row['department_name']; ?></div>
            <div style="width: 8%;float:left;"><b>สาขา :</b></div>
            <div style="width: 20%;float:left;"><?php echo $result_row['section_short']; ?></div>
        </div>
        <div style="width: 100%;display: inline; line-height: 20pt">
            <div style="width: 7%;float:left;"><b>GPA :</b></div>
            <div style="width: 15%;float:left;"><?php echo $result_row['GPA']; ?></div>
            <div style="width: 20%;float:left;"><b>ความสามารถพิเศษ :</b></div>
            <div style="width: 25%;float:left;"><?php echo $result_row['talent']; ?></div>

        </div>

    </div>

    <div style="width: 100%;display: inline; line-height: 20pt;margin-top: 15px">
        <div style="width: 100%;float:left;font-size: 16px;"><b>ข้อมูลครอบครัว</b></div>
    </div>
    <div style="font-size: 14px">
        <div style="width: 100%;display: inline; line-height: 20pt">
            <div style="width: 10%;float:left;"><b>บิดาชื่อ :</b></div>
            <div style="width: 17%;float:left;"><?php echo $result_row['father_first_name']; ?></div>
            <div style="width: 11%;float:left;"><b>นามสกุล :</b></div>
            <div style="width: 17%;float:left;"><?php echo $result_row['father_last_name']; ?></div>
            <div style="width: 7%;float:left;"><b>อายุ :</b></div>
            <div style="width: 5%;float:left;"><?php echo $result_row['father_ago']; ?></div>
            <div style="width: 8%;float:left;"><b>อาชีพ :</b></div>
            <div style="width: 10%;float:left;"><?php echo $result_row['father_job']; ?></div>
        </div>
        <div style="width: 100%;display: inline; line-height: 20pt">
            <div style="width: 11%;float:left;"><b>เงินเดือน :</b></div>
            <div style="width: 18%;float:left;"><?php echo number_format($result_row['father_salary'],2) ; ?></div>
            <div style="width: 12%;float:left;"><b>สถานะภาพ :</b></div>
            <div style="width: 18%;float:left;"><?php if ($result_row['father_status_id'] == 1) {
                    echo "มีชีวิตอยู่";
                } elseif ($result_row['father_status_id'] == 2) {
                    echo "แยกกันอยู่";
                } else {
                    echo "ถึงแก่กรรม";
                } ?><?php echo $result_row['father_olabeler_status']; ?>
            </div>
            <div style="width: 11%;float:left;"><b>โทรศัพท์ :</b></div>
            <div style="width: 18%;float:left;"><?php echo $result_row['father_phone']; ?>
            </div>
        </div>
        <div style="width: 100%;display: inline; line-height: 20pt">
            <div style="width: 14%;float:left;"><b>ที่อยู่ปัจจุบัน :</b></div>
            <div style="width: 80%;float:left;"><?php echo $result_row['father_address']; ?></div>

        </div>
        <div style="width: 100%;display: inline; line-height: 20pt">
            <div style="width: 12%;float:left;"><b>มารดาชื่อ :</b></div>
            <div style="width: 17%;float:left;"><?php echo $result_row['mother_first_name']; ?></div>
            <div style="width: 11%;float:left;"><b>นามสกุล :</b></div>
            <div style="width: 17%;float:left;"><?php echo $result_row['mother_last_name']; ?></div>
            <div style="width: 7%;float:left;"><b>อายุ :</b></div>
            <div style="width: 5%;float:left;"><?php echo $result_row['mother_ago']; ?></div>
            <div style="width: 8%;float:left;"><b>อาชีพ :</b></div>
            <div style="width: 10%;float:left;"><?php echo $result_row['mother_job']; ?></div>
        </div>
        <div style="width: 100%;display: inline; line-height: 20pt">
            <div style="width: 11%;float:left;"><b>เงินเดือน :</b></div>
            <div style="width: 18%;float:left;"><?php echo number_format($result_row['mother_salary'],2) ; ?></div>
            <div style="width: 12%;float:left;"><b>สถานะภาพ :</b></div>
            <div style="width: 18%;float:left;"><?php if ($result_row['mother_status_id'] == 1) {
                    echo "มีชีวิตอยู่";
                } elseif ($result_row['mother_status_id'] == 2) {
                    echo "แยกกันอยู่";
                } else {
                    echo "ถึงแก่กรรม";
                } ?><?php echo $result_row['mother_other_status']; ?>
            </div>
            <div style="width: 11%;float:left;"><b>โทรศัพท์ :</b></div>
            <div style="width: 20%;float:left;"><?php echo $result_row['mother_phone']; ?>
            </div>
            <div style="width: 100%;display: inline; line-height: 20pt">
                <div style="width: 14%;float:left;"><b>ที่อยู่ปัจจุบัน :</b></div>
                <div style="width: 80%;float:left;"><?php echo $result_row['mother_address']; ?></div>
            </div>
        </div>
        <div style="width: 100%;display: inline; line-height: 20pt">
            <div style="width: 14%;float:left;"><b>ผู้ปกครองชื่อ :</b></div>
            <div style="width: 17%;float:left;"><?php echo $result_row['parent_first_name']; ?></div>
            <div style="width: 11%;float:left;"><b>นามสกุล :</b></div>
            <div style="width: 17%;float:left;"><?php echo $result_row['parent_last_name']; ?></div>
            <div style="width: 7%;float:left;"><b>อายุ :</b></div>
            <div style="width: 5%;float:left;"><?php echo $result_row['parent_age']; ?></div>
            <div style="width: 8%;float:left;"><b>อาชีพ :</b></div>
            <div style="width: 10%;float:left;"><?php echo $result_row['parent_job']; ?></div>
        </div>
        <div style="width: 100%;display: inline; line-height: 20pt">
            <div style="width: 11%;float:left;"><b>เงินเดือน :</b></div>
            <div style="width: 18%;float:left;"><?php echo number_format($result_row['parent_salary'],2) ; ?></div>
            <div style="width: 14%;float:left;"><b>เกี่ยวข้องเป็น :</b></div>
            <div style="width: 18%;float:left;"><?php echo $result_row['parent_abount']; ?></div>
            <div style="width: 11%;float:left;"><b>โทรศัพท์ :</b></div>
            <div style="width: 18%;float:left;"><?php echo $result_row['parent_phone']; ?>
            </div>
            <div style="width: 100%;display: inline; line-height: 20pt">
                <div style="width: 14%;float:left;"><b>ที่อยู่ปัจจุบัน :</b></div>
                <div style="width: 80%;float:left;"><?php echo $result_row['parent_address']; ?></div>

            </div>
        </div>
    </div>
    <div style="width: 100%;display: inline; line-height: 20pt;margin-top: 15px">
        <div style="width: 50%;float:left; text-align: right">ลงชื่อ</div>
        <div style="float:left;">..........................................ผู้สมัคร</div>
    </div>
    <div style="width: 100%;display: inline; line-height: 20pt">
        <div style="width: 50%;float:left; text-align: right">(</div>
        <div style="width: 30%;float:left;">..........................................)</div>
    </div>
    <div style="width: 100%;display: inline; line-height: 20pt">
        <div style="width: 50%;float:left; text-align: right">.</div>
        <div style="width: 30%;float:left;">.........../............../................</div>
    </div>
</div>
<!--<div id="content" style="height: auto;width:20%;float:left;">-->
<!--    -->
<!--    <div id="boder" style="width: 100%;">-->
<!--        <div style="text-align: center; line-height: 20pt;font-size: 14px"><b>เฉพาะบุคลากรประวัติการได้รับทุนฯ</b></div>-->
<!--        <div style="text-align: center; line-height: 20pt;font-size: 14px"><b>************</b></div>-->
<!--        --><?php
//        $index = $start;
//        foreach ($select_all1 as $row) {
//            $index++;
//            ?>
<!--            <div style="line-height: 20pt;font-size: 12px">ครั้งที่ --><?php //echo $index; ?><!--</div>-->
<!--            <div style="line-height: 20pt;font-size: 12px"> --><?php //echo $row['scholarship_name'] ?><!--</div>-->
<!--        --><?php //} ?>
<!---->
<!--        <div style="text-align: center;line-height: 20pt;font-size: 14px"><b>************</b></div>-->
<!--        <div style="text-align: center;line-height: 20pt;font-size: 14px"><b>การเข้าร่วมกิจกรรม</b></div>-->
<!--        --><?php
//        $index = $start;
//        foreach ($select_all2 as $row) {
//            $index++;
//            ?>
<!--            <div style="line-height: 20pt;font-size: 12px"> --><?php //echo $index; ?><!--)--><?php //echo $row['event_name'] ?><!--</div>-->
<!--            <div style="line-height: 20pt;font-size: 12px">วันที่ --><?php //echo $row['register_date'] ?><!--</div>-->
<!--        --><?php //} ?>
<!--        <div style="text-align: center;line-height: 20pt;font-size: 14px"><b>************</b></div>-->
<!--        <div style="text-align: center;line-height: 20pt;font-size: 14px"><b>การให้ความช่วยเหลือพิเศษ</b></div>-->
<!--        <div style="line-height: 20pt;font-size: 12px">.......................................</div>-->
<!--        <div style="line-height: 20pt;font-size: 12px">.......................................</div>-->
<!--        <div style="line-height: 20pt;font-size: 12px">.......................................</div>-->
<!--        <div style="line-height: 20pt;font-size: 12px">.......................................</div>-->
<!--        <div style="line-height: 20pt;font-size: 12px">.......................................</div>-->
<!--        <div style="line-height: 20pt;font-size: 12px">.......................................</div>-->
<!--    </div>-->
</div>
</body>
</html>
<script language="javascript">
    function calAge(o){
        var tmp = o.value.split("-");
        var current = new Date();
        var current_year = current.getFullYear();
        document.getElementById("age").value = current_year - tmp[0];
    }
</script>
<?php
$html = ob_get_contents();
ob_end_clean();
$pdf = new mPDF('th', 'A4', '0');
$pdf->autoPageBreak = true;
$pdf->WriteHTML($html, 2);
$pdf->output();
?>
