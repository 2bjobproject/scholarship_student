<?php
session_start();
require_once "../common.inc.php";
require_once '../connection.inc.php';
$cmd = getIsset('__cmd');
$error_type = false;
//$permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
//$string = generate_string($permitted_chars, 6);
//
//function generate_string($input, $strength = 16) {
//    $input_length = strlen($input);
//    $random_string = '';
//    for($i = 0; $i < $strength; $i++) {
//        $random_character = $input[mt_rand(0, $input_length - 1)];
//        $random_string .= $random_character;
//    }
//    return $random_string;
//}
if ($cmd == "login") {
    $w = 1;
//    $tttttt = getIsset('ttttt');
    $check_chars = getIsset('check_chars');
    $username = getIsset('__username');
    $password = (getIsset('__password'));
    if($check_chars != $_SESSION['real']) {
        $message = "ยืนยันรหัสไม่ถูกต้อง";
        $error_type = true;
    }else{
        $chk_login = $conn->select('employee', array('username' => $username, 'password' => $password), true);
        if ($chk_login != null) {
            $level = $conn->select("employee_type ", array("employee_type_id" => $chk_login['employee_type_id']), true);
            $uprofile = array();
            $uprofile['id'] = $chk_login['employee_id'];
            $uprofile['name'] = $chk_login['first_name'] . ' ' . $chk_login['last_name'];
            $uprofile['level'] = $chk_login['employee_type_id'];
            $uprofile['level_name'] = 'admin';

            $uprofile['email'] = $chk_login['email'];
            $uprofile['tel'] = $chk_login['phone'];
            $uprofile['picture'] = "";
            $_SESSION['uprofile'] = $uprofile;
            redirectTo('index.php');
        } else {
            $chk_login = $conn->select('employee', array('username' => $username, 'password' => $password), true);
            if ($chk_login != null) {
                $level = $conn->select("employee_type", array("employee_type_id" => $chk_login['employee_type_id']), true);
                $uprofile = array();
                $uprofile['id'] = $chk_login['employee_id'];
                $uprofile['name'] = $chk_login['first_name'] . ' ' . $chk_login['last_name'];
                $uprofile['level'] = STAFF_LEVEL;
                $uprofile['email'] = $chk_login['email'];
                $uprofile['tel'] = $chk_login['phone'];
                $uprofile['picture'] = "";
                $uprofile['level_name'] = 'staff';
                $_SESSION['uprofile'] = $uprofile;
                if ($level['employee_type_id'] == STAFF_LEVEL) {
                    redirectTo('index.php');
                }
            } else {
                $chk_loginn = $conn->select('nisit', array('nisit_code' => $username, 'id_card' => $password), true);
                if ($chk_loginn != null) {
                    $level = $conn->select("employee_type", array("employee_type_id" => $chk_loginn['employee_type_id']), true);
                    $uprofile = array();
                    $uprofile['id'] = $chk_loginn['nisit_id'];
                    $uprofile['name'] = $chk_loginn['first_name'] . ' ' . $chk_loginn['last_name'];
                    $uprofile['level'] = NISIT_LEVEL;
                    $uprofile['email'] = $chk_loginn['email'];
                    $uprofile['tel'] = $chk_loginn['phone'];
                    $uprofile['picture'] = $chk_loginn['image_path'];
                    $uprofile['level_name'] = 'นักศึกษา';
                    $_SESSION['uprofile'] = $uprofile;
                    redirectTo('../font/index.php');
                } else {

                    $message = "ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง";
                    $error_type = true;
                }
            }
        }
    }
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo TITLE_ENG; ?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Font Icon -->

    <!-- Main css -->
    <?php require_once "css.php" ?>
    <link rel="stylesheet" href="css/style.css">
    <style>
        .container {
             padding-right: 0px;
            padding-left: 0px;
            margin-right: auto;
            margin-left: auto;
        }
        .container {
             width: unset!important;
        }
        .separator {
            display: flex;
            align-items: center;
            text-align: center;
            color: white;
        }

        .separator::before, .separator::after {
            content: '';
            flex: 1;
            border-bottom: 1px solid white;
        }

        .separator::before {
            margin-right: .25em;
        }

        .separator::after {
            margin-left: .25em;
        }
        body {
            margin: 0;
            padding: 0;
        }
        img.op{
            opacity: 1;
        }
    </style>
</head>
<body style="overflow: hidden">

<div class="main">

    <div class="container">

        <div class="signup-content">

            <div class="signup-form">
                <img src="../uploads/220px-KMUTNB-LOGO.jpg" alt="" style="width: 100px;height: 100px">
                <form class="form-horizontal" method="post" id="form_login" name="form_login" style="padding: 100px">
                    <input type="hidden" name="__cmd" id="__cmd" value="login">
                    <h3 style="color:black;margin-bottom:10px">เข้าสู่ระบบ</h3>
                    <br>

                    <div class="input-group" style="margin-bottom:20px;">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input name="__username" type="text" maxlength="255" id="__username" autofocus
                               class=" form-control input-sm " placeholder="รหัสนักศึกษา" required>
                    </div>

                    <div class="input-group" style="margin-bottom:20px;">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input name="__password" type="password" maxlength="20" id="__password"
                               class=" form-control input-sm " placeholder="เลขบัตรประชาชน" required>
                    </div>
                    <div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-4" onselectstart="return false" oncopy="return false"  oncut="return false">
                            <img src="captcha.php" class="captcha-image" width="120" height="30" border="1" alt="CAPTCHA">

                            <!--                        <input type="hidden" name="ttttt" id="ttttt" value="--><?php //echo $string;?><!--">-->
                            <!--                        <h3 style="color:white;margin:0px" >--><?php //echo $string;?><!--</h3>-->
                        </div>
                        <div class="col-lg-8">
                            <i class="fa fa-redo refresh-captcha" style="color: white;cursor: pointer"></i>

                        </div>
                    </div>
                    <div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <input name="check_chars" type="password" maxlength="6" id="check_chars"
                                   class=" form-control input-sm " placeholder="รหัสความปลอดภัย" required>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-info btn-md" style="width: 100%">เข้าสู่ระบบ</button>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom:20px;">
                        <div class="col-md-12">
                            <div class="separator" style="color: white;">ยังไม่มีบัญชีผู้ใช้</div>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom:20px;">
                        <div class="col-md-6">
                            <a class=" pull-left" style="color: white" href="../font/registor.php">สมัครสมาชิก</a>
                        </div>
                        <div class="col-md-6">
                            <a class="pull-right" style="color: white" href="forget_password.php">ลืมรหัสผ่าน</a>
                        </div>
                    </div>
                    <!--                <div class="row">-->
                    <!--                    <div class="col-md-6">-->
                    <!--                        <a class="btn btn-default pull-left" href="../font/index.php">Back</a>-->
                    <!--                    </div>-->
                    <!---->
                    <!--                    <div class="col-md-6">-->
                    <!--                        <button class="btn btn-info pull-right" type="submit">Login</button>-->
                    <!---->
                    <!--                    </div>-->
                    <!--                </div>-->
                </form>
            </div>
            <div class="signup-img">

                <img src="../assets/img/logo2.jpg" alt="" >
                <div class="signup-img-content" style="padding: 50px;text-align: left">
                    <h2>กองกิจการนักศึกษา เป็นหน่วยงานที่ </h2>
                    <p>“ส่งเสริมการพัฒนานักศึกษา ทั้งในด้านร่างกาย สติปัญญา อารมณ์ สังคม และคุณธรรมจริยธรรม รวมทั้งจัดให้บริการ และสวัสดิการ เพื่อ ส่งเสริมให้นักศึกษามีความพร้อมในการศึกษาเล่าเรียน และจัดสภาพแวดล้อมของ มจพ. ที่เอื้อต่อการศึกษาและการพัฒนานักศึกษา”</p>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="modal flat" id="alertModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: white">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">x</span></button>
                <span>
                <span class="glyphicon glyphicon-info-sign" style="font-size:30px;color:#f2b712"></span>
                <span style="font-size: 20px;" id="alertTitle"></span></span>
            </div>
        </div>
    </div>
</div>
<?php require_once 'javascript.php'; ?>
<script>
    var refreshButton = document.querySelector(".refresh-captcha");
    refreshButton.onclick = function() {
        document.querySelector(".captcha-image").src = 'captcha.php?' + Date.now();
    }
    function showModal() {
        $('#alertTitle').text('<?php echo isset($message) ? $message : ""; ?>');
        $('#alertModal').modal();
    }
</script>
<?php if ($error_type) {
    echo '<script>showModal()</script>';
} ?>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>