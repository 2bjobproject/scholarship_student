<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(ADMIN_LEVEL,STAFF_LEVEL)))
    redirect_to('index.php');

require_once "../connection.inc.php";

$cmd = getIsset("__cmd");
if ($cmd == "delete") {
    $conn->delete("nisit", array("nisit_id" => getIsset('__delete_field')));
    $conn->delete("nisit_education", array("nisit_id" => getIsset('__delete_field')));
    $conn->delete("nisit_family", array("nisit_id" => getIsset('__delete_field')));
    redirectTo('nisit.php');
}
$filterDefault = " where 1=1 ";

$keyword = getIsset("keyword");
$option_val = getIsset("option");
if ($keyword != "") {
    $filterDefault .= " and " . $option_val . " like '%" . $keyword . "%'";
}
$sql = "select nisit.*,department_name,major_name,faculty_name from nisit 
LEFT JOIN nisit_education on nisit_education.nisit_id = nisit.nisit_id
LEFT JOIN faculty on faculty.faculty_id = nisit_education.faculty_id
LEFT JOIN major on major.major_id = nisit_education.major_id
LEFT JOIN department on department.department_id = nisit_education.department_id
";
$result_row = $conn->queryRaw($sql . $filterDefault);//คิวรี่ คำสั่ง
$total = sizeof($result_row);
$for_end = $limit;
$for_start = $start * $limit;

$select_all = $conn->queryRaw($sql . $filterDefault . " order by nisit_id desc  limit " . $for_start . "," . $for_end);
$total_num = sizeof($select_all);

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php require_once "css.php" ?>
</head>
<body class="skin-custom sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                จัดการข้อมูลนักศึกษา
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <form class="form-horizontal" id="form_data" name="form_data" method="post">
                <input id="__delete_field" name="__delete_field" type="hidden" value="">
                <input id="__cmd" name="__cmd" type="hidden" value="">
                <div class="box box-custom">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-7">
                                <?php if($uprofile['level'] == ADMIN_LEVEL){?>
                                <a class="btn btn-primary" href="nisit-update.php"><i
                                        class="fa fa-plus"></i> เพิ่มข้อมูล</a>
                                <?php }?>
                            </div>
                            <div class="col-sm-2">
                                <select id="option" name="option" class="form-control">
                                    <option value="nisit_code">รหัสนักศึกษา</option>
                                    <option value="first_name">ชื่อนักศึกษา</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <input class="form-control" type="text" id="keyword" name="keyword"
                                           onblur="trimValue(this)" onkeyup="goSearch()"
                                           value="<?php echo $keyword; ?>">
                                    <a href="javascript:goSearch();"
                                       class="btn btn-default input-group-addon"><i
                                            class="fa fa-search"></i> </a>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class=" table-responsive">
                                    <table class="table table-bordered table-striped" id="tbView">
                                        <tr>
                                            <th class="text-center" width="5%">ลำดับ</th>
                                            <th class="text-center" width="10%">รหัสนักศึกษา</th>
                                            <th class="text-center" width="10%">ชื่อ - นามสกุล</th>
<!--                                            <th width="10%">เลขบัตรประจำตัวประชาชน</th>-->
<!--                                            <th width="10%">ที่อยู่</th>-->
                                            <th class="text-center" width="10%">คณะ</th>
                                            <th class="text-center" width="10%">สาขา</th>
<!--                                            <th width="10%">ภาควิชา</th>-->
                                            <th  class="text-center" width="10%" class="text-center"></th>
                                        </tr>
                                        <tbody>
                                        <?php
                                        $index = $for_start;
                                        foreach ($select_all as $row) {
                                            $index++;
                                            ?>
                                            <tr>
                                                <td class="text-center"
                                                    nowrap><?php echo $index; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['nisit_code']; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['first_name']. ' ' .$row['last_name']; ?></td>
<!--                                                <td class=""-->
<!--                                                    nowrap>--><?php //echo $row['id_card']; ?><!--</td>-->
<!--                                                <td class=""-->
<!--                                                    nowrap>--><?php //echo $row['address']; ?><!--</td>-->
                                                <td class=""
                                                    nowrap><?php echo $row['faculty_name']; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['major_name']; ?></td>
<!--                                                <td class=""-->
<!--                                                    nowrap>--><?php //echo $row['department_name']; ?><!--</td>-->
                                                <td class="text-center" nowrap >
                                                    <div class="btn-group">
                                                        <?php if($uprofile['level'] == STAFF_LEVEL){?>
                                                            <a class="btn btn-primary btn-xs" title="Print"
                                                               href="nisit-view.php?__nisit_id=<?php echo $row['nisit_id']; ?>"><i
                                                                        class="fa fa-print"></i> </a>
                                                        <?php }?>
                                                        <?php if($uprofile['level'] == ADMIN_LEVEL){?>
                                                        <a class="btn btn-warning btn-xs" title="แก้ไข"
                                                           href="nisit-update.php?__nisit_id=<?php echo $row['nisit_id']; ?>"><i
                                                                class="fa fa-edit"></i> </a>
                                                        <a class="btn btn-danger btn-xs" title="ลบ"
                                                           onclick="deleteRowData('<?php echo $row['nisit_id']; ?>')"><i
                                                                class="fa fa-trash"></i> </a>
                                                        <?php } ?>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-12">
                                    <?php include "pageindex.php"; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <?php include "footer.php" ?>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>
    $('#menu-nisit-main').addClass('active');
    $('#menu-nisit').addClass('active');
</script>
</body>
</html>


