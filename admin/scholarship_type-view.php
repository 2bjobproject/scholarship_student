<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(STAFF_LEVEL)))
    redirect_to('index.php');

require_once "../connection.inc.php";

$cmd = getIsset("__cmd");
$test = array();
$scholarship_type_id = getIsset('__scholarship_type_id');
if ($cmd == "save") {
        $value = array(
            "scholarship_type_name" => getIsset('__scholarship_type_name'),
        );
        if ($scholarship_type_id == "0") {
            if ($conn->create("scholarship_type", $value)) {
                redirectTo("scholarship_type.php");
            }

        } else {
            if ($conn->update("scholarship_type", $value, array("scholarship_type_id" => $scholarship_type_id))) {
                redirectTo("scholarship_type.php");
            }
        }
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php include "css.php" ?>

</head>
<body class="skin-custom sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                ข้อมูลประเภททุน
                <small>จัดการข้อมูลประเภททุน</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>
                <li><a href="scholarship_type.php">ประเภททุน</a></li>
                <li class="active">จัดการข้อมูล</li>
            </ol>
        </section>
        <section class="content">
            <form class="form-horizontal" id="form_data" name="form_data" method="post" enctype="multipart/form-data">
                <input id="__cmd" name="__cmd" type="hidden" value="">
                <div class="box box-custom">
                    <div class="box-header with-border">
                        <h3 class="box-title">จัดการข้อมูลประเภททุน </h3>
                    </div>
                    <div class="box-body">
                        <input type="hidden" name="__scholarship_type_id" id="__scholarship_type_id" class="form-control"
                               value="0"
                               required="true" readonly>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    ชื่อ :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="__scholarship_type_name" id="__scholarship_type_name"
                                       class="form-control"
                                       value="" readonly
                                       onblur="trimValue(this);" required="true">
                            </div>
                        </div>

                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <a class="btn btn-default" href="scholarship_type.php">ย้อนกลับ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <?php include "footer.php" ?>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>
    $('#menu-scholarship_type-main').addClass('active');
    $('#menu-scholarship_type').addClass('active');
    function helpReturn(value, action) {
        $.ajax({
            url: 'Allservice.php',
            data: {id: value, action: action},
            method: 'GET',
            success: function (result) {
                var data = JSON.parse(result);
                if (action == "getScholarship_typeById") {
                    if (data.scholarship_type_id != null) {
                        console.log(data);
                        setValueScholarship_type(data);
                    }
                }
            }
        });
    }
    function setValueScholarship_type(data) {
        with (document.form_data) {
            $("#__scholarship_type_id").val(data.scholarship_type_id);
            $("#__scholarship_type_name").val(data.scholarship_type_name);

        }
    }
</script>
<script>helpReturn('<?php echo $scholarship_type_id;?>', 'getScholarship_typeById')</script>
</body>
</html>


