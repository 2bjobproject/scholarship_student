<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(ADMIN_LEVEL,STAFF_LEVEL)))
    redirect_to('index.php');
require_once "../connection.inc.php";

$cmd = getIsset("__cmd");
if ($cmd == "delete") {
    $conn->delete("nisit_scholarship", array("nisit_scholarship_id" => getIsset('__delete_field')));
    redirectTo('check_scholarship.php');
}
$show = 0;
$filterDefault = " where 1=1 and nisit_scholarship.status_id=1";

$keyword = getIsset("keyword");
$option_val = getIsset("option");
if ($keyword != "") {
    $filterDefault .= " and " . $option_val . " like '%" . $keyword . "%'";
}

if ($cmd == "search") {
    $show  = 1;
}

$sql = "select nisit_scholarship.*,status_name,first_name,last_name,scholarship_name,semester_name from nisit_scholarship
left join nisit on nisit.nisit_id=nisit_scholarship.nisit_id
left join scholarship on scholarship.scholarship_id=nisit_scholarship.scholarship_id
left join status on status.status_id=nisit_scholarship.status_id
left join semester on semester.semester_id=scholarship.semester_id
";
$result_row = $conn->queryRaw($sql . $filterDefault);//คิวรี่ คำสั่ง
$total = sizeof($result_row);
$for_end = $limit;
$for_start = $start * $limit;

$select_all = $conn->queryRaw($sql . $filterDefault . " order by nisit_scholarship_id desc   " );
$total_num = sizeof($select_all);
$user_id = $uprofile['id'];
$approve_date =  date('Y-m-d') ;
if ($cmd == "save") {
    $check = getIsset('status');
    if($check){
        foreach ($check as $row){

            $value = array(
                "status_id" => 2,
                "employee_id" => $user_id,
                "approve_date" =>$approve_date
            );
            $insert_result = $conn->update("nisit_scholarship", $value,array('nisit_scholarship_id'=>$row));
            $sql = "select nisit_scholarship.*,email,status_name,first_name,last_name,scholarship_name,semester_name from nisit_scholarship
left join nisit on nisit.nisit_id=nisit_scholarship.nisit_id
left join scholarship on scholarship.scholarship_id=nisit_scholarship.scholarship_id
left join status on status.status_id=nisit_scholarship.status_id
left join semester on semester.semester_id=scholarship.semester_id where nisit_scholarship_id = $row
";
            $nisit = $conn->queryRaw($sql ,true);//คิวรี่ คำสั่ง
            try {
                require '../plugins/PHPMailer/PHPMailerAutoload.php';
                $mail = new PHPMailer;
                $mail->SMTPOptions = array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                );

                $mail->isSMTP();
                $mail->CharSet = "utf-8";
                $mail->Host = 'smtp.gmail.com';
                $mail->SMTPAuth = true;
                $mail->SMTPKeepAlive = true;
                $mail->Port = 587;
                $mail->Username = 'katejeerawontestsendmail@gmail.com';
                $mail->Password = 'katejeerawon1234';
                $mail->setFrom('katejeerawontestsendmail@gmail.com', "Mail Testing");
                $mail->Subject = 'Your Username Password';
                $mail->addAddress($nisit['email'], $nisit['first_name'] . ' ' . $nisit['last_name']);
                $mail->msgHTML("แจ้งผลทุนการศึกษา  ".$nisit['scholarship_name']."
เรียน...".$nisit['first_name']. ' ' . $nisit['last_name']."..
ตามที่ท่านได้ยื่นความประสงค์สมัครขอรับทุนการศึกษา..".$nisit['scholarship_name']."..ในภาคเรียนที่..".$nisit['semester_name']."..นั้น
ทางมหาวิทยาลัยเทคโนโลยีพระจอมเกล้าพระนครเหนือได้ทำการคัดเลือกและประกาศรายชื่อนักศึกษา
ที่ได้รับทุนเรียบร้อยแล้ว จึงเรียนมาเพื่อแจ้งให้ท่านทราบว่า ท่าน..".$nisit['status_name']."..ดังกล่าว
ระบบการจัดการฐานข้อมูลทุนการศึกษา กองกิจการนักศึกษามมหาวิทยาลัยเทคโนโลยีพระจอมเกล้าพระนครเหนือ
***อีเมลนี้ส่งโดยระบบอัตโนมัติโปรดอย่าตอบกลับอีเมล***
");

                if ($mail->send()) {
                    $mail->clearAddresses();
                    $mail->clearAttachments();
                    alertMassage('ส่งสำเร็จ');
                    echo "<script language='javascript'>";
                    echo " alert('ยืนยันการอนุมัติทุน'); ";
                    echo " top.window.location = 'check_scholarship.php'; ";
                    echo "</script>";
                } else {
                    echo "<script language='javascript'>";
                    echo " alert('ส่งไม่สำเร็จ'); ";
                    echo " top.window.location = 'check_scholarship.php'; ";
                    echo "</script>";
                }
            } catch (Exception $e) {
                echo $e->getMessage();
            }


        }
    }

}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php require_once "css.php" ?>
</head>
<body class="skin-custom sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                ข้อมูลรออนุมัติทุน
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <form class="form-horizontal" id="form_data" name="form_data" method="post">
                <input id="__delete_field" name="__delete_field" type="hidden" value="">
                <input id="__cmd" name="__cmd" type="hidden" value="">
                <div class="box box-custom">
                    <div class="box-header with-border">
                        <h3 class="box-title">จัดการข้อมูลรออนุมัติทุน</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-7">
<!--                                <a class="btn btn-primary" href="nisit_scholarship-update.php"><i-->
<!--                                            class="fa fa-plus"></i> เพิ่มข้อมูล</a>-->
                            </div>
                            <div class="col-sm-2">
                                <select id="option" name="option" class="form-control">
                                    <option value="scholarship_name">ทุน</option>
                                    <option value="nisit_scholarship.register_date">วันที่ลงทะเทียน</option>
                                    <option value="status_name">สถานะ</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <input class="form-control" type="text" id="keyword" name="keyword"
                                           onblur="trimValue(this)" onkeyup="goSearch1()"
                                           value="<?php echo $keyword;?>">
                                    <a href="javascript:goSearch1();"
                                       class="btn btn-default input-group-addon"><i
                                            class="fa fa-search"></i> </a>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class=" table-responsive">
                                    <table class="table table-bordered table-striped" id="tbView">
                                        <tr>
                                            <th width="5%" class="text-center">อนุมัติ</th>
                                            <th width="5%" class="text-center">ลำดับ</th>
                                            <th width="10%">ชื่อ - นามสกุล</th>
                                            <th width="10%">ชื่อทุน</th>
                                            <th width="10%">ปีการศึกษา</th>
                                            <th width="10%">วันที่สมัครทุนการศึกษา</th>
                                            <th width="10%">สถานะ</th>
                                        </tr>
                                        <tbody>
                                        <?php
                                        $index = $for_start;
                                        foreach ($select_all as $row) {
                                            $index++;
                                            ?>
                                            <tr>
                                                <td class="text-center" nowrap>
                                                    <input type="checkbox" onclick="sssss(<?php echo $show?>)" class="chk1"
                                                           id="status<?php echo $row['nisit_scholarship_id']?>"
                                                           name="status[]" value="<?php echo $row['nisit_scholarship_id']?>">
                                                </td>
                                                <td class="text-center"
                                                    nowrap><?php echo $index; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['first_name']. ' ' .$row['last_name']; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['scholarship_name']; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['semester_name']; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['register_date']; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['status_name']; ?></td>
<!--                                                <td class="text-center" nowrap >-->
<!--                                                    <div class="btn-group">-->
<!--                                                        <a class="btn btn-success btn-xs"-->
<!--                                                           href="nisit_scholarship-update.php?__nisit_scholarship_id=--><?php //echo $row['nisit_scholarship_id']; ?><!--&__nisit_id=--><?php //echo $row['nisit_id']; ?><!--&status_id=1"><i-->
<!--                                                                class="fa fa-edit"></i> </a>-->
<!--                                                        <a class="btn btn-danger btn-xs"-->
<!--                                                           href="nisit_scholarship-update.php?__nisit_scholarship_id=--><?php //echo $row['nisit_scholarship_id']; ?><!--&__nisit_id=--><?php //echo $row['nisit_id']; ?><!--&status_id=0"><i-->
<!--                                                                    class="fa fa-edit"></i> </a>-->
<!---->
<!--                                                    </div>-->
<!--                                                </td>-->
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-12 text-center form-group">
                                    <a class="btn btn-success" href="javascript:goSave();" <?php if($show == 0) {?> style="display: none" <?php }?>>บันทึก</a>
                                    <a class="btn btn-warning" href="javascript:goClear()" <?php if($show == 0) {?> style="display: none" <?php }?>>ล้าง</a>
                                    <a class="btn btn-default" href="index.php">ย้อนกลับ</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <?php include "footer.php" ?>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>
    function sssss(id) {
        if (id == 0){
            alert('กรุณากดค้นหาชื่อทุน');
            $("input.chk1").prop('checked', false);

        }
    }
    function goSave() {
        if (confirm("คุณยืนยันการอนุมัติทุนจำนวน.."+ $("input[name^='status']").filter(':checked').length +"..คนหรือไม่")) {
            txt = " OK!";
            $('input[name=__cmd]').val("save");
            $('#form_data').submit();
        }
    }

    $('#menu-scholarship-main').addClass('active');
    $('#menu-nisit_scholarship').addClass('active');

    function goSearch1() {
        with (document.form_data) {
            __cmd.value = "search";
            if (typeof(document.getElementById("page")) != "undefined" && document.getElementById("page") != null) {
                document.getElementById("page").value = "1";
            }
            submit();
        }
    }
    var chartRenderIntervalId = null;
    var interval = 2400;
    function intervalChartRender() {
        chartRenderIntervalId = setInterval(() => {
            updateChart();
        }, interval);
    }


    function updateChart() {
        $.ajax({
            url: 'http://student-scholarship.com/apis/updateStatusDateApis.php',

            method: 'GET',
            success: function (result) {
                // var data = JSON.parse(result);
                // alert("xxxx");
                // if (action == "getNisit_scholarshipById") {
                //     if (data.nisit_scholarship_id != null) {
                //         setValueNisit_Scholarship(data);
                //     }
                // }
            }
        });
    }
</script>


<script>intervalChartRender()</script>
</body>
</html>


