<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(ADMIN_LEVEL, STAFF_LEVEL)))
    redirect_to('index.php');

require_once "../connection.inc.php";
$cmd = getIsset("__cmd");
$dataY = array();
$dataN = array();
$label = array();
$fac = $conn->queryRaw("Select * from faculty");
foreach ($fac as $row) {
    $item = array();
    $item = $row['faculty_name'];
    $label[] = $item;
}
$filterDefault = " where 1=1 ";
$show = 0;
$scholarship = getIsset("__scholarship_id");
$semester = getIsset("__semester_id");

if ($cmd == "save") {
    $show = 1;


    foreach ($fac as $row) {
        $yyy = $conn->queryRaw(" Select *  from nisit_scholarship
 left join nisit on nisit.nisit_id = nisit_scholarship.nisit_id
 left join nisit_education on nisit_education.nisit_id = nisit.nisit_id
 left join faculty on faculty.faculty_id = nisit_education.faculty_id
 left join scholarship on scholarship.scholarship_id = nisit_scholarship.scholarship_id
 left join semester on semester.semester_id = scholarship.semester_id
 where nisit_scholarship.scholarship_id = '" . $scholarship . "'
 and scholarship.semester_id = '" . $semester . "'
 and nisit_education.faculty_id = '" . $row['faculty_id'] . "'
 and status_id = 2
 ");
        $item = array();
        $item = sizeof($yyy);
        $dataY[] = $item;
    }
    foreach ($fac as $row) {
        $nnn = $conn->queryRaw(" Select *  from nisit_scholarship
 left join nisit on nisit.nisit_id = nisit_scholarship.nisit_id
 left join nisit_education on nisit_education.nisit_id = nisit.nisit_id
 left join faculty on faculty.faculty_id = nisit_education.faculty_id
 left join scholarship on scholarship.scholarship_id = nisit_scholarship.scholarship_id
 left join semester on semester.semester_id = scholarship.semester_id
 where nisit_scholarship.scholarship_id = '" . $scholarship . "'
 and scholarship.semester_id = '" . $semester . "'
 and nisit_education.faculty_id = '" . $row['faculty_id'] . "'
 
 ");
        $item = array();
        $item = sizeof($nnn);
        $dataN[] = $item;
    }
}


?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php require_once "css.php" ?>
    <style>
        .display {
            display: unset;
        }

        .text-red {
            color: red;
        }
    </style>

</head>
<body class="skin-custom sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div id="posContain" class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" id="form_data" name="form_data" enctype="multipart/form-data"
                          method="post">
                        <div class="">
                            <input id="__delete_field" name="__delete_field" type="hidden" value="">
                            <input id="__cmd" name="__cmd" type="hidden" value="">
                            <div class="col-md-12">
                                <label class="col-sm-3 control-label">
                                </label>
                            </div>
                            <div class="clr"></div>
                            <div class="col-sm-12">
                                <div class="box box-success">

                                    <div class="box-body">
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-3 control-label">
                                                    <i class="text-red">*</i> ปีการศึกษา :
                                                </label>
                                            </div>
                                            <div class="col-sm-3">
                                                <select name="__semester_id" id="__semester_id" class="form-control">
                                                    <option value="">เลือก</option>
                                                    <?php
                                                    $level = "SELECT * FROM semester";
                                                    $select_allaaa = $conn->queryRaw($level . " order by semester_id desc ");
                                                    foreach ($select_allaaa as $type) {
                                                        ?>
                                                        <option <?php echo getIsset("__semester_id") == $type['semester_id'] ? "selected" : ""; ?>
                                                                value="<?php echo $type['semester_id']; ?>"><?php echo $type['semester_name'] . ' ' . $type['year']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-3 control-label">
                                                    <i class="text-red">*</i> ทุน :
                                                </label>
                                            </div>
                                            <div class="col-sm-3">
                                                <select name="__scholarship_id" id="__scholarship_id"
                                                        class="form-control">
                                                    <option value="">เลือก</option>
                                                    <?php
                                                    $level = "SELECT * FROM scholarship";
                                                    $select_allaaa = $conn->queryRaw($level . " order by scholarship_id desc ");
                                                    foreach ($select_allaaa as $type) {
                                                        ?>
                                                        <option <?php echo getIsset("__scholarship_id") == $type['scholarship_id'] ? "selected" : ""; ?>
                                                                value="<?php echo $type['scholarship_id']; ?>"><?php echo $type['scholarship_name']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-3 control-label">
                                                </label>
                                            </div>
                                            <div class="col-sm-5">
                                                <?php if (getIsset("__scholarship_type_id") and getIsset("__scholarship_id") and getIsset("__scholarship_type_id")) ?>
                                                <a class="btn btn-success" href="javascript:goSave();"
                                                   onkeyup="goSave()">ค้นหา</a>
                                                <a class="btn btn-warning" href="statistics.php">ล้าง</a>
                                            </div>
                                        </div>


                                        <div class="form-group" <?php if ($show == 0) { ?> style="display: none" <?php } ?>>
                                            <div class="col-sm-12">
                                                <canvas id="myChart" width="400" height="200"></canvas>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>

<?php require_once 'javascript.php'; ?>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
<!-- Page script -->

<script>


    $(document).ready(function () {

        showChart(null);
    });

    function showChart() {
        var ctx = document.getElementById("myChart").getContext("2d");
        // var data = {
        //     labels: ['คณะวิศวกรรมศาสตร์','คณะศิลปศาสตร์ประยุกต์','คณะวิทยาลัยเทคโนโลยีอุตสาหกรรม','พัฒนาธุรกิจและอุตสาหกรรม','คณะครุศาสตร์อุตสาหกรรม','คณะวิทยาศาสตร์ประยุกต์','คณะสถาปัตยกรรมและการออกแบบ'],
        //     datasets: [
        //         {
        //             label: "นักศึกษาที่สมัครทุน",
        //             backgroundColor: "#3385ff",
        //             data: [25,15,20,30,20,23,20]
        //         },
        //         {
        //             label: "นักศึกษาที่ได้รับทุน",
        //             backgroundColor: "#ff4d4d",
        //             data: [23,20,27,27,23,20,23]
        //         },
        //     ]
        // };

        var data = {
            labels: <?php echo json_encode($label)?>,
            datasets: [
                {
                    label: "นักศึกษาที่สมัครทุน",
                    backgroundColor: "#3385ff",
                    data: <?php echo json_encode($dataN,JSON_NUMERIC_CHECK)?>
                },
                {
                    label: "นักศึกษาที่ได้รับทุน",
                    backgroundColor: "#ff4d4d",
                    data: <?php echo json_encode($dataY,JSON_NUMERIC_CHECK)?>
                },
            ]
        };

        var myBarChart = new Chart(ctx, {
            type: 'bar',
            data: data,
            options: {
                barValueSpacing: 20,
                scales: {
                    yAxes: [{
                        ticks: {
                            min: 0,
                            stepSize: 5
                        },
                    }]
                }
            }
        });
    }


    function goSave() {

        var semester_id = $("#__semester_id").val();
        var scholarship_id = $("#__scholarship_id").val();
        if (semester_id && scholarship_id != "") {
            var s1 = $("#__scholarship_id").val();
            var s2 = $("#__semester_id").val();
            if (s1 && s2 != null) {
                $('input[name=__cmd]').val("save");
                $('#form_data').submit();
            } else {
                alert("ยืนยันการทำรายการหรือไม่");
                $('input[name=__cmd]').val("save");
                $('#form_data').submit();
            }

        } else {
            alert("กรุณาระบุข้อมูลปีการศึกษา ");
        }
    }


    $('#menu-report2').addClass('active'); 


</script>
</body>
</html>


