<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(STAFF_LEVEL)))
    redirect_to('index.php');
require_once "../connection.inc.php";
$user_id = $uprofile['id'];
$cmd = getIsset("__cmd");
$test = array();
$nisit_scholarship_id = getIsset('__nisit_scholarship_id');
$event_id = getIsset('__event_id');
$nisit_id = $conn->select("nisit_scholarship", array("nisit_scholarship_id" => $nisit_scholarship_id), true);
if ($cmd == "save") {
    $value = array(
        "employee_id" => $user_id,
        "is_save_event" => 1,
    );
    if ($conn->update("nisit_scholarship", $value, array("nisit_scholarship_id" => $nisit_scholarship_id))) {
        $conn->delete("nisit_event", array("nisit_scholarship_id" => $nisit_scholarship_id));
    }
    $list = json_decode(getIsset('__list_json'));
    foreach ($list as $key => $item) {
        $child = array(
            "nisit_id" => getIsset('__nisit_id'),
            "event_id" => $item->event_id,
            "register_date" => $item->register_date,
        );
        $conn->create("nisit_event", $child);
    }
    redirectTo("nisit_event-detail.php");
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php include "css.php" ?>

</head>
<body class="skin-custom sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                รายการขอทุน
                <small>จัดการข้อมูลรายการขอทุน</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>
                <li><a href="nisit_event-detail.php">รายการขอทุน</a></li>
                <li class="active">จัดการข้อมูล</li>
            </ol>
        </section>
        <section class="content">
            <form class="form-horizontal" id="form_data" name="form_data" method="post" enctype="multipart/form-data">
                <input id="__cmd" name="__cmd" type="hidden" value="">
                <input id="__list_json" name="__list_json" type="hidden" value="[]">
                <div class="box box-custom">
                    <div class="box-header with-border">
                        <h3 class="box-title">จัดการข้อมูลการขอทุน </h3>
                    </div>
                    <div class="box-body">
                        <input type="hidden" name="__nisit_scholarship_id" id="__nisit_scholarship_id" class="form-control"
                               value="0" readonly>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    ชื่อนักศึกษา :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <input type="hidden" name="__nisit_id" id="__nisit_id"
                                           class="form-control"
                                           value=""
                                           readonly>
                                    <input type="text" name="__nisit_name" id="__nisit_name"
                                           class="form-control" value=""
                                           onblur="trimValue(this);" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    ชื่อทุน :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <input type="hidden" name="__scholarship_id" id="__scholarship_id"
                                           class="form-control"
                                           value=""
                                           readonly>
                                    <input type="text" name="__scholarship_name" id="__scholarship_name"
                                           class="form-control"
                                           value="" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    วันที่ลงทะเบียน :
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="register_date" id="register_date"
                                       class="form-control"
                                       value="<?php echo date("Y-m-d"); ?>" required readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <hr>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    กิจกรรม :
                                </label>
                            </div>
                            <div class="col-sm-5">

                                <div class="input-group">
                                    <input type="hidden" name="__event_id" id="__event_id"
                                           class="form-control"
                                           value=""
                                           readonly>
                                    <input type="text" name="__event_name" id="__event_name"
                                           class="form-control" value=""
                                           onblur="trimValue(this);" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    วันที่ลงทะเบียน :
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="__register_date" id="__register_date"
                                       class="form-control"
                                       value=""  readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped" id="tbView">
                                        <tbody>
                                        <tr>
                                            <th width="5%">ลำดับ</th>
                                            <th width="20%">ชื่อกิจกรรม</th>
                                            <th width="5%">จัดการ</th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <a class="btn btn-default" href="nisit_event-detail.php">ย้อนกลับ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <?php include "footer.php" ?>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>
    $('#menu-scholarship-main').addClass('active');
    $('#menu-event-').addClass('active');
    function helpReturn(value, action) {
        $.ajax({
            url: 'Allservice.php',
            data: {id: value, action: action},
            method: 'GET',
            success: function (result) {
                var data = JSON.parse(result);
                if (action == "getNisit_scholarshipById") {
                    if (data.nisit_scholarship_id != null) {
                        setValueNisit_Scholarship(data);
                    }
                }
                if (action == "getScholarshipById") {
                    if (data.Scholarship_id != null) {
                        setValueScholarship(data);
                    }
                }
                if (action == "getEventById") {
                    if (data.event_id != null) {
                        setValueEvent(data);
                    }
                }
                if (action == "getNisitById") {
                    if (data.nisit_id != null) {
                        setValueNisit(data);
                    }
                }
            }
        });
    }
    function setValueNisit_Scholarship(data) {
        with (document.form_data) {
            $("#__nisit_scholarship_id").val(data.nisit_scholarship_id);
            $("#__nisit_id").val(data.nisit_id);
            $("#__scholarship_id").val(data.scholarship_id);
            $("#register_date").val(data.register_date);
            $("#__event_id").val(data.event_id);
            $("#__event_name").val(data.event_name);
            $("#__scholarship_name").val(data.scholarship_name);
            $("#__nisit_name").val(data.nisit_name);
            setListJson(data.child);
            setTable(data.child);
        }
    }
    function setValueScholarship(data) {
        with (document.form_data) {
            $("#__scholarship_id").val(data.scholarship_id);
            $("#__scholarship_name").val(data.scholarship_name);
        }
    }
    function setValueEvent(data) {
        with (document.form_data) {
            $("#__event_id").val(data.event_id);
            $("#__event_name").val(data.event_name);
        }
    }
    function setValueNisit(data) {
        with (document.form_data) {
            $("#__nisit_id").val(data.nisit_id);
            $("#__nisit_name").val(data.first_name + ' ' + data.last_name);
        }
    }

    function delSession(index) {
        if (confirm('ต้องการลบหรือไม่')) {
            var data = getListJson();
            data.splice(index, 1);
            setTable(data);
            setListJson(data);
        }
    }
    function getListJson() {
        return JSON.parse($('#__list_json').val());
    }
    function setListJson(obj) {
        $('#__list_json').val(JSON.stringify(obj));
    }
    function getJsonByIndex(index) {
        return getListJson()[index];
    }
    function getDefaultTableHeader() {
        return '<tr><th width="5%">ลำดับ</th><th width="20%">ชื่อกิจกรรม</th> <th width="20%">วันที่ลงทะเบียน</th> <th width="10%">จัดการ</th></tr>';
    }
    function setTable(data) {
        var table = $("#tbView");
        table.find("tbody").empty();
        table.find("tbody").append(getDefaultTableHeader());
        $.each(data, function (index, value) {
            var tag = ["<tr>"];
            tag.push("<td class=' text-right'>" + (index + 1) + "</td>");
            tag.push("<td class=''>" + value.event_name + "</td>");
            tag.push("<td class=''>" + value.register_date + "</td>");
            tag.push("<td class=' text-center'><div class='btn-group'>" + '<a class="btn btn-warning btn-xs" onclick="editSession(\'' + index + '\')"><i class="fa fa-edit"></i></a><a class="btn btn-danger btn-xs" onclick="delSession(\'' + index + '\')"><i class="fa fa-trash"></i></a> </div></td>');
            tag.push("</tr>");
            table.find("tbody").append(tag.join(""));
        });
        setListJson(data);
    }
    function editSession(index) {
        var data = getListJson()[index];
        $("#__event_id").val(data.event_id);
        $("#__event_name").val(data.event_name);
        $("#__register_date").val(data.register_date);
    }

    function saveModal() {
        var event_id = $("#__event_id").val();
        var event_name = $("#__event_name").val();
        var register_date = $("#__register_date").val();

        if (event_id == "" || event_name == "") {
            alert("กรอกข้อมูลไม่ครบ");
        } else {
            var data = getListJson();
            var obj = {};
            obj.event_id = event_id;
            obj.event_name = event_name;
            obj.register_date = register_date;

            data.push(obj);
            setTable(data);
            setInit();
        }
    }
    function setInit() {
        $("#__event_id").val('');
        $("#__event_name").val('');
        $("#__register_date").val('');
    }
</script>
<script>helpReturn('<?php echo $nisit_scholarship_id;?>', 'getNisit_scholarshipById')</script>
</body>
</html>


