<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(ADMIN_LEVEL)))
    redirect_to('index.php');

require_once "../connection.inc.php";

$cmd = getIsset("__cmd");
$test = array();
$nisit_event_id = getIsset('__nisit_event_id');
if ($cmd == "save") {
    $value = array(
        "nisit_id" => getIsset('__nisit_id'),
        "event_id" => getIsset('__event_id'),
        "register_date" => date("Y-m-d H:i:s"),
    );
    if ($nisit_event_id == "0") {
        if ($conn->create("nisit_event", $value)) {
            redirectTo("nisit_event.php");
        }

    } else {
        if ($conn->update("nisit_event", $value, array("nisit_event_id" => $nisit_event_id))) {
            redirectTo("nisit_event.php");
        }
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php include "css.php" ?>

</head>
<body class="skin-custom sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                จัดการข้อมูลกิจกรรมนักศึกษา
            </h1>

        </section>
        <section class="content">
            <form class="form-horizontal" id="form_data" name="form_data" method="post" enctype="multipart/form-data">
                <input id="__cmd" name="__cmd" type="hidden" value="">
                <div class="box box-custom">
                    <div class="box-body">
                        <input type="hidden" name="__nisit_event_id" id="__nisit_event_id" class="form-control"
                               value="0"
                               required="true" readonly>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    ชื่อกิจกรรม :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <input type="hidden" name="__event_id" id="__event_id"
                                           class="form-control"
                                           value=""
                                           readonly>
                                    <input type="text" name="__event_name" id="__event_name"
                                           class="form-control"
                                           value="" readonly required>
                                    <a href="javascript:goPage('linkhelp.php?__filter=event&__action=getEventById');"
                                       class="btn btn-default input-group-addon"><i
                                                class="fa fa-search"></i> </a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    นักศึกษา :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <input type="hidden" name="__nisit_id" id="__nisit_id"
                                           class="form-control"
                                           value=""
                                           readonly>
                                    <input type="text" name="__nisit_name" id="__nisit_name"
                                           class="form-control"
                                           value="" readonly required>
                                    <a href="javascript:goPage('linkhelp.php?__filter=nisit&__action=getNisitById');"
                                       class="btn btn-default input-group-addon"><i
                                                class="fa fa-search"></i> </a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <a class="btn btn-success" href="javascript:goSave();">บันทึก</a>
                                <a class="btn btn-warning" href="javascript:goClear()">ล้าง</a>
                                <a class="btn btn-default" href="nisit_event.php">ย้อนกลับ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <?php include "footer.php" ?>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>
    $('#menu-nisit-main').addClass('active');
    $('#menu-nisit_event').addClass('active');
    function helpReturn(value, action) {
        $.ajax({
            url: 'Allservice.php',
            data: {id: value, action: action},
            method: 'GET',
            success: function (result) {
                var data = JSON.parse(result);
                if (action == "getNisit_eventById") {
                    if (data.nisit_event_id != null) {
                        console.log(data);
                        setValueNisit_event(data);
                    }
                }
                if (action == "getNisitById") {
                    if (data.nisit_id != null) {
                        console.log(data);
                        setValueNisit(data);
                    }
                }
                if (action == "getEventById") {
                    if (data.event_id != null) {
                        console.log(data);
                        setValueEvent(data);
                    }
                }
            }
        });
    }

    function setValueNisit_event(data) {
        with (document.form_data) {
            $("#__nisit_event_id").val(data.nisit_event_id);
            $("#__nisit_id").val(data.nisit_id);
            $("#__event_id").val(data.event_id);
            $("#__register_date").val(data.register_date);
            $("#__nisit_name").val(data.first_name+ ' '+data.last_name);
            $("#__event_name").val(data.event_name);
        }
    }

    function setValueNisit(data) {
        with (document.form_data) {
            $("#__nisit_id").val(data.nisit_id);
            $("#__nisit_name").val(data.first_name+ ' '+data.last_name);

        }
    }

    function setValueEvent(data) {
        with (document.form_data) {
            $("#__event_id").val(data.event_id);
            $("#__event_name").val(data.event_name);

        }
    }

</script>
<script>helpReturn('<?php echo $nisit_event_id;?>', 'getNisit_eventById')</script>
</body>
</html>


