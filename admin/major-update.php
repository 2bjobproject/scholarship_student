<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(ADMIN_LEVEL)))
    redirect_to('index.php');

require_once "../connection.inc.php";

$cmd = getIsset("__cmd");
$test = array();
$major_id = getIsset('__major_id');
if ($cmd == "save") {
        $value = array(
            "major_name" => getIsset('__major_name'),
        );
        if ($major_id == "0") {
            if ($conn->create("major", $value)) {
                redirectTo("major.php");
            }

        } else {
            if ($conn->update("major", $value, array("major_id" => $major_id))) {
                redirectTo("major.php");
            }
        }
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php include "css.php" ?>

</head>
<body class="skin-custom sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                จัดการข้อมูลระดับการศึกษา

            </h1>
        </section>
        <section class="content">
            <form class="form-horizontal" id="form_data" name="form_data" method="post" enctype="multipart/form-data">
                <input id="__cmd" name="__cmd" type="hidden" value="">
                <div class="box box-custom">
                    <div class="box-body">
                        <input type="hidden" name="__major_id" id="__major_id" class="form-control"
                               value="0"
                               required="true" readonly>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    ชื่อระดับการศึกษา :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="__major_name" id="__major_name"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true">
                            </div>
                        </div>

                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <a class="btn btn-success" href="javascript:goSave();">บันทึก</a>
                                <a class="btn btn-warning" href="javascript:goClear()">ล้าง</a>
                                <a class="btn btn-default" href="major.php">ย้อนกลับ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <?php include "footer.php" ?>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>
    $('#menu-major-main').addClass('active');
    $('#menu-major').addClass('active');
    function helpReturn(value, action) {
        $.ajax({
            url: 'Allservice.php',
            data: {id: value, action: action},
            method: 'GET',
            success: function (result) {
                var data = JSON.parse(result);
                if (action == "getMajorById") {
                    if (data.major_id != null) {
                        console.log(data);
                        setValueMajor(data);
                    }
                }
            }
        });
    }
    function setValueMajor(data) {
        with (document.form_data) {
            $("#__major_id").val(data.major_id);
            $("#__major_name").val(data.major_name);

        }
    }
</script>
<script>helpReturn('<?php echo $major_id;?>', 'getMajorById')</script>
</body>
</html>


