<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(ADMIN_LEVEL)))
    redirect_to('index.php');

require_once "../connection.inc.php";

$cmd = getIsset("__cmd");
$test = array();
$scholarship_id = getIsset('__scholarship_id');

if ($cmd == "saveFac") {
    if ($scholarship_id == "0") {
        if ($conn->create("scholarship", $value)) {
            $last_scholarship_id = $conn->getLastInsertId();
            $level = $conn->select('faculty');
            foreach ($level as $type) {
                if (getIsset('__faculty_' . $type['faculty_id']) != "") {
                    $value_ = array(
                        "scholarship_id" => $last_scholarship_id,
                        "faculty_id" => getIsset('__faculty_' . $type['faculty_id']),
                    );
                    if ($conn->create("scholarship_detail", $value_)) {

                    }
                }
            }
        }
    } else {
        if ($conn->update("scholarship", $value, array("scholarship_id" => $scholarship_id))) {
            $level = $conn->select('faculty');
            $conn->delete("scholarship_detail", array("scholarship_id" => $scholarship_id));
            foreach ($level as $type) {
                if (getIsset('__faculty_' . $type['faculty_id']) != "") {
                    $value_ = array(
                        "scholarship_id" => $scholarship_id,
                        "faculty_id" => getIsset('__faculty_' . $type['faculty_id']),
                    );
                    if ($conn->create("scholarship_detail", $value_)) {

                    }
                }
            }
        }
    }
}
?>
