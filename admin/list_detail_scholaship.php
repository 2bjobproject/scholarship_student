<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(ADMIN_LEVEL,STAFF_LEVEL)))
    redirect_to('index.php');

require_once "../connection.inc.php";

$cmd = getIsset("__cmd");
$test = array();
$nisit_scholarship_id = getIsset('__nisit_scholarship_id');

$sql = "select nisit_scholarship.*,event_name,nisit_event.register_date,concat(nisit.first_name,' ',nisit.last_name) as nisit_name from nisit_scholarship
left join nisit on nisit.nisit_id=nisit_scholarship.nisit_id 
left join nisit_event on nisit_event.nisit_id=nisit.nisit_id 
left join event on event.event_id=nisit_event.event_id 
where nisit_scholarship_id=$nisit_scholarship_id
";
$result_row = $conn->queryRaw($sql);//คิวรี่ คำสั่ง
$total = sizeof($result_row);
$for_end = $limit;
$for_start = $start * $limit;

$select_all = $conn->queryRaw($sql . " order by nisit_scholarship_id desc  limit " . $for_start . "," . $for_end);

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php include "css.php" ?>

</head>
<body class="skin-custom sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                จัดการข้อมูลประวัติ

            </h1>
        </section>
        <section class="content">
            <form class="form-horizontal" id="form_data" name="form_data" method="post" enctype="multipart/form-data">
                <input id="__cmd" name="__cmd" type="hidden" value="">
                <div class="box box-custom">
                    <div class="box-body">
                        <input type="hidden" name="__faculty_id" id="__faculty_id" class="form-control"
                               value="0"
                               required="true" readonly>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    ชื่อผู้ลงทะเบียน :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="__nisit_name" id="__nisit_name"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    ชื่อทุน :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="__scholarship_name" id="__scholarship_name"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    วันที่ลงทะเบียน :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="__register_date" id="__register_date"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    สถานะ :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="__status_name" id="__status_name"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    วันที่อนุมัติ :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="__approve_date" id="__approve_date"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    ชื่อผู้ใช้งาน :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="__employee_name" id="__employee_name"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    ภาควิชา :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="__department_name" id="__department_name"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    สาขา :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="__major_name" id="__major_name"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    คณะ :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="__faculty_name" id="__faculty_name"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <a class="btn btn-default" href="scholarship-nisit.php">ย้อนกลับ</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class=" table-responsive">
                                    <table class="table table-bordered table-striped" id="tbView">
                                        <tr>
                                            <th width="5%">ลำดับ</th>
                                            <th width="20%">ชื่อกิจกรรม</th>
                                            <th width="20%">วันที่ลงทะเบียน</th>
                                        </tr>
                                        <tbody>
                                        <?php
                                        $index = $for_start;
                                        foreach ($select_all as $row) {
                                            $index++;
                                            ?>
                                            <tr>
                                                <td class="text-right"
                                                    nowrap><?php echo $index; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['event_name']; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['register_date']; ?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <?php include "footer.php" ?>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>
    $('#menu-scholarship-main').addClass('active');
    $('#menu-list-sc-').addClass('active');

    function helpReturn(value, action) {
        $.ajax({
            url: 'Allservice.php',
            data: {id: value, action: action},
            method: 'GET',
            success: function (result) {
                var data = JSON.parse(result);
                if (action == "getNisit_scholarshipById") {
                    if (data.nisit_scholarship_id != null) {
                        console.log(data);
                        setValueNisit_scholarship(data);
                    }
                }
                if (action == "getNisitById") {
                    if (data.nisit_id != null) {
                        console.log(data);
                        setValueNisit(data);
                    }

                }

            }
        });
    }

    function setValueNisit_scholarship(data) {
        with (document.form_data) {
            $("#__scholarship_id").val(data.scholarship_id);
            $("#__nisit_id").val(data.nisit_id);
            $("#__nisit_name").val(data.nisit_name);
            $("#__scholarship_name").val(data.scholarship_name);
            $("#__register_date").val(data.register_date);
            $("#__status_id").val(data.status_id);
            $("#__status_name").val(data.status_name);
            $("#__employee_id").val(data.employee_id);
            $("#__employee_name").val(data.employee_name);
            $("#__is_save_event").val(data.is_save_event);
            $("#__department_name").val(data.department_name);
            $("#__major_name").val(data.major_name);
            $("#__faculty_name").val(data.faculty_name);
            $("#__approve_date").val(data.approve_date);

        }
    }
    function setValueNisit(data) {
        with (document.form_data) {
            $("#__nisit_id").val(data.nisit_id);
            $("#__nisit_name").val(data.first_name + ' ' + data.last_name);
        }
    }
</script>
<script>helpReturn('<?php echo $nisit_scholarship_id;?>', 'getNisit_scholarshipById')</script>
</body>
</html>


