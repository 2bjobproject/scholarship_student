<header class="main-header">
    <!-- Logo -->
    <a class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>LT</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Admin</b>LTE</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
<!--                        <img src="--><?php //echo PATH_UPLOAD . $uprofile['picture']; ?><!--"-->
<!--                             onerror="this.src='../dist/img/user2-160x160.jpg'"-->
<!--                             class="user-image">-->
                        <span class="hidden-xs"><?php echo $uprofile['name']; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo PATH_UPLOAD . $uprofile['picture']; ?>"
                                 onerror="this.src='../dist/img/user2-160x160.jpg'" class="img-circle" alt="User Image">
                            <p class="text-center" style="margin: 0"><?php echo $uprofile['name']; ?></p>
                            <div class="text-left">
                                <small class="text-white">ระดับผู้ใช้งาน : <?php echo $uprofile['level_name']; ?></small>
                                <br>
                                <small class="text-white">เบอร์โทรศัพท์ : <?php echo $uprofile['tel']; ?></small>
                            </div>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="employee-update.php?__employee_id=<?php echo $uprofile['id']?>" class="btn btn-default btn-flat">ข้อมูลส่วนตัว</a>
                            </div>
                            <div class="pull-right">
                                <a href="logout.php" class="btn btn-default btn-flat">ออกจากระบบ</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
