<?php
session_start();
require_once '../mpdf/mpdf.php';
require_once '../connection.inc.php';

$semester = getIsset("__semester");

$sql = "SELECT scholarship.*,scholarship_name,faculty_name,semester_name,scholarship_type_name FROM scholarship
LEFT JOIN faculty on faculty.faculty_id = scholarship.faculty_id
LEFT JOIN semester on semester.semester_id = scholarship.semester_id
LEFT JOIN scholarship_type on scholarship_type.scholarship_type_id = scholarship.scholarship_type_id where scholarship.semester_id='$semester'  ";
$list = $conn->queryRaw($sql);
ob_start();
?>
    <html>
    <head>
        <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">

    </head>
    <body>
    <span style="font-size: 0.6em">พิมพ์เมื่อ <?php echo DateThai() ?></span>

    <div style="font-size: 18px;
    font-weight: bold;
    text-align: center;">
        <?php echo TITLE_THAI; ?>
    </div>
    <div style="font-size: 14px;;
    text-align: center;">
        <?php echo ADDRESS; ?>
    </div>
    <div style="text-align: center;">รายงานทุนการศึกษา</div>
    <div style="text-align: center;font-size: 10px">
        ประจำปี <?php echo DateMonthThaiByDate("$year"); ?>
    </div>
    <br>
    <table width="100%" border="1" cellspacing="0" cellpadding="1" align="center">
        <tbody>
        <tr>
            <th style="padding: 7px;
    font-size: 10px;
    background-color: #D5D5D5;
    text-align: center;" width="5%">ลำดับ
            </th>

            <th style="padding: 7px;
    font-size: 10px;
    width: 10%;
    background-color: #D5D5D5;
    text-align: center;">ทุน
            </th>
            <th style="padding: 7px;
    font-size: 10px;
    width: 15%;
    background-color: #D5D5D5;
    text-align: center;">จำนวนที่รับ
            </th>
            <th style="padding: 7px;
    font-size: 10px;
    width: 15%;
    background-color: #D5D5D5;
    text-align: center;">เทอม
            </th>
            <th style="padding: 7px;
    font-size: 10px;
    width: 15%;
    background-color: #D5D5D5;
    text-align: center;">ประเภททุน
            </th>
            <th style="padding: 7px;
    font-size: 10px;
    width: 15%;
    background-color: #D5D5D5;
    text-align: center;">คณะ
            </th>
        </tr>
        <?php
        foreach ($list as $index => $row) {
            ?>
            <tr>
                <td style="padding: 7px;
    vertical-align: top;
    font-size: 10px;
    background-color: white;
    text-align: center;"><?php echo $index + 1; ?></td>
                <td style="padding: 7px;
    vertical-align: top;
    font-size: 10px;
    background-color: white;
    text-align: left;"><?php echo $row['scholarship_name']; ?></td>
                <td style="padding: 7px;
    vertical-align: top;
    font-size: 10px;
    background-color: white;
    text-align: left;"><?php echo $row['amount_use']; ?></td>
                <td style="padding: 7px;
    vertical-align: top;
    font-size: 10px;
    background-color: white;
    text-align: left;"><?php echo $row['semester_name']; ?></td>
                <td style="padding: 7px;
    vertical-align: top;
    font-size: 10px;
    background-color: white;
    text-align: left;"><?php echo $row['scholarship_type_name']; ?></td>
                <td style="padding: 7px;
    vertical-align: top;
    font-size: 10px;
    background-color: white;
    text-align: left;"><?php echo $row['faculty_name']; ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <br>
    </body>
    </html>
<?php
$html = ob_get_contents();
ob_end_clean();
$pdf = new mPDF('th', 'A4-L', '0', 'TH SarabunPSK');
$pdf->autoPageBreak = true;
$pdf->WriteHTML($html, 2);
$pdf->Output();
?>