<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(ADMIN_LEVEL,STAFF_LEVEL)))
    redirect_to('index.php');

require_once "../connection.inc.php";

$cmd = getIsset("__cmd");
$semester = getIsset("__semester");



?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php include "css.php" ?>
</head>
<body class="skin-custom sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div id="posContain" class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" id="form_data" name="form_data" enctype="multipart/form-data"
                          method="post">
                        <div class="">
                            <input id="__delete_field" name="__delete_field" type="hidden" value="">
                            <input id="__cmd" name="__cmd" type="hidden" value="">
                            <div class="col-md-12">
                                <label class="col-sm-3 control-label">
                                </label>
                            </div>
                            <div class="clr"></div>
                            <div class="col-sm-12">
                                <div class="box box-custom">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">รายงานทุนการศึกษา</h3>
                                    </div>

                                    <div class="box-body">
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-3 control-label">
                                                    ปีการศึกษา :
                                                </label>
                                            </div>
                                            <div class="col-sm-2">
                                                <select name="__semester" id="__semester" class="form-control">
                                                    <?php
                                                    $semester = $conn->queryRaw('select * from semester');
                                                    foreach ($semester as $item) {
                                                        ?>
                                                        <option
                                                                value="<?php echo $item['semester_id']; ?>"><?php echo $item['semester_name']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-3 control-label">
                                                </label>
                                            </div>
                                            <div class="col-sm-5">
                                                <a class="btn btn-success" href="javascript:goPrint();">พิมพ์รายงาน</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>
    $('#menu-report-main,#menu-report-').addClass('active');
    function goPrint() {
        if(!required()){
            if (confirm("ต้องการพิมพ์รายงานหรือไม่")) {
                var semester = $("#__semester").val();
                window.open("print-scholarship.php?__semester=" + semester , "_blank");
            }
        }
    }
</script>
</body>
</html>


