<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(ADMIN_LEVEL)))
    redirect_to('index.php');

require_once "../connection.inc.php";

$cmd = getIsset("__cmd");
$test = array();
$faculty_id = getIsset('__faculty_id');
if ($cmd == "save") {
    $value = array(
        "faculty_name" => getIsset('__faculty_name'),
        "faculty_short" => getIsset('__faculty_short'),
    );
    if ($faculty_id == "0") {
        if ($conn->create("faculty", $value)) {
            redirectTo("faculty.php");
        }

    } else {
        if ($conn->update("faculty", $value, array("faculty_id" => $faculty_id))) {
            redirectTo("faculty.php");
        }
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php include "css.php" ?>

</head>
<body class="skin-custom sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                จัดการข้อมูลคณะ

            </h1>


        </section>
        <section class="content">
            <form class="form-horizontal" id="form_data" name="form_data" method="post" enctype="multipart/form-data">
                <input id="__cmd" name="__cmd" type="hidden" value="">
                <div class="box box-custom">
                    <div class="box-body">
                        <input type="hidden" name="__faculty_id" id="__faculty_id" class="form-control"
                               value="0"
                               required="true" readonly>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    ชื่อคณะ :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="__faculty_name" id="__faculty_name"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    ชื่อย่อ :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="__faculty_short" id="__faculty_short"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <a class="btn btn-success" href="javascript:goSave();">บันทึก</a>
                                <a class="btn btn-warning" href="javascript:goClear()">ล้าง</a>
                                <a class="btn btn-default" href="faculty.php">ย้อนกลับ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <?php include "footer.php" ?>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>
    $('#menu-fac-main').addClass('active');
    $('#menu-faculty').addClass('active');

    function helpReturn(value, action) {
        $.ajax({
            url: 'Allservice.php',
            data: {id: value, action: action},
            method: 'GET',
            success: function (result) {
                var data = JSON.parse(result);
                if (action == "getFacultyById") {
                    if (data.faculty_id != null) {
                        console.log(data);
                        setValueFaculty(data);
                    }
                }
            }
        });
    }

    function setValueFaculty(data) {
        with (document.form_data) {
            $("#__faculty_id").val(data.faculty_id);
            $("#__faculty_name").val(data.faculty_name);
            $("#__faculty_short").val(data.faculty_short);

        }
    }
</script>
<script>helpReturn('<?php echo $faculty_id;?>', 'getFacultyById')</script>
</body>
</html>


