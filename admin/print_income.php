<?php
session_start();
require_once '../mpdf/mpdf.php';
require_once '../connection.inc.php';
$filterDefault = " where 1=1 and status_id = 2";
$scholarship_type_id = getIsset("scholarship_type_id");
$scholarship_id = getIsset("scholarship_id");
$semester_id = getIsset("semester_id");
if ($scholarship_type_id != "") {
    $filterDefault .= " and " . 'scholarship.scholarship_type_id'  . " ='" . $scholarship_type_id . "'";
}
if ($scholarship_id != "") {
    $filterDefault .= " and " . 'nisit_scholarship.scholarship_id'  . "='" . $scholarship_id . "'";
}
if ($semester_id != "") {
    $filterDefault .= " and " . 'scholarship.semester_id'  . "='" . $semester_id . "'";
}
$sql = "SELECT nisit_scholarship.*,first_name,last_name,scholarship_name,faculty_name,semester_name,semester.year,scholarship_type_name,tall,phone FROM nisit_scholarship
LEFT JOIN scholarship on scholarship.scholarship_id = nisit_scholarship.scholarship_id
LEFT JOIN nisit on nisit.nisit_id = nisit_scholarship.nisit_id
LEFT JOIN nisit_education on nisit_education.nisit_id = nisit.nisit_id
LEFT JOIN faculty on faculty.faculty_id = nisit_education.faculty_id
LEFT JOIN semester on semester.semester_id = scholarship.semester_id
LEFT JOIN scholarship_type on scholarship_type.scholarship_type_id = scholarship.scholarship_type_id";
$select_all = $conn->queryRaw($sql . $filterDefault . " order by nisit_scholarship_id desc ");

$semester = $conn->queryRaw("select * from semester where semester_id='$semester_id'", true);
$scholarship = $conn->queryRaw("select * from scholarship where scholarship_id='$scholarship_id'", true);
ob_start();
?>
    <html>
    <head>
        <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">

    </head>
    <body>
    <span style="font-size: 0.6em">พิมพ์เมื่อ <?php echo DateThai() ?></span>

    <div style="font-size: 18px;
    font-weight: bold;
    text-align: center;">
       รายชื่อนักศึกษาที่ได้รับทุนการศึกษา
    </div>
    <div style="font-size: 14px;;
    text-align: center;">
        <?php if($scholarship_id!=''){
            echo 'ทุน '.$scholarship['scholarship_name'];
        }else{
            echo '';
        }
        ?>
    </div>
    <div style="text-align: center;">
        <?php if($semester_id!=''){
            echo 'ปีการศึกษา ' . $semester['semester_name'].' '.$semester['year'];
        }else{
            echo '';
        }
        ?>
    </div>

    <br>
    <table width="100%"   cellspacing="0" cellpadding="1" align="center">
        <tbody>
        <tr>
            <th style="padding: 7px;
    font-size: 14px;
    
    text-align: left;" width="3%">
            </th>

            <th style="padding: 7px;
    font-size: 14px;
    width: 20%;
    
    text-align: left;">ชื่อ - นามสกุล
            </th>

            <th style="padding: 7px;
    font-size: 14px;
    width: 20%;
    
    text-align: left;">คณะ
            </th>
            <th style="padding: 7px;
    font-size: 14px;
    width: 5%;
    
    text-align: left;">ชั้นปี
            </th>


            <th style="padding: 7px;
    font-size: 14px;
    width: 15%;
    
    text-align: left;">เบอร์โทร
            </th>
        </tr>
        <?php
        foreach ($select_all as $index => $row) {
            ?>
            <tr>
                <td style="padding: 7px;
    vertical-align: top;
    font-size: 14px;
    background-color: white;
    text-align: left;"><?php echo $index + 1; ?></td>
                <td style="padding: 7px;
    vertical-align: top;
    font-size: 14px;
    background-color: white;
    text-align: left;"><?php echo $row['first_name']. ' ' .$row['last_name']; ?></td>
                <td style="padding: 7px;
    vertical-align: top;
    font-size: 14px;
    background-color: white;
    text-align: left;"><?php echo $row['faculty_name']; ?></td>
                <td style="padding: 7px;
    vertical-align: top;
    font-size: 14px;
    background-color: white;
    text-align: left;"><?php echo $row['semester_name']; ?></td>
                <td style="padding: 7px;
    vertical-align: top;
    font-size: 14px;
    background-color: white;
    text-align: left;"><?php echo $row['phone']; ?></td>


            </tr>
        <?php } ?>
        </tbody>
    </table> 
    <br>
    <div style="position: fixed;bottom: 0">
    <div style="width: 100%;display: inline; line-height: 18pt">
        <div style="width: 65%;float:left; text-align: right">ลงชื่อ</div>
        <div style="float:left;">..........................................</div>
        <!--        <div style="width: 30%;float:left; text-align: right">ผู้สมัคร</div>-->
    </div>

    <div style="width: 100%;display: inline; line-height: 18pt">
        <div style="width: 65%;float:left; text-align: right">(</div>
        <div style="width: 30%;float:left;">....................................)</div>
        <!--                <div style="width: 20%;float:left; text-align: right"></div>-->
    </div>
    <div style="width: 100%;display: inline; line-height: 18pt">
        <div style="width: 65%;float:left; text-align: right">.</div>
        <div style="width: 30%;float:left;">........./............/..............</div>
        <!--        <div style="width: 20%;float:left; text-align: right"></div>-->
    </div>
    </div>
    </body>
    </html>
<?php
$html = ob_get_contents();
ob_end_clean();
$pdf = new mPDF('th', 'A4', '0', 'TH SarabunPSK');
$pdf->autoPageBreak = true;
$pdf->WriteHTML($html, 2);
$pdf->Output();
?>
