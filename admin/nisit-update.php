<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(ADMIN_LEVEL)))
    redirect_to('index.php');
require_once "../connection.inc.php";
$user_id = $uprofile['id'];
$cmd = getIsset("__cmd");
$major_id = getIsset("__major_id");
$faculty_id = getIsset("__faculty_id");
$scholarship_type_id = getIsset("scholarship_type_id");
$father_status_id = getIsset("father_status_id");
$mother_status_id = getIsset("mother_status_id");
$test = array();
$nisit_id = getIsset('__nisit_id');
if ($cmd == "save") {
    $value = array(
        "nisit_code" => getIsset('__nisit_code'),
        "first_name" => getIsset('__first_name'),
        "last_name" => getIsset('__last_name'),
        "id_card" => getIsset('__id_card'),
        "religion" => getIsset('__religion'),
        "birth_date" => getIsset('__birth_date'),
        "image_path" => uploadFile($_FILES['__file_upload'], getIsset('__image_path'), PATH_UPLOAD),
        "title_id" => getIsset('__title_id'),
        "weight" => getIsset('__weight'),
        "tall" => getIsset('__tall'),
        "national" => getIsset('__national'),
        "rac" => getIsset('__rac'),
        "address" => getIsset('__address'),
        "moo" => getIsset('__moo'),
        "soi" => getIsset('__soi'),
        "street" => getIsset('__street'),
        "zipcode" => getIsset('__zipcode'),
        "province_code" => getIsset('__province_code'),
        "amphur_code" => getIsset('__amphur_code'),
        "district_code" => getIsset('__district_code'),
        "phone" => getIsset('__phone'),
        "email" => getIsset('__email'),
        "present_address" => getIsset('__present_address'),
        "present_moo" => getIsset('__present_moo'),
        "present_soi" => getIsset('__present_soi'),
        "present_street" => getIsset('__present_street'),
        "present_province_code" => getIsset('__present_province_code'),
        "present_amphur_code" => getIsset('__present_amphur_code'),
        "present_district_code" => getIsset('__present_district_code'),
        "present_zipcode" => getIsset('__present_zipcode'),
        "present_phone" => getIsset('__present_phone'),
        "present_fax" => getIsset('__present_fax'),
        "account_type_id" => getIsset('__account_type_id'),
        "bank_id" => getIsset('__bank_id'),
        "account_no" => getIsset('__account_no'),
        "register_date" => date("Y-m-d H:i:s"),
        //        "password" => getIsset("__password"),
    );
    if ($nisit_id == "0") {
        if ($conn->create("nisit", $value)) {
            $nisit_id = $conn->getLastInsertId();
            $data = array("nisit_id" => $nisit_id);
            $conn->create("nisit_family", $data);
            $conn->create("nisit_event", $data);
            $conn->create("nisit_education", $data);
        }

    } else {
        if ($conn->update("nisit", $value, array("nisit_id" => $nisit_id))) {

        }
    }
    $value = array(
        "nisit_id" => $nisit_id,
        "education_level" => getIsset('__education_level'),
        "education_section" => getIsset('__education_section'),
        "education_from" => getIsset('__education_from'),
        "education_province_code" => getIsset('__education_province_code'),
        "major_id" => $major_id,
        "year" => getIsset('__year'),
        "GPA" => getIsset('__GPA'),
        "faculty_id" => $faculty_id,
        "department_id" => getIsset('__department_id'),
        "section_id" => getIsset('__section_id'),
        "talent" => getIsset('__talent'),
        //        "other_faculty" => getIsset('__other_faculty'),
        "employee_id" => $user_id,
    );

    $conn->update("nisit_education", $value, array("nisit_id" => $nisit_id));

    $value = array(
        "nisit_id" => $nisit_id,
        "father_first_name" => getIsset('__father_first_name'),
        "father_last_name" => getIsset('__father_last_name'),
        "father_ago" => getIsset('__father_ago'),
        "father_job" => getIsset('__father_job'),
        "father_salary" => getIsset('__father_salary'),
        "father_status_id" => $father_status_id,
        "father_other_status" => getIsset('__father_other_status'),
        "father_address" => getIsset('__father_address'),
        "father_phone" => getIsset('__father_phone'),
        "mother_first_name" => getIsset('__mother_first_name'),
        "mother_last_name" => getIsset('__mother_last_name'),
        "mother_ago" => getIsset('__mother_ago'),
        "mother_job" => getIsset('__mother_job'),
        "mother_salary" => getIsset('__mother_salary'),
        "mother_status_id" => $mother_status_id,
        "mother_other_status" => getIsset('__mother_other_status'),
        "mother_address" => getIsset('__mother_address'),
        "mother_phone" => getIsset('__mother_phone'),

        "parent_first_name" => getIsset('__parent_first_name'),
        "parent_last_name" => getIsset('__parent_last_name'),
        "parent_age" => getIsset('__parent_age'),
        "parent_job" => getIsset('__parent_job'),
        "parent_salary" => getIsset('__parent_salary'),
        "parent_abount" => getIsset('__parent_abount'),
        "parent_address" => getIsset('__parent_address'),
        "parent_phone" => getIsset('__parent_phone'),
    );
    $conn->update("nisit_family", $value, array("nisit_id" => $nisit_id));

//    redirectTo("nisit.php");
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php include "css.php" ?>

</head>
<body class="skin-custom sidebar-mini">

<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                จัดการข้อมูลนักศึกษา

            </h1>
        </section>
        <section class="content">
            <form class="form-horizontal" id="form_data" name="form_data" method="post" enctype="multipart/form-data">
                <input id="__cmd" name="__cmd" type="hidden" value="">
                <div class="box box-custom">
                    <div class="box-body">
                        <input type="hidden" name="__nisit_id" id="__nisit_id" class="form-control"
                               value="0"
                               required="true" readonly>
                        <div class="form-group">
                            <div class="form-group">
                                <div align="right">
                                    <label class="col-sm-3 control-label">
                                        รูปภาพ :
                                    </label>
                                </div>
                                <div class="col-sm-3">
                                    <img id="img-preview" src="" class="image img-thumbnail"
                                         onerror="src='../uploads/uploadfile.png'">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">

                                </label>

                                <div class="col-sm-5">
                                    <input type="file" name="__file_upload" style="display: none">
                                    <input type="hidden" name="__image_path" id="__image_path">
                                    <a type="button" class="btn btn-success btn-xs upload-logo">อัพโหลด</a>
                                    <a type="button" class="btn btn-danger btn-xs del-logo">ลบไฟล์</a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    รหัสนักศึกษา :
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="__nisit_code" id="__nisit_code"
                                       class="form-control"
                                       value=""
                                       maxlength="15" required
                                       onkeypress="chkInteger(event)">
                            </div>
                            <div align="right">
                                <label class="col-sm-2 control-label">
                                    คำนำหน้า :
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <select name="__title_id" id="__title_id" class="form-control">
                                    <?php
                                    $level = $conn->select('title');
                                    foreach ($level as $type) {
                                        ?>
                                        <option
                                                value="<?php echo $type['title_id']; ?>">
                                            <?php echo $type['title_name']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    ชื่อ :
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="__first_name" id="__first_name"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true">
                            </div>
                            <div align="right">
                                <label class="col-sm-2 control-label">
                                    นามสกุล :
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="__last_name" id="__last_name"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    เลขบัตรประจำตัวประชาชน :
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="__id_card" id="__id_card"
                                       class="form-control" value=""
                                       maxlength="13" required
                                       onkeypress="chkInteger(event)">
                            </div>

                            <div align="right">
                                <label class="col-sm-2 control-label">
                                    ศาสนา :
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="__religion" id="__religion"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    วันเกิด:
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="__birth_date"
                                       id="__birth_date" data-provide="datepicker" data-date-language="th-th"
                                       class="form-control"
                                       value="" readonly
                                       required>
                            </div>
                            <!--                            <div align="right">-->
                            <!--                                <label class="col-sm-2 control-label">-->
                            <!--                                    รหัสผ่าน:-->
                            <!--                                </label>-->
                            <!--                            </div>-->
                            <!--                            <div class="col-sm-3">-->
                            <!--                                <input type="password" name="__password"-->
                            <!--                                       id="__password"-->
                            <!--                                       class="form-control"-->
                            <!--                                       value=""-->
                            <!--                                       required>-->
                            <!--                            </div>-->
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-2 control-label">
                                    น้ำหนัก :
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" name="__weight" id="__weight"
                                       class="form-control" value=""
                                       onblur="trimValue(this);" required
                                       onkeypress="chkInteger(event)">
                            </div>
                            <div align="right">
                                <label class="col-sm-1 control-label">
                                    ส่วนสูง :
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" name="__tall" id="__tall"
                                       class="form-control" value=""
                                       onblur="trimValue(this);" maxlength="10" required
                                       onkeypress="chkInteger(event)">
                            </div>

                            <div align="right">
                                <label class="col-sm-1 control-label">
                                    สัญชาติ :
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" name="__national" id="__national"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-2 control-label">
                                    เชื่อชาติ :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="__rac" id="__rac"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-2 control-label">
                                    บ้านเลขที่ :
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" name="__address" id="__address"
                                       class="form-control" value=""
                                       onblur="trimValue(this);" required
                                       onkeypress="return chkNumberr()">
                            </div>

                            <div align="right">
                                <label class="col-sm-1 control-label">
                                    หมู่ :
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" name="__moo" id="__moo"
                                       class="form-control" value=""
                                       onblur="trimValue(this);" required
                                >
                            </div>
                            <div align="right">
                                <label class="col-sm-1 control-label">
                                    ซอย :
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" name="__soi" id="__soi"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-2 control-label">
                                    ถนน :
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="__street" id="__street"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true">
                            </div>
                            <div align="right">
                                <label class="col-sm-2 control-label">
                                    รหัสไปรษณี :
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="__zipcode" id="__zipcode"
                                       class="form-control" value=""
                                       onblur="trimValue(this);" maxlength="5" required
                                       onkeypress="chkInteger(event)">
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-2 control-label">
                                    จังหวัด :
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <input type="hidden" name="__province_code" id="__province_code"
                                           class="form-control"
                                           value=""
                                           readonly>
                                    <input type="text" name="__province_name" id="__province_name"
                                           class="form-control"
                                           value="" readonly required>
                                    <a href="javascript:goPage('linkhelp.php?__filter=province&__action=getProvinceById');"
                                       class="btn btn-default input-group-addon"><i
                                                class="fa fa-search"></i> </a>
                                </div>
                            </div>
                            <div align="right">
                                <label class="col-sm-2 control-label">
                                    อำเภอ(เขต) :
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <input type="hidden" name="__amphur_code" id="__amphur_code"
                                           class="form-control"
                                           value=""
                                           readonly>
                                    <input type="text" name="__amphur_name" id="__amphur_name"
                                           class="form-control"
                                           value="" readonly required>
                                    <a href="javascript:goPageReference('linkhelp.php?__filter=amphur&__action=getAmphurById','__province_code');"
                                       class="btn btn-default input-group-addon"><i
                                                class="fa fa-search"></i> </a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-2 control-label">
                                    ตำบล(แขวง) :
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <input type="hidden" name="__district_code" id="__district_code"
                                           class="form-control"
                                           value=""
                                           readonly>
                                    <input type="text" name="__district_name" id="__district_name"
                                           class="form-control"
                                           value="" readonly required>
                                    <a href="javascript:goPageReference('linkhelp.php?__filter=district&__action=getDistrictById','__amphur_code');"
                                       class="btn btn-default input-group-addon"><i
                                                class="fa fa-search"></i> </a>
                                </div>
                            </div>
                            <div align="right">
                                <label class="col-sm-2 control-label">
                                    เบอร์โทร :
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="__phone" id="__phone"
                                       class="form-control" value=""
                                       onblur="trimValue(this);" maxlength="10" required
                                       onkeypress="chkInteger(event)">
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-2 control-label">
                                    อีเมล์ :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <input type="email" name="__email" id="__email"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true">
                            </div>
                        </div>


                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-2 control-label">
                                    ที่อยู่ปัจจุบัน: บ้านเลขที่ :
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" name="__present_address" id="__present_address"
                                       class="form-control" value=""
                                       onblur="trimValue(this);" required
                                       onkeypress="return chkNumberr()">
                            </div>

                            <div align="right">
                                <label class="col-sm-1 control-label">
                                    หมู่ :
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" name="__present_moo" id="__present_moo"
                                       class="form-control" value=""
                                       onblur="trimValue(this);" = required
                                onkeypress="chkInteger(event)">
                            </div>

                            <div align="right">
                                <label class="col-sm-1 control-label">
                                    ซอย :
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" name="__present_soi" id="__present_soi"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-2 control-label">
                                    ถนน :
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" name="__present_street" id="__present_street"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true">
                            </div>
                            <div align="right">
                                <label class="col-sm-1 control-label">
                                    จังหวัด :
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <input type="hidden" name="__present_province_code" id="__present_province_code"
                                           class="form-control"
                                           value=""
                                           readonly>
                                    <input type="text" name="__present_province_name" id="__present_province_name"
                                           class="form-control"
                                           value="" readonly required>
                                    <a href="javascript:goPage('linkhelp.php?__filter=present_province&__action=getProvincePById');"
                                       class="btn btn-default input-group-addon"><i
                                                class="fa fa-search"></i> </a>
                                </div>
                            </div>
                            <div align="right">
                                <label class="col-sm-1 control-label">
                                    อำเภอ(เขต) :
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <input type="hidden" name="__present_amphur_code" id="__present_amphur_code"
                                           class="form-control"
                                           value=""
                                           readonly>
                                    <input type="text" name="__present_amphur_name" id="__present_amphur_name"
                                           class="form-control"
                                           value="" readonly required>
                                    <a href="javascript:goPageReference('linkhelp.php?__filter=present_amphur&__action=getAmphurPById','__present_province_code');"
                                       class="btn btn-default input-group-addon"><i
                                                class="fa fa-search"></i> </a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-2 control-label">
                                    ตำบล(แขวง) :
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <input type="hidden" name="__present_district_code" id="__present_district_code"
                                           class="form-control"
                                           value=""
                                           readonly>
                                    <input type="text" name="__present_district_name" id="__present_district_name"
                                           class="form-control"
                                           value="" readonly required>
                                    <a href="javascript:goPageReference('linkhelp.php?__filter=present_district&__action=getDistrictPById','__present_amphur_code');"
                                       class="btn btn-default input-group-addon"><i
                                                class="fa fa-search"></i> </a>
                                </div>
                            </div>
                            <div align="right">
                                <label class="col-sm-2 control-label">
                                    รหัสไปรษณี :
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" name="__present_zipcode" id="__present_zipcode"
                                       class="form-control" value=""
                                       onblur="trimValue(this);" maxlength="5" required
                                       onkeypress="chkInteger(event)">
                            </div>

                            <div align="right">
                                <label class="col-sm-1 control-label">
                                    เบอร์โทร:
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" name="__present_phone" id="__present_phone"
                                       class="form-control" value=""
                                       onblur="trimValue(this);" maxlength="10" required
                                       onkeypress="chkInteger(event)">
                            </div>
                        </div>
                        <div class="form-group">
                            <!--                            <div align="right">-->
                            <!--                                <label class="col-sm-2 control-label">-->
                            <!--                                    แฟรกซ์ :-->
                            <!--                                </label>-->
                            <!--                            </div>-->
                            <!--                            <div class="col-sm-2">-->
                            <!--                                <input type="text" name="__present_fax" id="__present_fax"-->
                            <!--                                       class="form-control" value=""-->
                            <!--                                       onblur="trimValue(this);" maxlength="15"-->
                            <!--                                       onkeypress="return chkNumber()">-->
                            <!--                            </div>-->
                            <div align="right">
                                <label class="col-sm-2 control-label">
                                    ประเภทบัญชี :
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <select name="__account_type_id" id="__account_type_id" class="form-control">
                                    <?php
                                    $level = $conn->select('account_type');
                                    foreach ($level as $type) {
                                        ?>
                                        <option
                                                value="<?php echo $type['account_type_id']; ?>">
                                            <?php echo $type['account_type_name']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div align="right">
                                <label class="col-sm-1 control-label">
                                    บัญชี :
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <select name="__bank_id" id="__bank_id" class="form-control">
                                    <?php
                                    $level = $conn->select('bank');
                                    foreach ($level as $type) {
                                        ?>
                                        <option
                                                value="<?php echo $type['bank_id']; ?>">
                                            <?php echo $type['bank_name']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        เลขที่บัญชี :
                                    </label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="__account_no" id="__account_no"
                                           class="form-control" value=""
                                           onblur="trimValue(this);" maxlength="20" required
                                           onkeypress="chkInteger(event)">
                                </div>
                            </div>
                        </div>

                        <div class="box-header with-border">
                        </div>
                        <div class="box-header with-border">
                            <h3 class="box-title">ข้อมูลการศึกษา</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        กำลังศึกษาอยู่ :
                                    </label>
                                </div>
                                <?php
                                $result = $conn->queryRaw('select * from  major');
                                foreach ($result as $row) {
                                    ; ?>
                                    <div class="col-sm-2">
                                    <div class="input-group">
                                        <label class="radio">
                                            <input type="radio" name="__major_id"
                                                   id="__major_id<?php echo $row['major_id']; ?>"
                                                   value="<?php echo $row['major_id']; ?>"> <?php echo $row['major_name']; ?>
                                        </label>
                                    </div>
                                    </div><?php } ?>
                            </div>
                            <div class="form-group">
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        คณะ :
                                    </label>
                                </div>
                                <div class="col-sm-2">
                                    <select name="__faculty_id" id="__faculty_id" class="form-control">
                                        <!--                                        <option>คณะ</option>-->
                                        <?php
                                        $level = $conn->select('faculty');
                                        foreach ($level as $type) {
                                            ?>
                                            <option
                                                    value="<?php echo $type['faculty_id']; ?>">
                                                <?php echo $type['faculty_name']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                            </div>

                            <div class="form-group">
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        ภาควิชา :
                                    </label>
                                </div>
                                <div class="col-sm-3">
                                    <div class="input-group">
                                        <input type="hidden" name="__department_id" id="__department_id"
                                               class="form-control"
                                               value=""
                                               readonly>
                                        <input type="text" name="__department_name" id="__department_name"
                                               class="form-control"
                                               value="" readonly required>
                                        <!--                                        <a href="javascript:goPage('linkhelp.php?__filter=department&__action=getDepartmentById');"-->
                                        <!--                                           class="btn btn-default input-group-addon"><i-->
                                        <!--                                                    class="fa fa-search"></i> </a>-->
                                        <a href="javascript:goPageReference('../admin/linkhelp.php?__filter=department2&__action=getDepartmentById','__faculty_id');"
                                           class="btn btn-default input-group-addon"><i
                                                    class="fa fa-search"></i> </a>
                                    </div>
                                </div>
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        สาขา :
                                    </label>
                                </div>
                                <div class="col-sm-3">
                                    <div class="input-group">
                                        <input type="hidden" name="__section_id" id="__section_id"
                                               class="form-control"
                                               value=""
                                               readonly>
                                        <input type="text" name="__section_name" id="__section_name"
                                               class="form-control"
                                               value="" readonly required>
                                        <a href="javascript:goPageReference('linkhelp.php?__filter=section&__action=getSectionById','__department_id');"
                                           class="btn btn-default input-group-addon"><i
                                                    class="fa fa-search"></i> </a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        ปี :
                                    </label>
                                </div>
                                <div class="col-sm-1">
                                    <input type="text" name="__year" id="__year"
                                           class="form-control" value=""
                                           onblur="trimValue(this);" required
                                           onkeypress="chkInteger(event)" maxlength="4">
                                </div>
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        GPA :
                                    </label>
                                </div>
                                <div class="col-sm-1">
                                    <input type="text" name="__GPA" id="__GPA"
                                           class="form-control" value=""
                                           onblur="trimValue(this);" required
                                           onkeypress="return chkNumber2()" maxlength="5">
                                </div>

                            </div>
                        </div>


                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-2 control-label">
                                    ความสามารถพิเศษ :
                                </label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" name="__talent" id="__talent"
                                       class="form-control"
                                       value=""
                                       onblur="trimValue(this);" required="true">
                            </div>
                        </div>
                        <div class="box-header with-border">
                        </div>
                        <div class="box-header with-border">
                            <h3 class="box-title">ข้อมูลครอบครัว</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        สถานะภาพ :
                                    </label>
                                </div>
                                <div class="col-sm-2">
                                    <label class="radio">
                                        <input type="radio" name="father_status_id" id="father_status_id1"
                                               value="1" onclick="hiddenndad('1')"/> มีชีวิตอยู่<br/>
                                    </label>
                                </div>
                                <div class="col-sm-2">
                                    <label class="radio">
                                        <input type="radio" name="father_status_id" id="father_status_id2"
                                               value="2" onclick="hiddenndad('2')"/> แยกกันอยู่
                                    </label>
                                </div>
                                <div class="col-sm-2">
                                    <label class="radio">
                                        <input type="radio" name="father_status_id" id="father_status_id3"
                                               value="3" onclick="hiddenndad('3')"/> ถึงแก่กรรม
                                    </label>
                                </div>
                                <div class="col-sm-1">
                                    <label class="radio">
                                        <input type="radio" name="father_status_id" id="father_status_id4"
                                               value="4" onclick="hiddenndad('0')"/> อื่นๆ
                                    </label>
                                </div>
                                <div class="col-sm-2">
                                    <input style="display: none" type="text" name="father_other_status"
                                           id="__father_other_status"
                                           class="form-control"
                                           value=""
                                           onblur="trimValue(this);" required="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        บิดาชื่อ :
                                    </label>
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="__father_first_name" id="__father_first_name"
                                           class="form-control"
                                           value=""
                                           onblur="trimValue(this);" required="true">
                                </div>
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        นามสกุล :
                                    </label>
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="__father_last_name" id="__father_last_name"
                                           class="form-control"
                                           value=""
                                           onblur="trimValue(this);" required="true">
                                </div>
                            </div>
                            <div class="form-group" name="__father" id="__father">
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        อายุ :
                                    </label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="__father_ago" id="__father_ago"
                                           class="form-control" value=""
                                           onblur="trimValue(this);" maxlength="3" required
                                           onkeypress="chkInteger(event)">
                                </div>
                                <div align="right">
                                    <label class="col-sm-1 control-label">
                                        อาชีพ :
                                    </label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="__father_job" id="__father_job"
                                           class="form-control"
                                           value=""
                                           onblur="trimValue(this);" required="true">
                                </div>
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        เงินเดือน :
                                    </label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="__father_salary" id="__father_salary"
                                           class="form-control" value=""
                                           onblur="trimValue(this);" maxlength="7" required
                                           onkeypress="chkInteger(event)">
                                </div>
                            </div>

                            <div class="form-group" name="__father2" id="__father2">
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        ที่อยู่ :
                                    </label>
                                </div>
                                <div class="col-sm-5">
                                    <textarea class="form-control" name="__father_address" id="__father_address"
                                              rows="6" onblur="trimValue(this);" required></textarea>
                                </div>

                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        เบอร์โทร :
                                    </label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="__father_phone" id="__father_phone"
                                           class="form-control" value=""
                                           onblur="trimValue(this);" maxlength="10" required
                                           onkeypress="chkInteger(event)">
                                </div>
                            </div>
                            <div class="form-group">
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        สถานะภาพ :
                                    </label>
                                </div>

                                <div class="col-sm-2">
                                    <label class="radio">
                                        <input type="radio" name="mother_status_id" id="mother_status_id1"
                                               value="1" onclick="hiddennmom('1')"/> มีชีวิตอยู่
                                    </label>
                                </div>
                                <div class="col-sm-2">
                                    <label class="radio">
                                        <input type="radio" name="mother_status_id" id="mother_status_id2"
                                               value="2" onclick="hiddennmom('2')"/> แยกกันอยู่
                                    </label>

                                </div>
                                <div class="col-sm-2">
                                    <label class="radio">
                                        <input type="radio" name="mother_status_id" id="mother_status_id3"
                                               value="3" onclick="hiddennmom('3')"/> ถึงแก่กรรม
                                    </label>
                                </div>
                                <div class="col-sm-1">
                                    <label class="radio">
                                        <input type="radio" name="mother_status_id" id="mother_status_id4"
                                               value="4" onclick="hiddennmom('0')"/> อื่นๆ
                                    </label>
                                </div>
                                <div class="col-sm-2">
                                    <input style="display: none" type="text" name="mother_other_status"
                                           id="__mother_other_status"
                                           class="form-control"
                                           value=" "
                                           required="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        มารดาชื่อ :
                                    </label>
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="__mother_first_name" id="__mother_first_name"
                                           class="form-control"
                                           value=""
                                           onblur="trimValue(this);" required="true">
                                </div>
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        นามสกุล :
                                    </label>
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="__mother_last_name" id="__mother_last_name"
                                           class="form-control"
                                           value=""
                                           onblur="trimValue(this);" required="true">
                                </div>
                            </div>
                            <div class="form-group" name="__mother" id="__mother">
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        อายุ :
                                    </label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="__mother_ago" id="__mother_ago"
                                           class="form-control" value=""
                                           onblur="trimValue(this);" maxlength="3" required
                                           onkeypress="chkInteger(event)">
                                </div>
                                <div align="right">
                                    <label class="col-sm-1 control-label">
                                        อาชีพ :
                                    </label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="__mother_job" id="__mother_job"
                                           class="form-control"
                                           value=""
                                           onblur="trimValue(this);" required="true">
                                </div>
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        เงินเดือน :
                                    </label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="__mother_salary" id="__mother_salary"
                                           class="form-control" value=""
                                           onblur="trimValue(this);" maxlength="7" required
                                           onkeypress="chkInteger(event)">
                                </div>
                            </div>

                            <div class="form-group" name="__mother2" id="__mother2">
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        ที่อยู่ :
                                    </label>
                                </div>
                                <div class="col-sm-5">
                                    <textarea class="form-control" name="__mother_address" id="__mother_address"
                                              rows="6" onblur="trimValue(this);" required></textarea>
                                </div>
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        เบอร์โทร :
                                    </label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="__mother_phone" id="__mother_phone"
                                           class="form-control" value=""
                                           onblur="trimValue(this);" maxlength="10" required
                                           onkeypress="chkInteger(event)">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2">

                                </div>
                                <div class="col-sm-2">
                                    <div class="input-group">
                                        <label class="radio">
                                            <input type="radio" name="__parent"
                                                   id="__parent1" onclick="CheckParent(this.value)"
                                                   value="1">บิดา
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="input-group">
                                        <label class="radio">
                                            <input type="radio" name="__parent"
                                                   id="__paren2" onclick="CheckParent(this.value)"
                                                   value="2">มารดา
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        ผู้ปกครองชื่อ :
                                    </label>
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="__parent_first_name" id="__parent_first_name"
                                           class="form-control"
                                           value=""
                                           onblur="trimValue(this);" required="true">
                                </div>
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        นามสกุล :
                                    </label>
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="__parent_last_name" id="__parent_last_name"
                                           class="form-control"
                                           value=""
                                           onblur="trimValue(this);" required="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        อายุ :
                                    </label>
                                </div>
                                <div class="col-sm-1">
                                    <input type="text" name="__parent_age" id="__parent_age"
                                           class="form-control" value=""
                                           onblur="trimValue(this);" maxlength="3" required
                                           onkeypress="chkInteger(event)">
                                </div>
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        อาชีพ :
                                    </label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="__parent_job" id="__parent_job"
                                           class="form-control"
                                           value=""
                                           onblur="trimValue(this);" required="true">
                                </div>
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        เงินเดือน :
                                    </label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="__parent_salary" id="__parent_salary"
                                           class="form-control" value=""
                                           onblur="trimValue(this);" maxlength="7" required
                                           onkeypress="chkInteger(event)">
                                </div>
                            </div>
                            <div class="form-group">
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        เกี่ยวข้องเป็น :
                                    </label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="__parent_abount" id="__parent_abount"
                                           class="form-control"
                                           value=""
                                           onblur="trimValue(this);" required="true">
                                </div>
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        เบอร์โทร:
                                    </label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="__parent_phone" id="__parent_phone"
                                           class="form-control" value=""
                                           onblur="trimValue(this);" maxlength="10" required
                                           onkeypress="chkInteger(event)">
                                </div>
                            </div>

                            <div class="form-group">
                                <div align="right">
                                    <label class="col-sm-2 control-label">
                                        ที่อยู่ :
                                    </label>
                                </div>
                                <div class="col-sm-5">
                                    <textarea class="form-control" name="__parent_address" id="__parent_address"
                                              rows="6" onblur="trimValue(this);"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-5 control-label">
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <a class="btn btn-success" href="javascript:goSave();">บันทึก</a>
                                <a class="btn btn-warning" href="javascript:goClear()">ล้าง</a>
                                <a class="btn btn-default" href="nisit.php">ย้อนกลับ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <?php include "footer.php" ?>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>

    function chkNumber(ele) {
        var vchar = String.fromCharCode(event.keyCode);
        if ((vchar < '0' || vchar > '9') && (vchar != '-')) return false;
        ele.onKeyPress = vchar;
    }

    function chkNumber2(ele) {
        var vchar = String.fromCharCode(event.keyCode);
        if ((vchar < '0' || vchar > '9') && (vchar != '.')) return false;
        ele.onKeyPress = vchar;
    }

    function chkNumberr(ele) {
        var vchar = String.fromCharCode(event.keyCode);
        if ((vchar < '0' || vchar > '9') && (vchar != '/')) return false;
        ele.onKeyPress = vchar;
    }

    $('#menu-nisit-main').addClass('active');
    $('#menu-nisit').addClass('active');
    $('#__birth_date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
    });

    function helpReturn(value, action) {
        $.ajax({
            url: 'Allservice.php',
            data: {id: value, action: action},
            method: 'POST',
            success: function (result) {
                var data = JSON.parse(result);
                console.log(data);
                if (action == "getNisitById") {
                    if (data.nisit_id != null) {

                        setValueNisit(data);
                    }
                }
                if (action == "getProvinceById") {
                    if (data.province_code != null) {
                        setValueProvince(data);
                    }
                }
                if (action == "getAmphurById") {
                    if (data.amphur_code != null) {
                        setValueAmphur(data);
                    }
                }
                if (action == "getDistrictById") {
                    if (data.district_code != null) {
                        setValueDistrict(data);
                    }
                }
                if (action == "getProvincePById") {
                    if (data.province_code != null) {
                        setValueProvinceP(data);
                    }
                }
                if (action == "getAmphurPById") {
                    if (data.amphur_code != null) {
                        setValueAmphurP(data);
                    }
                }
                if (action == "getDistrictPById") {
                    if (data.district_code != null) {
                        setValueDistrictP(data);
                    }
                }
                if (action == "getSectionById") {
                    if (data.section_id != null) {
                        setValueSection(data);
                    }
                }
                if (action == "getDepartmentById") {
                    if (data.department_id != null) {
                        setValueDepartment(data);
                    }
                }
            }
        });
    }

    function CheckParent(value) {
        if (value == 1) {
            $("#__parent_abount").val('บิดา');
            $("#__parent_first_name").val($("#__father_first_name").val());
            $("#__parent_last_name").val($("#__father_last_name").val());
            $("#__parent_age").val($("#__father_ago").val());
            $("#__parent_job").val($("#__father_job").val());
            $("#__parent_salary").val($("#__father_salary").val());
            $("#__parent_address").val($("#__father_address").val());
            $("#__parent_phone").val($("#__father_phone").val());
        } else {
            $("#__parent_first_name").val($("#__mother_first_name").val());
            $("#__parent_last_name").val($("#__mother_last_name").val());
            $("#__parent_age").val($("#__mother_ago").val());
            $("#__parent_job").val($("#__mother_job").val());
            $("#__parent_salary").val($("#__mother_salary").val());
            $("#__parent_address").val($("#__mother_address").val());
            $("#__parent_phone").val($("#__mother_phone").val());
            $("#__parent_abount").val('มารดา');
        }
    }


    function setValueNisit(data) {
        with (document.form_data) {
            $("#__nisit_id").val(data.nisit_id);
            $("#__nisit_code").val(data.nisit_code);
            $("#__first_name").val(data.first_name);
            $("#__last_name").val(data.last_name);
            $("#__id_card").val(data.id_card);
            $("#__birth_date").val(data.birth_date);
            $("#__religion").val(data.religion);
            $("#__image_path").val(data.image_path);
            $("#img-preview").attr('src', '<?php echo PATH_UPLOAD . '/';?>' + data.image_path);
            $("#__title_id").val(data.title_id);
            $("#__weight").val(data.weight);
            $("#__tall").val(data.tall);
            $("#__national").val(data.national);
            $("#__rac").val(data.rac);
            $("#__address").val(data.address);
            $("#__moo").val(data.moo);
            $("#__soi").val(data.soi);
            $("#__street").val(data.street);
            $("#__zipcode").val(data.zipcode);
            $("#__province_code").val(data.province_code);
            $("#__amphur_code").val(data.amphur_code);
            $("#__district_code").val(data.district_code);
            $("#__province_name").val(data.province_name);
            $("#__amphur_name").val(data.amphur_name);
            $("#__district_name").val(data.district_name);
            $("#__phone").val(data.phone);
            $("#__email").val(data.email);
            $("#__present_address").val(data.present_address);
            $("#__present_moo").val(data.present_moo);
            $("#__present_soi").val(data.present_soi);
            $("#__present_street").val(data.present_street);
            $("#__present_province_code").val(data.present_province_code);
            $("#__present_amphur_code").val(data.present_amphur_code);
            $("#__present_district_code").val(data.present_district_code);
            $("#__present_province_name").val(data.province_name);
            $("#__present_amphur_name").val(data.amphur_name);
            $("#__present_district_name").val(data.district_name);
            $("#__present_zipcode").val(data.present_zipcode);
            $("#__present_phone").val(data.present_phone);
            $("#__present_fax").val(data.present_fax);
            $("#__account_type_id").val(data.account_type_id);
            $("#__bank_id").val(data.bank_id);
            $("#__account_no").val(data.account_no);
            $("#__register_date").val(data.register_date);

            $("#__nisit_id").val(data.nisit_id);
            $("#__education_level").val(data.education_level);
            $("#__education_section").val(data.education_section);
            $("#__education_from").val(data.education_from);
            $("#__education_province_code").val(data.education_province_code);
            $("#__major_id" + data.major_id).prop("checked", true);
            $("#__year").val(data.year);
            $("#__GPA").val(data.GPA);
            $("#__faculty_id").val(data.faculty_id).change();
            // $("#__faculty_id" + data.faculty_id).prop("checked", true);
            $("#__department_id").val(data.department_id);
            $("#__section_id").val(data.section_id);
            $("#__talent").val(data.talent);
            // $("#__other_faculty").val(data.other_faculty);
            $("#__section_name").val(data.section_name);
            $("#__department_name").val(data.department_name);
            $("#__faculty_name").val(data.faculty_name);
            $("#__major_name").val(data.major_name);

            $("#__father_first_name").val(data.father_first_name);
            $("#__father_last_name").val(data.father_last_name);
            $("#__father_ago").val(data.father_ago);
            $("#__father_job").val(data.father_job);
            $("#__father_salary").val(data.father_salary);
            $("#father_status_id" + data.father_status_id).prop("checked", true);
            $("#__father_other_status").val(data.father_other_status);
            $("#__father_address").val(data.father_address);
            $("#__father_phone").val(data.father_phone);
            $("#__mother_first_name").val(data.mother_first_name);
            $("#__mother_last_name").val(data.mother_last_name);
            $("#__mother_ago").val(data.mother_ago);
            $("#__mother_job").val(data.mother_job);
            $("#__mother_salary").val(data.mother_salary);
            $("#mother_status_id" + data.mother_status_id).prop("checked", true);
            $("#__mother_other_status").val(data.mother_other_status);
            $("#__mother_address").val(data.mother_address);
            $("#__mother_phone").val(data.mother_phone);

            $("#__parent_first_name").val(data.parent_first_name);
            $("#__parent_last_name").val(data.parent_last_name);
            $("#__parent_age").val(data.parent_age);
            $("#__parent_job").val(data.parent_job);
            $("#__parent_salary").val(data.parent_salary);
            $("#__parent_abount").val(data.parent_abount);
            $("#__parent_address").val(data.parent_address);
            $("#__parent_phone").val(data.parent_phone);
            $("#scholarship_type_id" + data.scholarship_type_id).prop("checked", true);
            $("#__scholarship_id").val(data.scholarship_id);
            $("#__scholarship_name").val(data.scholarship_name);
            $("#___faculty_id").val(data.faculty_id);
            $("#___faculty_name").val(data.faculty_name);
        }
    }

    function setValueProvince(data) {
        with (document.form_data) {
            $("#__province_code").val(data.province_code);
            $("#__province_name").val(data.province_name);
        }
    }

    function setValueAmphur(data) {
        with (document.form_data) {
            $("#__amphur_code").val(data.amphur_code);
            $("#__amphur_name").val(data.amphur_name);
        }
    }

    function setValueDistrict(data) {
        with (document.form_data) {
            $("#__district_code").val(data.district_code);
            $("#__district_name").val(data.district_name);
        }
    }

    function setValueProvinceP(data) {
        with (document.form_data) {
            $("#__present_province_code").val(data.province_code);
            $("#__present_province_name").val(data.province_name);
        }
    }

    function setValueAmphurP(data) {
        with (document.form_data) {
            $("#__present_amphur_code").val(data.amphur_code);
            $("#__present_amphur_name").val(data.amphur_name);
        }
    }

    function setValueDistrictP(data) {
        with (document.form_data) {
            $("#__present_district_code").val(data.district_code);
            $("#__present_district_name").val(data.district_name);
        }
    }

    function setValueSection(data) {
        with (document.form_data) {
            $("#__section_id").val(data.section_id);
            $("#__section_name").val(data.section_name);
        }
    }

    function setValueDepartment(data) {
        with (document.form_data) {
            $("#__department_id").val(data.department_id);
            $("#__department_name").val(data.department_name);
        }
    }


    function hiddenndad(statusdad) {
        if (statusdad == 0) {
            document.getElementById("__father_other_status").style.display = '';
        } else {
            document.getElementById("__father_other_status").style.display = 'none';
        }
        if (statusdad == 3) {
            document.getElementById("__father").style.display = 'none';
            document.getElementById("__father2").style.display = 'none';
        } else {
            document.getElementById("__father").style.display = '';
            document.getElementById("__father2").style.display = '';
        }
    }

    function hiddennmom(statusmom) {
        if (statusmom == 0) {
            document.getElementById("__mother_other_status").style.display = '';
        } else {
            document.getElementById("__mother_other_status").style.display = 'none';
        }
        if (statusmom == 3) {
            document.getElementById("__mother").style.display = 'none';
            document.getElementById("__mother2").style.display = 'none';
        } else {
            document.getElementById("__mother").style.display = '';
            document.getElementById("__mother2").style.display = '';
        }

    }
</script>
<script>helpReturn('<?php echo $nisit_id;?>', 'getNisitById')</script>
</body>
</html>


