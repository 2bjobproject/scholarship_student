<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(ADMIN_LEVEL,STAFF_LEVEL)))
    redirect_to('index.php');
require_once "../connection.inc.php";

$cmd = getIsset("__cmd");
if ($cmd == "delete") {
    $conn->delete("nisit_scholarship", array("nisit_scholarship_id" => getIsset('__delete_field')));
    redirectTo('check_scholarship.php');
}

$filterDefault = " where 1=1 ";

$keyword = getIsset("keyword");
$option_val = getIsset("option");
if ($keyword != "") {
    $filterDefault .= " and " . $option_val . " like '%" . $keyword . "%'";
}
$sql = "SELECT nisit_scholarship.*,first_name,last_name,scholarship_name,faculty_name,semester_name,scholarship_type_name FROM nisit_scholarship
LEFT JOIN scholarship on scholarship.scholarship_id = nisit_scholarship.scholarship_id
LEFT JOIN nisit on nisit.nisit_id = nisit_scholarship.nisit_id
LEFT JOIN nisit_education on nisit_education.nisit_id = nisit.nisit_id
LEFT JOIN faculty on faculty.faculty_id = scholarship.faculty_id
LEFT JOIN semester on semester.semester_id = scholarship.semester_id
LEFT JOIN scholarship_type on scholarship_type.scholarship_type_id = scholarship.scholarship_type_id
";
$result_row = $conn->queryRaw($sql . $filterDefault);//คิวรี่ คำสั่ง
$total = sizeof($result_row);
$for_end = $limit;
$for_start = $start * $limit;

$select_all = $conn->queryRaw($sql . $filterDefault . " order by nisit_scholarship_id desc  limit " . $for_start . "," . $for_end);
$total_num = sizeof($select_all);


?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php require_once "css.php" ?>
</head>
<body class="skin-custom sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                รายงานทุน
                <small>รายงานนักศึกษาทุน</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>
                <li class="active">รายงานนักศึกษาทุน</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <form class="form-horizontal" id="form_data" name="form_data" method="post">
                <input id="__delete_field" name="__delete_field" type="hidden" value="">
                <input id="__cmd" name="__cmd" type="hidden" value="">
                <div class="box box-custom">
                    <div class="box-header with-border">
                        <h3 class="box-title">รายงานนักศึกษาทุน </h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-7">
<!--                                <a class="btn btn-primary" href="nisit_scholarship-update.php"><i-->
<!--                                            class="fa fa-plus"></i> เพิ่มข้อมูล</a>-->
                            </div>
                            <div class="col-sm-2">
                                <select id="option" name="option" class="form-control">
                                    <option value="scholarship_name">ชื่อทุน</option>
                                    <option value="first_name">ชื่อนักศึกษา</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <input class="form-control" type="text" id="keyword" name="keyword"
                                           onblur="trimValue(this)"
                                           value="<?php echo $keyword; ?>">
                                    <a href="javascript:goSearch();"
                                       class="btn btn-default input-group-addon"><i
                                            class="fa fa-search"></i> </a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class=" table-responsive">
                                    <table class="table table-bordered table-striped" id="tbView">
                                        <tr>
                                            <th width="5%">ลำดับ</th>
                                            <th width="10%">ชื่อ - นามสกุล</th>
                                            <th width="10%">ชื่อทุน</th>
                                            <th width="10%">ปีการศึกษา</th>
                                            <th width="10%">วันที่สมัครทุนการศึกษา</th>
                                            <th width="10%" class="text-center">จัดการ</th>
                                        </tr>
                                        <tbody>
                                        <?php
                                        $index = $for_start;
                                        foreach ($select_all as $row) {
                                            $index++;
                                            ?>
                                            <tr>
                                                <td class="text-right"
                                                    nowrap><?php echo $index; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['first_name']. ' ' .$row['last_name']; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['scholarship_name']; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['semester_name']; ?></td>
                                                <td class=""
                                                    nowrap><?php echo $row['register_date']; ?></td>
                                                <td class="text-center" nowrap >
                                                    <div class="btn-group">
                                                        <a class="btn btn-success btn-xs" target="_blank"
                                                           href="print-nisit_scholarship-update.php?__nisit_scholarship_id=<?php echo $row['nisit_id']; ?>"><i
                                                                class="fa fa-print" ></i> </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-12">
                                    <?php include "pageindex.php"; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <?php include "footer.php" ?>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>
    $('#menu-report-main,#menu-report').addClass('active');
</script>
</body>
</html>