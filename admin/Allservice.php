<?php
session_start();
require_once('../connection.inc.php');
require_once('../common.inc.php');
$action = getIsset('action');
$result = array();

if ($action == 'getEmployeeById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select employee.*,employee_type_name from employee
left join employee_type on employee_type.employee_type_id=employee.employee_type_id  where employee_id='$id'", true);
}
if ($action == 'getCustomerById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select customer.*,customer_type_name from customer
left join customer_type on customer_type.customer_type_id=customer.customer_type_id  where customer_id='$id'", true);
}

if ($action == 'getFacultyById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select * from faculty where faculty_id='$id'", true);
}

if ($action == 'getMajorById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select * from major where major_id='$id'", true);
}

if ($action == 'getSemesterById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select * from semester where semester_id='$id'", true);
}
if ($action == 'getScholarshipById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select scholarship.*,scholarship_type_name,semester_name from scholarship
left join scholarship_type on scholarship_type.scholarship_type_id=scholarship.scholarship_type_id 
left join semester on semester.semester_id=scholarship.semester_id  where scholarship_id='$id'", true);
    $result['child'] = $conn->queryRaw("select scholarship_detail.*,faculty.faculty_name from scholarship_detail
left join faculty on faculty.faculty_id=scholarship_detail.faculty_id where scholarship_id='" . $id . "'");
}

if ($action == 'getNisit_scholarshipById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select nisit_scholarship.*,scholarship_name,status_name,major_name,faculty_name,department_name,
concat(employee.first_name,' ',employee.last_name) as employee_name,
concat(nisit.first_name,' ',nisit.last_name) as nisit_name from nisit_scholarship
left join nisit on nisit.nisit_id=nisit_scholarship.nisit_id 
left join scholarship on scholarship.scholarship_id=nisit_scholarship.scholarship_id 
left join status on status.status_id=nisit_scholarship.status_id 
left join employee on employee.employee_id=nisit_scholarship.employee_id
left join nisit_education on nisit_education.nisit_id=nisit.nisit_id 
left join department on department.department_id=nisit_education.department_id 
left join faculty on faculty.faculty_id=nisit_education.faculty_id 
left join major on major.major_id=nisit_education.major_id   where nisit_scholarship_id='$id'", true);
    $result['child'] = array();
}

if ($action == 'getScholarship_typeById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select * from scholarship_type where Scholarship_type_id='$id'", true);
}
if ($action == 'getNisit_eventById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select nisit_event.*,first_name,last_name,event_name from nisit_event
left join nisit on nisit.nisit_id=nisit_event.nisit_id 
left join event on event.event_id=nisit_event.event_id where nisit_event_id='$id'", true);
}
if ($action == 'getEventById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select * from event where event_id='$id'", true);
}
if ($action == 'getNisitById') {
    $id = getIsset('id');

    $result = $conn->queryRaw("select nisit.*,province_name,amphur_name,district_name,
nisit_education.*,major_name,faculty_name,department_name,section_name,nisit_family.* 
from nisit
left join province on province.province_code=nisit.province_code
left join amphur on amphur.amphur_code=nisit.amphur_code
left join district on district.district_code=nisit.district_code
left join nisit_education on nisit_education.nisit_id=nisit.nisit_id
left join major on major.major_id=nisit_education.major_id
left join faculty on faculty.faculty_id=nisit_education.faculty_id
left join department on department.department_id=nisit_education.department_id
left join section on section.section_id=nisit_education.section_id
left join nisit_family on nisit_family.nisit_id=nisit.nisit_id
where nisit.nisit_id='$id'", true);
}

if ($action == 'getDepartmentById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select department.*,faculty_name from department
left join faculty on faculty.faculty_id=department.faculty_id where department_id='$id'", true);
}

if ($action == 'getSectionById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select section.*,department_name from section
left join department on department.department_id=section.department_id where section_id='$id'", true);
}

if ($action == 'getProvinceById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select * from province  where province_code='$id'", true);
}

if ($action == 'getAmphurById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select * from amphur  where amphur_code='$id'", true);
}

if ($action == 'getDistrictById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select * from district  where district_code='$id'", true);
}

if ($action == 'getProvincePById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select * from province  where province_code='$id'", true);
}

if ($action == 'getAmphurPById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select * from amphur  where amphur_code='$id'", true);
}

if ($action == 'getDistrictPById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select * from district  where district_code='$id'", true);
}
echo json_encode($result);