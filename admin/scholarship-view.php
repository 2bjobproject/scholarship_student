<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(STAFF_LEVEL)))
    redirect_to('index.php');

require_once "../connection.inc.php";

$cmd = getIsset("__cmd");
$test = array();
$scholarship_id = getIsset('__scholarship_id');
if ($cmd == "save") {
    $value = array(
        "scholarship_name" => getIsset('__scholarship_name'),
        "amount_use" => getIsset('__amount_use'),
        "semester_id" => getIsset('__semester_id'),
        "scholarship_type_id" => getIsset('__scholarship_type_id'),
        "property_detail" => getIsset('__property_detail'),
        "faculty_id" => getIsset('__faculty_id'),
    );
    if ($scholarship_id == "0") {
        if ($conn->create("scholarship", $value)) {
            redirectTo("scholarship.php");
        }

    } else {
        if ($conn->update("scholarship", $value, array("scholarship_id" => $scholarship_id))) {
            redirectTo("scholarship.php");
        }
    }
}
$fac = $conn->queryRaw("select scholarship_detail.*,faculty.faculty_name from scholarship_detail
left join faculty on faculty.faculty_id=scholarship_detail.faculty_id where scholarship_id='" . $scholarship_id. "'");
$config = $conn->queryRaw("select * from scholarship where scholarship_id='" . $scholarship_id. "'",true);
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php include "css.php" ?>

</head>
<body class="skin-custom sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                ข้อมูลทุน
                <small>จัดการข้อมูลทุน</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>
                <li><a href="section.php">ทุน</a></li>
                <li class="active">จัดการข้อมูล</li>
            </ol>
        </section>
        <section class="content">
            <form class="form-horizontal" id="form_data" name="form_data" method="post" enctype="multipart/form-data">
                <input id="__cmd" name="__cmd" type="hidden" value="">
                <div class="box box-custom">
                    <div class="box-header with-border">
                        <h3 class="box-title">จัดการข้อมูลทุน </h3>
                    </div>
                    <div class="box-body">
                        <input type="hidden" name="__scholarship_id" id="__scholarship_id" class="form-control"
                               value="0"
                               required="true" readonly>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    ชื่อทุน :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="__scholarship_name" id="__scholarship_name"
                                       class="form-control"
                                       value="" readonly
                                       onblur="trimValue(this);" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    จำนวนคน :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="__amount_use" id="__amount_use"
                                       class="form-control"
                                       value="" readonly
                                       onblur="trimValue(this);" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    ต้องการคุณสมบัติ :
                                </label>
                            </div>
                            <div class="col-sm-7">
                                <textarea class="form-control" name="__property_detail" id="__property_detail" readonly
                                          rows="6" onblur="trimValue(this);" required="true"><?php echo $config['property_detail'];?></textarea>
                            </div>
                        </div>


                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    ปีการศึกษา :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <input type="hidden" name="__semester_id" id="__semester_id"
                                           class="form-control"
                                           value=""
                                           readonly>
                                    <input type="text" name="__semester_name" id="__semester_name"
                                           class="form-control"
                                           value="" readonly required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    ประเภททุน :
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <input type="hidden" name="__scholarship_type_id" id="__scholarship_type_id"
                                           class="form-control"
                                           value=""
                                           readonly>
                                    <input type="text" name="__scholarship_type_name" id="__scholarship_type_name"
                                           class="form-control"
                                           value="" readonly required>

                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                    คณะ :
                                </label>
                            </div>
                            <div class="col-lg-7">
                            <label class=" control-label" style="text-align: left">
                                <?php if ($fac != null) {
                                    foreach ($fac as $i => $list) {
                                        if($i+1 == sizeof($fac)){
                                            echo $list['faculty_name'] ;
                                        }else{
                                            echo $list['faculty_name'] . ' ,<br>';
                                        }
                                    }
                                }; ?>
                            </label>
                            </div>

                        </div>
                        <div class="form-group">
                            <div align="right">
                                <label class="col-sm-3 control-label">
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <a class="btn btn-default" href="scholarship.php">ย้อนกลับ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <?php include "footer.php" ?>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>
    $('#menu-scholarship_type-main').addClass('active');
    $('#menu-scholarship').addClass('active');

    function helpReturn(value, action) {
        $.ajax({
            url: 'Allservice.php',
            data: {id: value, action: action},
            method: 'GET',
            success: function (result) {
                var data = JSON.parse(result);
                if (action == "getScholarshipById") {
                    if (data.scholarship_id != null) {
                        console.log(data);
                        setValueScholarship(data);
                    }
                }
                if (action == "getSemesterById") {
                    if (data.semester_id != null) {
                        console.log(data);
                        setValueSemester(data);
                    }
                }
                if (action == "getScholarship_typeById") {
                    if (data.scholarship_type_id != null) {
                        console.log(data);
                        setValueScholarship_type(data);
                    }
                }
                if (action == "getFacultyById") {
                    if (data.faculty_id != null) {
                        console.log(data);
                        setValueFaculty(data);
                    }
                }
            }
        });
    }

    function setValueScholarship(data) {
        with (document.form_data) {
            $("#__scholarship_id").val(data.scholarship_id);
            $("#__scholarship_name").val(data.scholarship_name);
            $("#__amount_use").val(data.amount_use);
            $("#__semester_id").val(data.semester_id);
            $("#__scholarship_type_id").val(data.scholarship_type_id);
            $("#__faculty_id").val(data.faculty_id);
            $("#__semester_name").val(data.semester_name);
            $("#__scholarship_type_name").val(data.scholarship_type_name);
            $("#__faculty_name").val(data.faculty_name);
            // $("#__property_detail").val(data.property_detail);
        }
    }

    function setValueSemester(data) {
        with (document.form_data) {
            $("#__semester_id").val(data.semester_id);
            $("#__semester_name").val(data.semester_name);

        }
    }

    function setValueScholarship_type(data) {
        with (document.form_data) {
            $("#__scholarship_type_id").val(data.scholarship_type_id);
            $("#__scholarship_type_name").val(data.scholarship_type_name);

        }
    }

    function setValueFaculty(data) {
        with (document.form_data) {
            $("#__faculty_id").val(data.faculty_id);
            $("#__faculty_name").val(data.faculty_name);
        }
    }
    CKEDITOR.replace('__property_detail', {
        customConfig: '',
        disallowedContent: 'img{width,height,float}',
        extraAllowedContent: 'img[width,height,align]',
        extraPlugins: 'uploadimage',
        imageUploadUrl: 'image_uploader_drag.php',
        filebrowserUploadUrl: 'image_uploader.php',
        height: 200,
        bodyClass: 'document-editor',
        format_tags: 'p;h1;h2;h3;pre',
        removeDialogTabs: 'image:advanced;link:advanced',
        toolbarGroups: [

            // {name: 'document', groups: ['mode']},
            {name: 'basicstyles', groups: ['basicstyles']},
            {name: 'styles'},

        ],
    });
</script>
<script>helpReturn('<?php echo $scholarship_id;?>', 'getScholarshipById')</script>
</body>
</html>


