<?php
session_start(); // เปิดใช้งาน session
require_once "common.inc.php"; //
require_once "connection.inc.php"; //

$cmd = getIsset("__cmd");
if ($cmd == "delete") {
    $conn->delete("reserve_car", array("reserve_no" => getIsset('__delete_field')));
    redirectTo('reserve_car.php');
}
$user_id = $uprofile['id'];
$keyword = getIsset("keyword");
$option_val = getIsset("option");

$sql = "select nisit_scholarship.*,status_name,first_name,last_name,scholarship_name from nisit_scholarship
left join nisit on nisit.nisit_id=nisit_scholarship.nisit_id
left join scholarship on scholarship.scholarship_id=nisit_scholarship.scholarship_id
left join status on status.status_id=nisit_scholarship.status_id
where nisit.nisit_id=$user_id
";
$result_row = $conn->queryRaw($sql);//คิวรี่ คำสั่ง
$total = sizeof($result_row);
$for_end = $limit;
$for_start = $start * $limit;

$select_all = $conn->queryRaw($sql . " order by nisit_scholarship_id desc  limit " . $for_start . "," . $for_end);
$total_num = sizeof($select_all);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- METAS -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- TITLE -->
    <title><?php echo TITLE_ENG; ?></title>
    <?php require_once "css.php"; ?>
</head>

<body>
<!-- Preloader Start -->
<div id="preloader">
    <i class="fa fa-spinner fa-spin preloader-animation" aria-hidden="true"></i>
</div>
<!-- Preloader End -->

<!-- WRAPPER START -->
<div id="wrapper">
    <!-- HEADER START -->
    <?php require_once "menu.php"; ?>
    <!-- HEADER END -->
    <!-- HERO SLIDER -->
    <?php require_once "slider.php"; ?>
    <!-- HERO SLIDER END-->
    <!-- CONTENT START -->
    <section class="content">
        <section id="blog-list" class="container">
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" id="form_data" name="form_data" method="post">
                        <input id="__delete_field" name="__delete_field" type="hidden" value="">
                        <input id="__cmd" name="__cmd" type="hidden" value="">

                        <div class="col-md-12">
                            <label class="col-sm-3 control-label">
                            </label>
                        </div>
                        <div class="clr"></div>
                        <div class="col-sm-12">
                            <div class="box box-gold">
                                <div class="box-header with-border">
                                    <h3 class="box-title">รายการข้อมูลการขอทุน</h3>
                                </div>
                                <div class="box-body">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class=" table-responsive">
                                                <table class="table table-hover tbgray" id="tbView">
                                                    <tr>
                                                        <th width="5%">ลำดับ</th>
                                                        <th width="20%">ชื่อผู้ขอทุน</th>
                                                        <th width="20%">ชื่อทุน</th>
                                                        <th width="20%">วันที่สมัครทุนการศึกษา</th>
                                                        <th width="20%">สถานะ</th>
                                                        <th width="20%"></th>
                                                    </tr>
                                                    <tbody>
                                                    <?php
                                                    $index = $for_start;
                                                    foreach ($select_all

                                                    as $row) {
                                                    $index++;
                                                    ?>
                                                    <tr>
                                                        <td class="active" align="center"
                                                            nowrap><?php echo $index; ?></td>
                                                        <td class="active"
                                                            nowrap><?php echo $row['first_name'] . ' ' . $row['last_name']; ?></td>
                                                        <td class="active"
                                                            nowrap><?php echo $row['scholarship_name']; ?></td>
                                                        <td class="active"
                                                            nowrap><?php echo $row['register_date']; ?></td>
                                                        <td class="active"
                                                            nowrap><?php echo $row['status_name']; ?></td>

                                                        <td><a class="btn btn-warning btn-xs"
                                                               href="list_scholarship-detail.php?__nisit_scholarship_id=<?php echo $row['nisit_scholarship_id']; ?>"><i
                                                                        class="fa fa-edit"></i> </a></td>
                                                        <?php } ?>
                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </section>
    <!-- CONTENT END -->

    <!-- FOOTER START -->
    <?php require_once "footer.php"; ?>
    <!-- FOOTER END -->


</div>
<!-- WRAPPER END -->

<!-- back to top button -->
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="เลื่อนขึ้น"
   data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>


<!-- SCRIPT START -->
<?php require_once "script.php"; ?>
<!-- SCRIPT END -->
<script>

</script>
</body>
</html>

